
/* JavaScript content from js/main.js in folder common */

/* JavaScript content from js/main.js in folder common */

/* JavaScript content from js/main.js in folder common */
var getNativeData = function(type, successCallback, failureCallback, data) {
    if (data === undefined) {
        data = '';
    }
    cordova.exec(successCallback, failureCallback, "CordovaPlug", type, [data]);
};

var ua = navigator.userAgent;
if( ua.indexOf("Android") >= 0 )
{
var androidversion = parseFloat(ua.slice(ua.indexOf("Android")+8));
if (androidversion < 4)
{
}
else {
$("html, body").css("overflow","auto");
}
}

var busyInd;
var fldLangId = "eng";
var fldDeviceId = "43";
var fldAppId = "RS";
var fldFCDBSessionId = "";
var fldSessionId = "";
var fldRequestId = "";
var sessionid="";
var fldjsessionid = "";
var timeout = 600000;
var RBLModel;
var flduseragent = "Android";
var fldRoleId = "NOROLE";
var fldWebServerId = "YG";
var fldAppServerId = "ZZ";
var fldModule = "CH";
var serverConnError = "We are having difficulty in connecting to RBL Bank. Please retry after sometime.";
var NoResponseError = "We apologize this service is temporarily unavailable.Please try later.";
var NoDataError = "At this time, we are unable to carry out your request. Please try later.";
var Invalidmsg = "An incomplete message was received. Please retry or contact support.";
var lgnout = true;
var currentreguserid = ''
var dateformat = '';
curraccbalvalue = '';
var enckey = '123423';
var RQClientAPIVer = "4.1";

var activeP2Cpage=false;
var activeAddBenpage=false;
var aadharInsert = 0;
var selaccNo=0;

var activecreditpage=false;

var creditscoremodelcopy = null;

//Enum declaration for Beneficiery options
var BeneficieryOpt = { "InternalAddition":1, "RTGSAddition":2, "EXTERNALAddition":3, "InternalModification":4, "RTGSModification":5, "EXTERNALModification":6, "InternalDeletion":7, "RTGSDeletion":8, "EXTERNALDeletion":9 };

var BeneficieryOptState =  BeneficieryOpt.InternalAddition; //Set to 1 at the time of initialication
var BenePayeeList=[];
var selectedIndexOfBeneficiery = 0;
var IFSCList = [];
var AadharViewList=[];
var list1 = [];
var selectedIFSCIndex = 0;

//Entity structure for Beneficiary service
var objBeneficiery = {
		
		LoginUserId:"",
		RQRefNo:"",
		RQOTP:"",
 		BENE_SEQNO:"",
        OperationId:"",
        Action:"",
        BeneAccNo:"",
        BeneName:"",
        BeneNickName:"",
        BeneBankName:"",
        BeneBankBranch:"",
        BeneIFSCCode:"",
        BeneAddr1:"",
        BeneAddr2:"",
        BeneAccTyp:"",
        BeneAddr3:"",
        BeneAccCurr:"",
        BeneTransPWD:"",
        BeneSeqNo:""
};

var tptViewModelCopy;

function wlCommonInit(){

	/*
	 * Application is started in offline mode as defined by a connectOnStartup property in initOptions.js file.
	 * In order to begin communicating with Worklight Server you need to either:
	 * 
	 * 1. Change connectOnStartup property in initOptions.js to true. 
	 *    This will make Worklight framework automatically attempt to connect to Worklight Server as a part of application start-up.
	 *    Keep in mind - this may increase application start-up time.
	 *    
	 * 2. Use WL.Client.connect() API once connectivity to a Worklight Server is required. 
	 *    This API needs to be called only once, before any other WL.Client methods that communicate with the Worklight Server.
	 *    Don't forget to specify and implement onSuccess and onFailure callback functions for WL.Client.connect(), e.g:
	 *    
	 *    WL.Client.connect({
	 *    		onSuccess: onConnectSuccess,
	 *    		onFailure: onConnectFailure
	 *    });
	 *     
	 */
	// Common initialization code goes here
	//console.log("init callled");

	// Registeration call for notification
	
	//deviceId based service
	getDeviceId();
	
	PushNotificationWineCall();
	
	
	/*
	cordova.exec(successHandler, errorHandler, "PushPlugin", "register", [ {
		"senderID" : "136539293258",
		"ecb" : "onNotification"
	} ]); 
	*/ 
	busyInd = new WL.BusyIndicator('content', {text : 'Loading...'});
}

var onDeviceReady = function() {
	
	
	 document.addEventListener("backbutton", onBackKeyDown, false); 
	
};
document.addEventListener("deviceready", onDeviceReady, true);

$(window).on('hashchange', function() {
	setTimeout(function(){
		busyInd.hide();
	}, 150000);
});

function onBackKeyDown(e) {
	
	  activepage = window.location.hash;
	  //alert(activepage);
	// alert(activecreditpage);
	  
	//For aadhar Card Back functionality
	   if(activepage == '#aadharUpdate' && aadharInsert == 1)
	   {
		   var str="<fieldset class=' row_aadhar'> <label class='info_row_left black-text' style='padding-left:6px;' >Account Number</label>";
		     str+="<label style='padding:10px;' id='lblaccount' class='info_row_right black-text'></label><div class='clearfix'></div>"; 
		     str+="</fieldset><fieldset class=' fielderror row_aadhar' id='div123'><label class='black-text'>Enter Your Aadhaar Number</label>";
		     str+="<input type='number' name='aadharId' id='aadharId' maxlength='12' minlength='12' /></fieldset> <fieldset class=' fielderror row_aadhar'>";
		     str+="<label class='black-text'>Reconfirm Aadhaar Number </label><input type='number' name='confirmaadharId' id='confirmaadharId' minlength='12' maxlength='12' style='-webkit-text-security: disc;'  />";
		     str+="</fieldset><div class='bgded4d4 '> <div class='clearfix'></div><div class='container bgff6869'></div> ";
		     str+="<fieldset><input type='hidden' name='acctHolderName' id='acctHolderName' value=''/><input type='submit' value='Submit' class='btn'>";
		     str+="</fieldset><div class='clearfix' style='height: 30px;'></div></div>";
		     $("#tptCont1").html(str);
		    
		     document.getElementById("tptCont1").style.display="none";
		  
		     document.getElementById("tptCont").style.display="block";
		   
		     aadharInsert=0;
		     $("#note_joint_acc").css("display","block");	
	   }
	   
	   else if(activecreditpage){
		   
			  $("#contentData").load("Views/CreditScore/CreditScoreSelectAccount.html", null, function (response, status, xhr) {
					if (status != "error") {}
					$("#nftfundtrns").show();
					activecreditpage=false;
						ko.applyBindings(creditscoremodelcopy, $(".dynamic-page-content").get(0));
				});
		 
		  }
	   
	   
	   else if((activeP2Cpage && activepage == '#P2C_Details') || (activeAddBenpage)){
		  
		  e.preventDefault();
		    navigator.notification.confirm("Are you sure you want to exit this page ?", onCancel, "Alert", "Yes,Cancel"); 
	  }
	  
	  else if(activepage == '#PayeeWithinBank' || activepage == '#Payee_NEFT_IMPS' || activepage == '#Payee_RTGS' || activepage == '#Payee_Within' || activepage == '#Payee_Ext' || activepage == '#Payee_RTGS_Bene' || activepage == '#IFSC_Search')
	  {
		  
		  openBackPage(activepage);
	  }
	  
	  else if(activepage != MyParentPage){
	      window.location = MyParentPage;
	   }
	   else if(activepage == MyParentPage && MyParentPage !="#login" && MyParentPage !="#menu"){
			      e.preventDefault();
			      navigator.notification.confirm(
			          "Do you want to Logout?",
			          checkButtonSelection,
			          'Logout',
			          'Cancel,OK');
		}
			      
		   
	   
	  else if(activepage == '#menu'){
		  e.preventDefault();
		    navigator.notification.confirm("Are you sure you want to exit ?", onConfirm, "Alert", "Yes,No"); 
	  }

	  else{
		  //alert("Else Need to check");
	
	  }
	   $('.dw').detach(); $('.dwo').detach();
	}

	function onConfirm(button) {
		if (button == 1){
	        navigator.app.exitApp();
	    }
	}
	
	function onCancelCreditScore(button) {
		if (button == 1){
			activecreditpage=false;
			window.location.hash = '#rrasm01';
	    }
	}
	
	function onCancel(button) {
		if (button == 1){
			activeP2Cpage=false;
			window.location.hash = '#rrftr01';
	    }
	}

	function openLink(url){
			window.open(url,'_blank','location=no');  
			return false;
    }

$(document).on('touchstart','.logout .logout',function(e){
    e.preventDefault();
    navigator.notification.confirm(
	          "Do you want to Logout?",
	          checkButtonSelection,
	          'Logout',
	          'Cancel,OK');
});

function checkLogout(iValue){
    if (iValue == 2){
            window.location = "#logout";
        }
}

	
// WL.Device.getID(function (device) {
	// onsucess:alert (device.deviceID); 
// });	
	
// WL.Device.getNetworkInfo(function (networkInfo) {
	// alert (networkInfo.ipAddress); 
// });


var keyStr = "ABCDEFGHIJKLMNOP" +
"QRSTUVWXYZabcdef" +
"ghijklmnopqrstuv" +
"wxyz0123456789+/" +
"=";

function decode64(input){
	var output = "";
    var chr1, chr2, chr3 = "";
    var enc1, enc2, enc3, enc4 = "";
    var i = 0;	
	 
	 var base64test = /[^A-Za-z0-9\+\/\=]/g;
     if (base64test.exec(input)) {
        alert("There were invalid base64 characters in the input text.\n" +
              "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" +
              "Expect errors in decoding.");
     }
     input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

     do {
        enc1 = keyStr.indexOf(input.charAt(i++));
        enc2 = keyStr.indexOf(input.charAt(i++));
        enc3 = keyStr.indexOf(input.charAt(i++));
        enc4 = keyStr.indexOf(input.charAt(i++));

        chr1 = (enc1 << 2) | (enc2 >> 4);
        chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
        chr3 = ((enc3 & 3) << 6) | enc4;

        output = output + String.fromCharCode(chr1);

        if (enc3 != 64) {
           output = output + String.fromCharCode(chr2);
        }
        if (enc4 != 64) {
           output = output + String.fromCharCode(chr3);
        }

        chr1 = chr2 = chr3 = "";
        enc1 = enc2 = enc3 = enc4 = "";

     } while (i < input.length);
		//document.base64Form.theText.value=unescape(output);
		
     return unescape(output);
}

function encode64(input) {
	 
	input = escape(input);
    var output = "";
    var chr1, chr2, chr3 = "";
    var enc1, enc2, enc3, enc4 = "";
    var i = 0;

    do {
       chr1 = input.charCodeAt(i++);
       chr2 = input.charCodeAt(i++);
       chr3 = input.charCodeAt(i++);

       enc1 = chr1 >> 2;
       enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
       enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
       enc4 = chr3 & 63;

       if (isNaN(chr2)) {
          enc3 = enc4 = 64;
       } else if (isNaN(chr3)) {
          enc4 = 64;
       }

       output = output +
          keyStr.charAt(enc1) +
          keyStr.charAt(enc2) +
          keyStr.charAt(enc3) +
          keyStr.charAt(enc4);
       chr1 = chr2 = chr3 = "";
       enc1 = enc2 = enc3 = enc4 = "";
    } while (i < input.length);
     // document.base64Form.theText.value=output;
    return output;
    
}

$(document).ready(function(){

	$(document).on('touchstart','.header,.footer',function(e){	
		e.preventDefault();
		return false;
	});
	
	$(document).on('touchstart ','.back .back',function(e){	
		 //alert(location.pathname); 
		 //e.preventdefault(); //Added this
		 onBackKeyDown(e);
		  return false;
		
		
	});
	

	$(document).on('touchstart','.footer li',function(e){
	   e.stopPropagation();
	   $('.footer li').removeClass('active');   
	   $(this).addClass('active');
	   var currhref = $(this).find('a').attr('href');
	   if(currhref == "javascript:void(0);"){
		navigator.notification.alert('Coming Soon!');
	   }
	   else{
	   window.location = currhref;
	   }
	   e.preventDefault();
	});
	
});

/* Decimal Number check while typing */
function extractNumber(obj, decimalPlaces, allowNegative)
{
	var temp = obj.value;
	
	// avoid changing things if already formatted correctly
	var reg0Str = '[0-9]*';
	if (decimalPlaces > 0) {
		reg0Str += '\\.?[0-9]{0,' + decimalPlaces + '}';
	} else if (decimalPlaces < 0) {
		reg0Str += '\\.?[0-9]*';
	}
	reg0Str = allowNegative ? '^-?' + reg0Str : '^' + reg0Str;
	reg0Str = reg0Str + '$';
	var reg0 = new RegExp(reg0Str);
	if (reg0.test(temp)) return true;

	// first replace all non numbers
	var reg1Str = '[^0-9' + (decimalPlaces != 0 ? '.' : '') + (allowNegative ? '-' : '') + ']';
	var reg1 = new RegExp(reg1Str, 'g');
	temp = temp.replace(reg1, '');

	if (allowNegative) {
		// replace extra negative
		var hasNegative = temp.length > 0 && temp.charAt(0) == '-';
		var reg2 = /-/g;
		temp = temp.replace(reg2, '');
		if (hasNegative) temp = '-' + temp;
	}
	
	if (decimalPlaces != 0) {
		var reg3 = /\./g;
		var reg3Array = reg3.exec(temp);
		if (reg3Array != null) {
			// keep only first occurrence of .
			//  and the number of places specified by decimalPlaces or the entire string if decimalPlaces < 0
			var reg3Right = temp.substring(reg3Array.index + reg3Array[0].length);
			reg3Right = reg3Right.replace(reg3, '');
			reg3Right = decimalPlaces > 0 ? reg3Right.substring(0, decimalPlaces) : reg3Right;
			temp = temp.substring(0,reg3Array.index) + '.' + reg3Right;
		}
	}
	
	obj.value = temp;
}
function blockNonNumbers(obj, e, allowDecimal, allowNegative)
{
	var key;
	var isCtrl = false;
	var keychar;
	var reg;
		
	if(window.event) {
		key = e.keyCode;
		isCtrl = window.event.ctrlKey;
	}
	else if(e.which) {
		key = e.which;
		isCtrl = e.ctrlKey;
	}
	
	if (isNaN(key)) return true;
	
	keychar = String.fromCharCode(key);
	
	// check for backspace or delete, or if Ctrl was pressed
	if (key == 8 || isCtrl)
	{
		return true;
	}

	reg = /\d/;
	var isFirstN = allowNegative ? keychar == '-' && obj.value.indexOf('-') == -1 : false;
	var isFirstD = allowDecimal ? keychar == '.' && obj.value.indexOf('.') == -1 : false;
	
	return isFirstN || isFirstD || reg.test(keychar);
}
/* Decimal Number check while typing */


//load script
function loadScript(path){
	var s = document.createElement("script");
    s.type = "text/javascript";
    s.src = path;
    // Use any selector
    $("head").append(s);
}

function checkButtonSelection(iValue){
	  if (iValue == 2){
		  //e.stopPropagation();
		  
		    location.hash = '#logout';
		    //e.preventDefault();    
	      }
	  }


$.validator.addMethod('chkvalid', function(value, element, param) {
	if(value=="false"){
		return false;
	}
	else{
		return true;
	}
	}, 'Please Accept Terms & Conditions' );
	
	
	//Get current date

Date.prototype.currDate = function() {         
    
    var yyyy = this.getFullYear().toString();                                    
    var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based         
    var dd  = this.getDate().toString();             
                        
    return (dd[1]?dd:"0"+dd[0])  + '/' + (mm[1]?mm:"0"+mm[0]) + '/' +yyyy;
};  

function getCurrdate(){
d = new Date();
return d.currDate();
}
var noggers = function(string,invocationResult1){
			var str = '';
			var r = JSON.stringify(invocationResult1);
			invocationResult = string.invocationResult;
			//console.log("Login Success result "+invocationResult.RBL.STATUS.CODE);
			if (invocationResult.isSuccessful) {
				if(invocationResult.RBL){
					if(invocationResult.RBL.Response){
							if(invocationResult.RBL.STATUS.CODE==0){
							str = novels("resp0099*&^*&"+invocationResult.RBL.RequestParams.RQLoginPwd+"*&^%*&%"+invocationResult.RBL.RequestParams.RQTransSeq+""+invocationResult.RBL.RequestParams.RQOperationId+"Yesting^^&%^(*"+invocationResult.RBL.Response.customerid+"Hi89/*^%^&%%"+invocationResult.RBL.STATUS.CODE+"@snap$work()^&*#$HRbls"+invocationResult.RBL.STATUS.SEVERITY+"___---"+invocationResult.RBL.STATUS.CUSTOMERNAME+"Nice((("+invocationResult.RBL.RequestParams.RQLoginUserId+"9789%^^&$^%#"+invocationResult.RBL.RequestParams.RQOperationId);
							
							}else {
								str = novels("resp0099*&^*&"+invocationResult.RBL.RequestParams.RQLoginPwd+"*&^%*&%"+invocationResult.RBL.RequestParams.RQTransSeq+""+invocationResult.RBL.RequestParams.RQOperationId+"Yesting^^&%^(*"+invocationResult.RBL.Response.customerid+"Hi89/*^%^&%%"+invocationResult.RBL.STATUS.CODE+"@snap$work()^&*#$HRbls"+invocationResult.RBL.STATUS.SEVERITY+"___---"+invocationResult.RBL.STATUS.CUSTOMERNAME+invocationResult.RBL.STATUS.MESSAGE+"Nice((("+invocationResult.RBL.RequestParams.RQLoginUserId+"9789%^^&$^%#"+invocationResult.RBL.RequestParams.RQOperationId);	
								
							}
					}
				}
			}
			return str;
}
var novels = function (string) {
    
	
	function RotateLeft(lValue, iShiftBits) {
		return (lValue<<iShiftBits) | (lValue>>>(32-iShiftBits));
	}
 
	function AddUnsigned(lX,lY) {
		var lX4,lY4,lX8,lY8,lResult;
		lX8 = (lX & 0x80000000);
		lY8 = (lY & 0x80000000);
		lX4 = (lX & 0x40000000);
		lY4 = (lY & 0x40000000);
		lResult = (lX & 0x3FFFFFFF)+(lY & 0x3FFFFFFF);
		if (lX4 & lY4) {
			return (lResult ^ 0x80000000 ^ lX8 ^ lY8);
		}
		if (lX4 | lY4) {
			if (lResult & 0x40000000) {
				return (lResult ^ 0xC0000000 ^ lX8 ^ lY8);
			} else {
				return (lResult ^ 0x40000000 ^ lX8 ^ lY8);
			}
		} else {
			return (lResult ^ lX8 ^ lY8);
		}
 	}
 
 	function F(x,y,z) { return (x & y) | ((~x) & z); }
 	function G(x,y,z) { return (x & z) | (y & (~z)); }
 	function H(x,y,z) { return (x ^ y ^ z); }
	function I(x,y,z) { return (y ^ (x | (~z))); }
 
	function FF(a,b,c,d,x,s,ac) {
		a = AddUnsigned(a, AddUnsigned(AddUnsigned(F(b, c, d), x), ac));
		return AddUnsigned(RotateLeft(a, s), b);
	};
 
	function GG(a,b,c,d,x,s,ac) {
		a = AddUnsigned(a, AddUnsigned(AddUnsigned(G(b, c, d), x), ac));
		return AddUnsigned(RotateLeft(a, s), b);
	};
 
	function HH(a,b,c,d,x,s,ac) {
		a = AddUnsigned(a, AddUnsigned(AddUnsigned(H(b, c, d), x), ac));
		return AddUnsigned(RotateLeft(a, s), b);
	};
 
	function II(a,b,c,d,x,s,ac) {
		a = AddUnsigned(a, AddUnsigned(AddUnsigned(I(b, c, d), x), ac));
		return AddUnsigned(RotateLeft(a, s), b);
	};
 
	function ConvertToWordArray(string) {
		var lWordCount;
		var lMessageLength = string.length;
		var lNumberOfWords_temp1=lMessageLength + 8;
		var lNumberOfWords_temp2=(lNumberOfWords_temp1-(lNumberOfWords_temp1 % 64))/64;
		var lNumberOfWords = (lNumberOfWords_temp2+1)*16;
		var lWordArray=Array(lNumberOfWords-1);
		var lBytePosition = 0;
		var lByteCount = 0;
		while ( lByteCount < lMessageLength ) {
			lWordCount = (lByteCount-(lByteCount % 4))/4;
			lBytePosition = (lByteCount % 4)*8;
			lWordArray[lWordCount] = (lWordArray[lWordCount] | (string.charCodeAt(lByteCount)<<lBytePosition));
			lByteCount++;
		}
		lWordCount = (lByteCount-(lByteCount % 4))/4;
		lBytePosition = (lByteCount % 4)*8;
		lWordArray[lWordCount] = lWordArray[lWordCount] | (0x80<<lBytePosition);
		lWordArray[lNumberOfWords-2] = lMessageLength<<3;
		lWordArray[lNumberOfWords-1] = lMessageLength>>>29;
		return lWordArray;
	};
 
	function WordToHex(lValue) {
		var WordToHexValue="",WordToHexValue_temp="",lByte,lCount;
		for (lCount = 0;lCount<=3;lCount++) {
			lByte = (lValue>>>(lCount*8)) & 255;
			WordToHexValue_temp = "0" + lByte.toString(16);
			WordToHexValue = WordToHexValue + WordToHexValue_temp.substr(WordToHexValue_temp.length-2,2);
		}
		return WordToHexValue;
	};
 
	function Utf8Encode(string) {
		string = string.replace(/\r\n/g,"\n");
		var utftext = "";
 
		for (var n = 0; n < string.length; n++) {
 
			var c = string.charCodeAt(n);
 
			if (c < 128) {
				utftext += String.fromCharCode(c);
			}
			else if((c > 127) && (c < 2048)) {
				utftext += String.fromCharCode((c >> 6) | 192);
				utftext += String.fromCharCode((c & 63) | 128);
			}
			else {
				utftext += String.fromCharCode((c >> 12) | 224);
				utftext += String.fromCharCode(((c >> 6) & 63) | 128);
				utftext += String.fromCharCode((c & 63) | 128);
			}
 
		}
 
		return utftext;
	};
 
	var x=Array();
	var k,AA,BB,CC,DD,a,b,c,d;
	var S11=7, S12=12, S13=17, S14=22;
	var S21=5, S22=9 , S23=14, S24=20;
	var S31=4, S32=11, S33=16, S34=23;
	var S41=6, S42=10, S43=15, S44=21;
 
	string = Utf8Encode(string);
 
	x = ConvertToWordArray(string);
 
	a = 0x67452301; b = 0xEFCDAB89; c = 0x98BADCFE; d = 0x10325476;
 
	for (k=0;k<x.length;k+=16) {
		AA=a; BB=b; CC=c; DD=d;
		a=FF(a,b,c,d,x[k+0], S11,0xD76AA478);
		d=FF(d,a,b,c,x[k+1], S12,0xE8C7B756);
		c=FF(c,d,a,b,x[k+2], S13,0x242070DB);
		b=FF(b,c,d,a,x[k+3], S14,0xC1BDCEEE);
		a=FF(a,b,c,d,x[k+4], S11,0xF57C0FAF);
		d=FF(d,a,b,c,x[k+5], S12,0x4787C62A);
		c=FF(c,d,a,b,x[k+6], S13,0xA8304613);
		b=FF(b,c,d,a,x[k+7], S14,0xFD469501);
		a=FF(a,b,c,d,x[k+8], S11,0x698098D8);
		d=FF(d,a,b,c,x[k+9], S12,0x8B44F7AF);
		c=FF(c,d,a,b,x[k+10],S13,0xFFFF5BB1);
		b=FF(b,c,d,a,x[k+11],S14,0x895CD7BE);
		a=FF(a,b,c,d,x[k+12],S11,0x6B901122);
		d=FF(d,a,b,c,x[k+13],S12,0xFD987193);
		c=FF(c,d,a,b,x[k+14],S13,0xA679438E);
		b=FF(b,c,d,a,x[k+15],S14,0x49B40821);
		a=GG(a,b,c,d,x[k+1], S21,0xF61E2562);
		d=GG(d,a,b,c,x[k+6], S22,0xC040B340);
		c=GG(c,d,a,b,x[k+11],S23,0x265E5A51);
		b=GG(b,c,d,a,x[k+0], S24,0xE9B6C7AA);
		a=GG(a,b,c,d,x[k+5], S21,0xD62F105D);
		d=GG(d,a,b,c,x[k+10],S22,0x2441453);
		c=GG(c,d,a,b,x[k+15],S23,0xD8A1E681);
		b=GG(b,c,d,a,x[k+4], S24,0xE7D3FBC8);
		a=GG(a,b,c,d,x[k+9], S21,0x21E1CDE6);
		d=GG(d,a,b,c,x[k+14],S22,0xC33707D6);
		c=GG(c,d,a,b,x[k+3], S23,0xF4D50D87);
		b=GG(b,c,d,a,x[k+8], S24,0x455A14ED);
		a=GG(a,b,c,d,x[k+13],S21,0xA9E3E905);
		d=GG(d,a,b,c,x[k+2], S22,0xFCEFA3F8);
		c=GG(c,d,a,b,x[k+7], S23,0x676F02D9);
		b=GG(b,c,d,a,x[k+12],S24,0x8D2A4C8A);
		a=HH(a,b,c,d,x[k+5], S31,0xFFFA3942);
		d=HH(d,a,b,c,x[k+8], S32,0x8771F681);
		c=HH(c,d,a,b,x[k+11],S33,0x6D9D6122);
		b=HH(b,c,d,a,x[k+14],S34,0xFDE5380C);
		a=HH(a,b,c,d,x[k+1], S31,0xA4BEEA44);
		d=HH(d,a,b,c,x[k+4], S32,0x4BDECFA9);
		c=HH(c,d,a,b,x[k+7], S33,0xF6BB4B60);
		b=HH(b,c,d,a,x[k+10],S34,0xBEBFBC70);
		a=HH(a,b,c,d,x[k+13],S31,0x289B7EC6);
		d=HH(d,a,b,c,x[k+0], S32,0xEAA127FA);
		c=HH(c,d,a,b,x[k+3], S33,0xD4EF3085);
		b=HH(b,c,d,a,x[k+6], S34,0x4881D05);
		a=HH(a,b,c,d,x[k+9], S31,0xD9D4D039);
		d=HH(d,a,b,c,x[k+12],S32,0xE6DB99E5);
		c=HH(c,d,a,b,x[k+15],S33,0x1FA27CF8);
		b=HH(b,c,d,a,x[k+2], S34,0xC4AC5665);
		a=II(a,b,c,d,x[k+0], S41,0xF4292244);
		d=II(d,a,b,c,x[k+7], S42,0x432AFF97);
		c=II(c,d,a,b,x[k+14],S43,0xAB9423A7);
		b=II(b,c,d,a,x[k+5], S44,0xFC93A039);
		a=II(a,b,c,d,x[k+12],S41,0x655B59C3);
		d=II(d,a,b,c,x[k+3], S42,0x8F0CCC92);
		c=II(c,d,a,b,x[k+10],S43,0xFFEFF47D);
		b=II(b,c,d,a,x[k+1], S44,0x85845DD1);
		a=II(a,b,c,d,x[k+8], S41,0x6FA87E4F);
		d=II(d,a,b,c,x[k+15],S42,0xFE2CE6E0);
		c=II(c,d,a,b,x[k+6], S43,0xA3014314);
		b=II(b,c,d,a,x[k+13],S44,0x4E0811A1);
		a=II(a,b,c,d,x[k+4], S41,0xF7537E82);
		d=II(d,a,b,c,x[k+11],S42,0xBD3AF235);
		c=II(c,d,a,b,x[k+2], S43,0x2AD7D2BB);
		b=II(b,c,d,a,x[k+9], S44,0xEB86D391);
		a=AddUnsigned(a,AA);
		b=AddUnsigned(b,BB);
		c=AddUnsigned(c,CC);
		d=AddUnsigned(d,DD);
	}
 
	var temp = WordToHex(a)+WordToHex(b)+WordToHex(c)+WordToHex(d);
 
	return temp.toLowerCase();
}
function booksStore(str) {
    var encoded = "";
    for (i=0; i<str.length;i++) {
        var a = str.charCodeAt(i);
        var b = a ^ 123423;    // bitwise XOR with any number, e.g. 123
        encoded = encoded+String.fromCharCode(b);
    }
    return encoded;
}
/* JavaScript content from js/main.js in folder android */
// This method is invoked after loading the main HTML and successful initialization of the Worklight runtime.
function wlEnvInit(){
    wlCommonInit();
    // Environment initialization code goes here
}
/* JavaScript content from js/main.js in folder android */
// This method is invoked after loading the main HTML and successful initialization of the Worklight runtime.
function wlEnvInit(){
    wlCommonInit();
    // Environment initialization code goes here
}

 function  encryptXorNumber(str,enckey) {
            var encoded = "";
            for (i=0; i<str.length;i++) {
                var a = str.charCodeAt(i);
                var b = a ^ enckey;    // bitwise XOR with any number, e.g. 123
                encoded = encoded+String.fromCharCode(b);
            }
            return encoded;
        }

function dateformatting(month){ 
if(month != '' || month != undefined){
	day = month.split('-')[2];
	monthnameval = month.split('-')[1];
	year = month.split('-')[0];
	//year = year.slice(-2);
			if(monthnameval == '01') monthnameval = 'Jan';
            if(monthnameval == '02') monthnameval = 'Feb';
            if(monthnameval == '03') monthnameval = 'Mar';
            if(monthnameval == '04') monthnameval = 'Apr';
            if(monthnameval == '05') monthnameval = 'May';
            if(monthnameval == '06') monthnameval = 'Jun';
            if(monthnameval == '07') monthnameval = 'Jul';
            if(monthnameval == '08') monthnameval = 'Aug';
            if(monthnameval == '09') monthnameval = 'Sep';
            if(monthnameval == '10') monthnameval = 'Oct';
            if(monthnameval == '11') monthnameval = 'Nov';
            if(monthnameval == '12') monthnameval = 'Dec';
	dateformat = day+" "+monthnameval+" "+year;
}
}



var CustIDWine='undefined';
var wineTag=null;
var registerUserValueNew=null;



function checkForCustID()
{
if(localStorage.getItem("RegisteredUser"))
	{		 
		registerUserValueNew = encryptXorNumber(localStorage.getItem("RegisteredUser"),enckey);
		
		    if (registerUserValueNew != "" || registerUserValueNew != null)
		   {
		    	CustIDWine = registerUserValueNew;
		    	wineTag="moBank_All,moBank_5.4,moBank_"+CustIDWine;
		    	
			// alert("custid after reg : "+CustIDWine);
			 //alert("wine tags after reg : "+wineTag);
		  }
	}
		    
else
	{
		wineTag="moBank_All,moBank_5.4";
		
		// alert("custid before reg : "+CustIDWine);
		//alert("wine tags before reg : "+wineTag);
	}
}



function PushNotificationWineCall()
{
	
	//alert("wine call start");

cordova.exec(successHandler, errorHandler, "PushPlugin", "register", [ {
	"senderID" : "136539293258",
	"ecb" : "onNotification"
} ]); 
}


//handle GCM notifications for Android
function onNotification(e) {

//	alert("custid ="+CustIDWine);	
//alert("wine tags during notification function calling==="+wineTag);
	
	switch (e.event) {
	case 'registered':
		if (e.regid.length > 0)
		{
			checkForCustID();
			//alert(e.regid);
			//console.log("reg id:::::"+e.regid);
			//alert(e.regid);
			var today = new Date();
			var utc = ISODateString(today);
			//alert(utc);
			//alert(e.regid);
			//console.log("date:::::"+utc);
			//Call Adapter for registeration on sever
			//console.log("Juber regID = " + e.regid);
			if(window.navigator.onLine)
			{
				
				//console.log("inside notification");
				var invocationData = {
					adapter : "WINEAdapter",
					procedure : "setTokenUserData",
					parameters : ['Android','moBank1947',e.regid,'MoBank',window.device.uuid,
					              window.device.version,CustIDWine,'empty@rbl.com','5.4',utc,window.device.model,wineTag],
					compressResponse : true
				};
				
			/*	
				var invocationData = {
						adapter : "WINEAdapter",
						procedure : "setTokenUserData",
						parameters : ['Android','moBank1947',e.regid,'undefined',window.device.uuid,
						              window.device.version,'RBL','empty@rbl.com','empty',utc,window.device.model,"moBank_CUG"],
						compressResponse : true
					};
				*/
			
				//alert("adapter call started");
				
				WL.Client.invokeProcedure(invocationData, {
					onSuccess : pushSuccessHandler,
					onFailure : pushErrorHandler,	    		
					timeout: timeout
				});
				
			}
		}
		break;
	case 'message':
		// if this flag is set, this notification happened while we were in the
		// foreground.
		// you might want to play a sound to get the user's attention, throw up
		// a dialog, etc.
		
		//receiveCounter(e);
		
		if (e.foreground) 
		{
			
			//receiveCounter(e);
			//Todo Display Alert View
			//console.log("WINE message = " +  e.payload.payload );
			//navigator.notification.alert( e.payload.payload);
			//console.log(e.payload.payload);
			WL.SimpleDialog.show(
					"Notification", e.payload.payload, 
					[{text: "OK", handler: function() {WL.Logger.debug("First button pressed");}}]
				);
			
		} else 
		{ 
		//	receiveCounter(e);
			// otherwise we were launched because the user touched a
			// notification in the notification tray.
			if (e.coldstart) 
			{
				//readCounter(e);
				//console.log("WINE message = " +  e.payload.payload );
			} else 
			{
				//readCounter(e);
				//console.log("WINE message = " +  e.payload.payload );
				// --BACKGROUND NOTIFICATION--;
			}
		}
		break;

	case 'error':
		//Error Handling
		break;

	default:
		//Unknown event received
		break;
	}
}



function receiveCounter(e)
{
	try{
var type = e.payload.deeplinking;
var num = type.replace(/^\D+|\D+$/g, "");
//alert(num);
		var invocationData = {
				adapter : "WINEAdapter",
				procedure : "receiveDeepLinking",
				parameters : [num],
				compressResponse : true
			};
		
	
		WL.Client.invokeProcedure(invocationData, {
			onSuccess : pushSuccessHandler,
			onFailure : pushErrorHandler,	    		
			timeout: timeout
		});
	}
	catch(Err){
		//alert(Err.message);
	}	
}


function readCounter(e)
{
	try{
		
		var type = e.payload.deeplinking;
		var num = type.replace(/^\D+|\D+$/g, "");

		var invocationData = {
				adapter : "WINEAdapter",
				procedure : "readDeepLinking",
				parameters : [num],
				compressResponse : true
			};
		
	
		WL.Client.invokeProcedure(invocationData, {
			onSuccess : pushSuccessHandler,
			onFailure : pushErrorHandler,	    		
			timeout: timeout
		});
	}
	catch(Err){
		//alert(Err.message);
	}	
}




function successHandler(result) {
	//Success Handler for push notification
}

function errorHandler(error) {
	//error handler for push notification
}

function pushSuccessHandler(result) {
	//console.log("WINE success = "+JSON.stringify(result));
	//alert("WINE success = "+JSON.stringify(result));
	//alert("Wine Success");
	//Success Handler for push notification
}

function pushErrorHandler(error) {
	//error handler for push notification
	//console.log("WINE error = "+JSON.stringify(error));
	//alert("WINE error = "+JSON.stringify(error));
	//alert("Wine Error");
}

function ISODateString(d){
  function pad(n){return n<10 ? '0'+n : n}
  return d.getFullYear()+'-'
  + pad(d.getMonth()+1)+'-'
  + pad(d.getDate())+'T'
  + pad(d.getHours())+':'
  + pad(d.getMinutes())+':'
  + pad(d.getSeconds())+'.000Z'
}

//P2C Codes

var msg = 'You can now transfer funds to a payee of your choice using mobile number and without knowing their bank account details. A maximum of ₹ 10,000 per day can be transferred from your account.';

function CustomAlert(){
    this.render = function(dialog){
        var winW = window.innerWidth;
        var winH = window.innerHeight;
        var dialogoverlay = document.getElementById('dialogoverlay');
        var dialogbox = document.getElementById('dialogbox');
        dialogoverlay.style.display = "block";
        dialogoverlay.style.height = winH+"px";
     
        dialogbox.style.left = "10%";
        dialogbox.style.top = "20%";
        dialogbox.style.display = "block";
        document.getElementById('dialogboxhead').innerHTML = "P2C";
        document.getElementById('dialogboxbody').innerHTML = dialog;
        document.getElementById('dialogboxfoot').innerHTML = '<button class= "btn-ok">OK</button>';
       
    }
}

function CustomAlertNew(){
    this.render = function(dialog){
        var winW = window.innerWidth;
        var winH = window.innerHeight;
        var dialogoverlaynew = document.getElementById('dialogoverlaynew');
        var dialogboxnew = document.getElementById('dialogboxnew');
        dialogoverlaynew.style.display = "block";
        dialogoverlaynew.style.height = winH+"px";
     
        document.getElementById('dialogboxbodynew').scrollIntoView(true);
        
        dialogboxnew.style.left = "3%";
        dialogboxnew.style.top = "17%";
        dialogboxnew.style.display = "block";
        //document.getElementById('dialogboxheadnew').innerHTML = "IFSC Search";
       
    }
}

$(document).on('click', '.closePopup', function(){ 
	
	document.getElementById('dialogboxnew').style.display = "none";
	document.getElementById('dialogoverlaynew').style.display = "none";
});


$(document).on('click', '.btn-ok', function(){ 
	
	document.getElementById('dialogbox').style.display = "none";
	document.getElementById('dialogoverlay').style.display = "none";
	
	localStorage.setItem("status","olduser");
});

//Function to empty all the data
function makeEmptyStructureForBeneficiery()
{
	objBeneficiery.LoginUserId = "";
	objBeneficiery.RQRefNo = "";
	objBeneficiery.RQOTP = "";
	
	objBeneficiery.BENE_SEQNO = "";
	objBeneficiery.OperationId = "";
	objBeneficiery.Action = "";
	objBeneficiery.BeneAccNo = "";
	objBeneficiery.BeneName = "";
	objBeneficiery.BeneNickName = "";
	objBeneficiery.BeneBankName = "";
	objBeneficiery.BeneBankBranch = "";
	objBeneficiery.BeneIFSCCode = "";
	objBeneficiery.BeneAddr1 = "";
	objBeneficiery.BeneAddr2 = "";
	objBeneficiery.BeneAccTyp = "";
	objBeneficiery.BeneAddr3 = "";
	objBeneficiery.BeneAccCurr = "";
	objBeneficiery.BeneTransPWD = "";
	objBeneficiery.BeneSeqNo = "";
}


function openBackPage(page)
{
	if(tptViewModelCopy.previousPage == "Payee_Home")
	{
		tptViewModelCopy = null;
		window.location = "#Payee_Home";
	}
	else if(tptViewModelCopy.previousPage == "Payee_Modify_Home")
	{
		window.location = "#ModifyPayee_Home";
	}
	else if(tptViewModelCopy.previousPage == "Payee_List")
	{
//		$(".h_title").html("Third Party Fund Transfer");
    	loadViewModel("PayeeList");
	}
	else if(tptViewModelCopy.previousPage == "Payee_Modify_Details")
	{
//		$(".h_title").html("Third Party Fund Transfer");

		loadViewModel("Payee_Beneficiery_Details");
	}
	else if(tptViewModelCopy.previousPage == "Payee_Within")
		{
		
		$("#contentData").load("Views/Payee/PayeeWithinBank.html", null, function (response, status, xhr) {
            if (status != "error") {}
            BeneficieryOptState =  BeneficieryOpt.InternalAddition;
            tptViewModelCopy.openPage(page);
            ko.applyBindings(tptViewModelCopy, $(".content").get(0)); 
            
            tptViewModelCopy.previousPage = "Payee_Home";
        });
		
		
		
		}
	//'#Payee_NEFT_IMPS' || activepage == '#Payee_RTGS'
	else if(tptViewModelCopy.previousPage == "Payee_RTGS")
	{
		
		$("#contentData").load("Views/Payee/Payee_RTGS.html", null, function (response, status, xhr) {
            if (status != "error") {}
            BeneficieryOptState =  BeneficieryOpt.RTGSAddition;
            tptViewModelCopy.openPage(page);
            ko.applyBindings(tptViewModelCopy, $(".content").get(0));
            tptViewModelCopy.previousPage = "Payee_Home";
        });	
	}
	else if(tptViewModelCopy.previousPage == "Payee_NEFT")
	{
		
		$("#contentData").load("Views/Payee/Payee_NEFT_IMPS.html", null, function (response, status, xhr) {
            if (status != "error") {}
            BeneficieryOptState =  BeneficieryOpt.EXTERNALAddition;
            tptViewModelCopy.openPage(page);
            ko.applyBindings(tptViewModelCopy, $(".content").get(0));
            tptViewModelCopy.previousPage = "Payee_Home";
        });	
	
	}
	else if(tptViewModelCopy.previousPage == "Payee_RTGS_IFSC")
	{
		window.location = '#Payee_RTGS';
			
	}
	else if(tptViewModelCopy.previousPage == "Payee_NEFT_IFSC")
	{
		window.location = '#Payee_NEFT_IMPS';
	}
	
	else if (tptViewModelCopy.previousPage == "Success_Page"){
		
		window.location = "#rrftr01";
	}
	else {
		
		tptViewModelCopy.openPage(page);
	}

	
}

//deviceId based service
function getDeviceId(){
	WL.Device.getID({onSuccess : function(o) { 
		deviceId = o.deviceID; 
		}, onFailure : function(e) { 
		console.log("Error getting ID: " + e); 
		}}); 
}

function ifscListClick(i)
{
	activepage = window.location.hash;
	selectedIFSCIndex = i;
	openBackPage(activepage);
	
}

