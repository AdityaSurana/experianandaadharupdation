/* JavaScript content from js/appState.js in folder common */
var loadViewModel = function(viewId, param) {

	$('.menutt').attr('id', viewId);
	$("#note_joint_acc").css("display","none");
	if (viewId == 'login') {
		$("#contentData").load("Views/login/login.html", null,
				function(response, status, xhr) {
					if (status != "error") {
					}
					var model = new loginViewModel(param);
					ko.applyBindings(model, $(".dynamic-page-content").get(0));

				});
		// }
		// else if(viewId == 'logincustpass'){
		// $("#contentData").load("Views/login/loginCustPass.html", null,
		// function (response, status, xhr) {
		// if (status != "error") {}

		// var model = new loginViewModel(param);
		// model.showCustDetails();
		// ko.applyBindings(model, $(".dynamic-page-content").get(0));
		// });
		// viewId == 'rrasm01';
	} else if (viewId == 'new_user_reg') {
		$("#contentData").load("Views/Menu/new_user_reg.html", null,
				function(response, status, xhr) {
					if (status != "error") {
					}
					var model = new loginViewModel(param);
					ko.applyBindings(model, $(".dynamic-page-content").get(0));

				});
	} else if (viewId == 'new_user_reg1') {
		$("#contentData").load("Views/Menu/new_user_reg1.html", null,
				function(response, status, xhr) {
					if (status != "error") {
					}
					var model = new loginViewModel(param);
					// model.new_user_reg1Submit();
					ko.applyBindings(model, $(".dynamic-page-content").get(0));

				});
	} else if (viewId == 'new_user_reg2') {
		$("#contentData").load("Views/Menu/new_user_reg2.html", null,
				function(response, status, xhr) {
					if (status != "error") {
					}
					var model = new loginViewModel(param);
					ko.applyBindings(model, $(".dynamic-page-content").get(0));

				});
	} else if (viewId == 'new_user_reg3') {
		$("#contentData").load("Views/Menu/new_user_reg3.html", null,
				function(response, status, xhr) {
					if (status != "error") {
					}
					var model = new loginViewModel(param);
					ko.applyBindings(model, $(".dynamic-page-content").get(0));

				});
	} else if (viewId == 'PersonalReg') {
		$("#contentData").load("Views/Menu/PersonalReg.html", null,
				function(response, status, xhr) {
					if (status != "error") {
					}
					var model = new loginViewModel(param);
					ko.applyBindings(model, $(".dynamic-page-content").get(0));
				});
	} else if (viewId == 'DebitReg') {
		$("#contentData").load("Views/Menu/DebitReg.html", null,
				function(response, status, xhr) {
					if (status != "error") {
					}
					var model = new loginViewModel(param);
					ko.applyBindings(model, $(".dynamic-page-content").get(0));
				});
	} else if (viewId == 'ChangePwd') {
		$("#contentData").load("Views/login/ChangePwd.html", null,
				function(response, status, xhr) {
					if (status != "error") {
					}
					var model = new loginViewModel(param);
					ko.applyBindings(model, $(".dynamic-page-content").get(0));
				});
	} else if (viewId == 'PwdChangeSucess') {
		$("#contentData").load("Views/login/PwdChangeSucess.html", null,
				function(response, status, xhr) {
					if (status != "error") {
					}
					var model = new loginViewModel(param);
					ko.applyBindings(model, $(".dynamic-page-content").get(0));
				});
	} else if (viewId == 'contact') {
		$("#contentData").load("Views/Menu/contact.html", null,
				function(response, status, xhr) {
				});
	} else if (viewId == 'accountSummary' || viewId == 'rrasm01') {

		// var model = new AccountsViewModel(param);
		// model.getAccountSummary();
		var model = new AccountsViewModel(param);
		model.getAccountSummary();
		$('.footer li').removeClass('active');
		$('.footer').find('#rrasm01I').addClass('active');

	} else if (viewId == 'accountStatment') {
		// var accmodel = new AccountsViewModel();
		// accmodel.viewSelectedAccountStatement();
		var model = new AccountsViewModel(param);
		model.viewSelectedAccountStatement();

	} else if (viewId == 'rrsin01') {
		$("#contentData").load("Views/Accounts/rrsin01.html", null,
				function(response, status, xhr) {
					if (status != "error") {
					}

					var model = new AccountsViewModel(param);
					model.getAccountsList();
					ko.applyBindings(model, $(".content").get(0));
				});
	} else if (viewId == 'rrsin02') {
		$("#contentData").load("Views/Accounts/rrsin02.html", null,
				function(response, status, xhr) {
					if (status != "error") {
					}

					var model = new AccountsViewModel(param);
					model.ViewAccountStatements();
					ko.applyBindings(model, $(".content").get(0));
				});
	}

	else if (viewId == 'logout') {
		var model = new loginViewModel(param);
		model.logout();
		ko.applyBindings(model, $(".dynamic-page-content").get(0));
	} else if (viewId == 'Home') {
		// var model = new loginViewModel(param);
		$("#contentData").load("Views/login/Home.html", null,
				function(response, status, xhr) {

				});
	} else if (viewId == 'rrtpi01') {
		var model = new TPTViewModel(param);
		$("#contentData").load("Views/TPT/rrtpi01.html", null,
				function(response, status, xhr) {
					if (status != "error") {
					}
					ko.applyBindings(model, $(".dynamic-page-content").get(0));
				});
	} else if (viewId == 'rrrtg04') {
		var model = new TPTViewModel(param);
		model.rrrtg04Page();
	} else if (viewId == 'rrrtg05') {
		var model = new TPTViewModel(param);
		model.rrrtg05Page();
	}

	else if (viewId == 'rrtpn04') {
		var model = new TPTViewModel(param);
		model.tpn04Page();
	} else if (viewId == 'rrtpn05') {
		var model = new TPTViewModel(param);
		model.tpn05Page();
	} else if (viewId == 'impsFndTrnsList') {
		$("#contentData").load("Views/Accounts/impsFndTrnsList.html", null,
				function(response, status, xhr) {
					if (status != "error") {
					}
					var model = new TPTViewModel(param);
					// var model = new AccountsViewModel(param);

					ko.applyBindings(model, $(".content").get(0));
				});
	} else if (viewId == 'rrift01') {
		var model = new TPTViewModel(param);
		model.rrift01Page();
	} else if (viewId == 'rrift02') {
		var model = new TPTViewModel(param);
		model.rrift02Page();
	} else if (viewId == 'rrift03') {
		var model = new TPTViewModel(param);
		model.callVFT03();

	} else if (viewId == 'rrcbr02') {
		var model = new AccountsViewModel(param);
		model.CheckBReq();

	} else if (viewId == 'rrcbr03') {
		var model = new AccountsViewModel(param);
		model.CheckBReqCmfrmsub();

	} else if (viewId == 'rrcbr04') {
		var model = new AccountsViewModel(param);
		model.CheckBReqCmfrmsubSucces();

	} else if (viewId == 'rrcsi01') {
		$("#contentData").load("Views/Accounts/rrcsi01.html", null,
				function(response, status, xhr) {
					if (status != "error") {
					}
					var model = new AccountsViewModel(param);
					model.ViewCheckStatements();
					ko.applyBindings(model, $(".content").get(0));
				});
	} else if (viewId == 'rrcsi02') {
		$("#contentData").load("Views/Accounts/rrcsi02.html", null,
				function(response, status, xhr) {
					if (status != "error") {
					}

					var model = new AccountsViewModel(param);
					model.GetCheckStatus();
					ko.applyBindings(model, $(".content").get(0));
				});
	} else if (viewId == 'rrsch01') {
		var model = new AccountsViewModel(param);
		model.StopPaYmntcheck();

	} else if (viewId == 'rrsch02') {
		var model = new AccountsViewModel(param);
		model.StopPaYmntcheckRes();

	} else if (viewId == 'rrsch03') {
		var model = new AccountsViewModel(param);
		model.StopPaYmntcheckRes3();

	} else if (viewId == 'billpayment') {
		$("#contentData").load("Views/recharge/billpayment.html", null,
				function(response, status, xhr) {
					if (status != "error") {
					}
					$('.footer li').removeClass('active');
					$('.footer').find('#rrblp01I').addClass('active');
				});
	} else if (viewId == 'rrblp01') {
		var model = new BillPay(param);
		model.rrblp01Page();

	} else if (viewId == 'rrblp02') {
		var model = new BillPay(param);
		model.rrblp02Page();

	} else if (viewId == 'rrblp03') {
		var model = new BillPay(param);
		model.rrblp03Page();

	} else if (viewId == 'rrblp04') {
		var model = new BillPay(param);
		model.rrblp04Page();
	} else if (viewId == 'rrpmb01') {
		var model = new BillPay(param);
		model.rrpmb01Page();
	} else if (viewId == 'rrpmb02') {
		var model = new BillPay(param);
		model.rrpmb02Page();
	} else if (viewId == 'rrdth01') {
		var model = new BillPay(param);
		model.rrdth01Page();
	} else if (viewId == 'rrdth02') {
		var model = new BillPay(param);
		model.rrdth02Page();
	} else if (viewId == 'rrcrs01') {
		var model = new BillPay(param);
		model.rrcrs01Page();
	} else if (viewId == 'rrcrs02') {
		var model = new BillPay(param);
		model.rrcrs02Page();
	} else if (viewId == 'rrtpt03' || viewId == 'rrtpt04') {
		var tptmodel = new TPTViewModel(param);

		if (viewId == 'rrtpt03')
			tptmodel.callTPT03();
		else if (viewId == 'rrtpt04')
			tptmodel.callTPT04();
	} else if (viewId == 'FD_opening') {
		var model = new AccountsViewModel(param);
		model.getFDOpenSchemes();
		// $("#contentData").load("Views/Deposits/fd-account-opening.html",
		// null, function (response, status, xhr) {
		// if (status != "error") {}

		// ko.applyBindings(model, $(".content").get(0));
		// });

	} else if (viewId == 'FD_summary') {
		$("#contentData").load("Views/Deposits/fd-account-summary.html", null,
				function(response, status, xhr) {
					if (status != "error") {
					}

					ko.applyBindings(model, $(".content").get(0));
				});

	} else if (viewId == 'FD_Acnts') {
		var model = new AccountsViewModel(param);
		model.getFDAccountsList();
	} else if (viewId == 'RD_Accounts') {
		$("#contentData").load("Views/Deposits/rd-accounts.html", null,
				function(response, status, xhr) {
					if (nord == 'true') {
						$('#frmrdacnts').hide();
						$('#accExitsMsg').show();
					}
					if (status != "error") {
					}
					ko.applyBindings(model, $(".content").get(0));
				});
	} else if (viewId == 'Loan_Acnts') {
		$("#contentData").load("Views/Deposits/loan-accounts.html", null,
				function(response, status, xhr) {
					if (nofd == 'true') {
						$('#frmfdacnts').hide();
						$('#accExitsMsg').show();
					}
					if (status != "error") {
					}
					ko.applyBindings(model, $(".content").get(0));
				});
	} else if (viewId == 'Loan_Acnt') {
		// $("#contentData").load("Views/Deposits/rd-accounts.html", null,
		// function (response, status, xhr) {
		// if (status != "error") {}

		// ko.applyBindings(model, $(".content").get(0));
		// });
		var model = new AccountsViewModel(param);
		model.getLoanAccountsList();

	} else if (viewId == 'FD_Accounts') {
		$("#contentData").load("Views/Deposits/fd-accounts.html", null,
				function(response, status, xhr) {
					if (nofd == 'true') {
						$('#frmfdacnts').hide();
						$('#accExitsMsg').show();
					}
					if (status != "error") {
					}
					ko.applyBindings(model, $(".content").get(0));
				});
	} else if (viewId == 'RD_opening') {
		var model = new AccountsViewModel(param);
		model.getRDOpenPage();

	} else if (viewId == 'RD_summary') {
		$("#contentData").load("Views/Deposits/rd-account-summary.html", null,
				function(response, status, xhr) {
					if (status != "error") {
					}

					ko.applyBindings(model, $(".content").get(0));
				});
	} else if (viewId == 'Loan_summary') {
		$("#contentData").load("Views/Deposits/loan-account-summary.html",
				null, function(response, status, xhr) {
					if (status != "error") {
					}

					ko.applyBindings(model, $(".content").get(0));
				});
	} else if (viewId == 'RD_Acnts') {
		// $("#contentData").load("Views/Deposits/rd-accounts.html", null,
		// function (response, status, xhr) {
		// if (status != "error") {}

		// ko.applyBindings(model, $(".content").get(0));
		// });
		var model = new AccountsViewModel(param);
		model.getRDAccountsList();

	} else if (viewId == 'aadhar') {
		var model = new loginViewModel(param);
		model.Aadhar();
	} else if (viewId == 'rrftr02' || viewId == 'rrftr03') {
		var model = new AccountsViewModel(param);
		if (viewId == 'rrftr02')
			model.callrrftr02();
		else if (viewId == 'rrftr03')
			model.callrrftr03();
	}

	else if (viewId == 'rrtpv01' || viewId == 'rrtpv02') {
		var tptmodel = new TPTViewModel(param);
		if (viewId == 'rrtpv01')
			tptmodel.callTPV01();
		if (viewId == 'rrtpv02') {
			$("#contentData").load("Views/TPT/rrtpv02.html", null,
					function(response, status, xhr) {
						if (status != "error") {
						}
						var model = new TPTViewModel(param);
						model.rrtpv02();
						ko.applyBindings(model, $(".content").get(0));
					});
		}
	} else if (viewId == 'rrp2a01' || viewId == 'rrp2a02'
			|| viewId == 'rrp2a03') {
		var tptmodel = new TPTViewModel(param);
		if (viewId == 'rrp2a01')
			tptmodel.callP2A01();
		if (viewId == 'rrp2a02')
			tptmodel.callP2A02();
		if (viewId == 'rrp2a03')
			tptmodel.callVFT03();
	}
	// credit card section
	else if (viewId == 'CreditCard') {
		$('.footer li').removeClass('active');
		$('.footer').find('#r1I').addClass('active');
		var model = new CreditCardViewModel(param);
		model.getCCaccounts();
	} else if (viewId == 'Credit_card_acnts') {
		$("#contentData").load("Views/Credit/CreditHome.html", null,
				function(response, status, xhr) {
					if (status != "error") {
					}
					ko.applyBindings(model, $(".content").get(0));
				});
	} else if (viewId == 'ccsummary') {
		$("#contentData").load("Views/Credit/CardSummary.html", null,
				function(response, status, xhr) {
					if (status != "error") {
					}
					ko.applyBindings(model, $(".content").get(0));
				});
	} else if (viewId == 'CC_register') {
		$("#contentData").load("Views/Credit/CC-register.html", null,
				function(response, status, xhr) {
					if (status != "error") {
					}
					ko.applyBindings(model, $(".content").get(0));
				});
	} else if (viewId == 'Credit_card_info') {
		$("#contentData").load("Views/Credit/CreditCard-info.html", null,
				function(response, status, xhr) {
					if (status != "error") {
					}
					ko.applyBindings(model, $(".content").get(0));
				});
	} else if (viewId == 'cardInformation') {
		var model = new CreditCardViewModel(param);
		model.getccCardinformation();
	} else if (viewId == 'register_sucess') {
		$("#contentData").load("Views/Credit/CC-register-success.html", null,
				function(response, status, xhr) {
					if (status != "error") {
					}
					ko.applyBindings(model, $(".content").get(0));
				});
	} else if (viewId == 'CCconverEMI') {
		$("#contentData").load("Views/Credit/CC_Convert_EMI.html", null,
				function(response, status, xhr) {
					if (status != "error") {
					}
					ko.applyBindings(model, $(".content").get(0));
				});
	} else if (viewId == 'Credit_card_Trans') {
		$("#contentData").load("Views/Credit/CreditCard-transactions.html",
				null, function(response, status, xhr) {
					if (status != "error") {
					}
					if (CCcardtrans().length == 0) {
						$('#notransaction').show();
					}
					ko.applyBindings(model, $(".content").get(0));
				});
	} else if (viewId == 'Credit_card_EMIdet') {
		$("#contentData").load("Views/Credit/CreditCard-EMI.html", null,
				function(response, status, xhr) {
					if (status != "error") {
					}
					ko.applyBindings(model, $(".content").get(0));
				});
	} else if (viewId == 'CCUnbilled') {
		$("#contentData").load("Views/Credit/CC_UnbilledTrans.html", null,
				function(response, status, xhr) {
					if (status != "error") {
					}
					ko.applyBindings(model, $(".content").get(0));
				});
	} else if (viewId == 'CCLoanEnquiry') {
		$("#contentData").load("Views/Credit/CC_LoanEnquiry.html", null,
				function(response, status, xhr) {
					if (status != "error") {
					}
					ko.applyBindings(model, $(".content").get(0));
				});
	} else if (viewId == 'Credit_card_LoanTrans') {
		$("#contentData").load("Views/Credit/CreditCard-Loantrans.html", null,
				function(response, status, xhr) {
					if (status != "error") {
					}
					if (CCcardtrans().length == 0) {
						$('#notransaction').show();
					}
					ko.applyBindings(model, $(".content").get(0));
				});
	} else if (viewId == 'CCPaymentHome') {
		var model = new CreditCardViewModel(param);
		model.getccCardInfoForPay();
		// $("#contentData").load("Views/Credit/CC_CardPayment.html", null,
		// function (response, status, xhr) {
		// if (status != "error") {}
		// ko.applyBindings(model, $(".content").get(0));
		// });
	}

	// P2C

	else if (viewId == 'P2C_Details') {
		var model = new TPTViewModel(param);
		model.P2C_Details();
	}

	else if (viewId == 'P2C_History') {
		var model = new TPTViewModel(param);
		model.P2C_History();
	}

	// Add Payee

	else if (viewId == 'Payee_Home') {
		$("#contentData").load("Views/Payee/Payee_Home.html", null,
				function(response, status, xhr) {
					if (status != "error") {
					}
					ko.applyBindings(model, $(".content").get(0));
				});
	}

	else if (viewId == 'PayeeWithinBank') {
		$("#contentData").load("Views/Payee/PayeeWithinBank.html", null,
				function(response, status, xhr) {
					if (status != "error") {
					}
					BeneficieryOptState = BeneficieryOpt.InternalAddition;
					var model = new TPTViewModel(param);
					tptViewModelCopy = model;
					ko.applyBindings(model, $(".content").get(0));
					tptViewModelCopy.previousPage = "Payee_Home";
				});
	}

	else if (viewId == 'WithinBankDetails') {

		var model = new TPTViewModel(param);
		model.WithinBankDetails();

	}

	else if (viewId == 'Payee_NEFT_IMPS') {
		$("#contentData").load("Views/Payee/Payee_NEFT_IMPS.html", null,
				function(response, status, xhr) {
					if (status != "error") {
					}
					BeneficieryOptState = BeneficieryOpt.EXTERNALAddition;
					
					if (tptViewModelCopy) {
						
						tptViewModelCopy.openPage("#Payee_NEFT_IMPS");
						ko.applyBindings(tptViewModelCopy, $(".content").get(0));
					} else {
						
						var model = new TPTViewModel(param);
						tptViewModelCopy = model;
						ko.applyBindings(model, $(".content").get(0));
					}
					tptViewModelCopy.previousPage = "Payee_Home";
				});
	}

	else if (viewId == 'Payee_RTGS') {
		$("#contentData").load("Views/Payee/Payee_RTGS.html", null,
				function(response, status, xhr) {
					if (status != "error") {
					}
					BeneficieryOptState = BeneficieryOpt.RTGSAddition;
					
					if (tptViewModelCopy) {
						
						tptViewModelCopy.openPage("#Payee_RTGS");
						ko.applyBindings(tptViewModelCopy, $(".content").get(0));
					} else {
						
						var model = new TPTViewModel(param);
						tptViewModelCopy = model;
						ko.applyBindings(model, $(".content").get(0));
					}

					
					
					tptViewModelCopy.previousPage = "Payee_Home";
				});
	}

	// Modify Payee

	else if (viewId == 'ModifyPayee_Home') {
		$("#contentData").load("Views/ModifyPayee/ModifyPayee_Home.html", null,
				function(response, status, xhr) {
					if (status != "error") {
					}
					ko.applyBindings(model, $(".content").get(0));
				});
	}

	else if (viewId == 'PayeeList') {

		var model = new TPTViewModel(param);
		tptViewModelCopy = model;
		model.PayeeListDetails();
		tptViewModelCopy.previousPage = "Payee_Modify_Home";
	}

	else if (viewId == 'Payee_Beneficiery_Details') {
		var model = new TPTViewModel(param);
		tptViewModelCopy = model;
		model.payee_Bene_Details();
		tptViewModelCopy.previousPage = "Payee_List";
	}

	else if (viewId == 'IFSC_Search') {
		
		tptViewModelCopy.setPage();
		
		$("#contentData")
				.load(
						"Views/ModifyPayee/IFSC_Search.html",
						null,
						function(response, status, xhr) {
							if (status != "error") {
							}
							//var model = new TPTViewModel(param);
							//tptViewModelCopy = model;

							ko.applyBindings(tptViewModelCopy, $(".content").get(0));

							if (BeneficieryOptState == BeneficieryOpt.EXTERNALAddition) {
								
								tptViewModelCopy.previousPage = "Payee_NEFT_IFSC";
								//tptViewModelCopy.setPage();
								//setPage();
							} else {
								
								tptViewModelCopy.previousPage = "Payee_RTGS_IFSC";
								
								//tptViewModelCopy.setPage();
								//setPage();
							}

						});
	}
	
//Credit Score Module
	
//	else if (viewId == 'CreditScoreHome') {		
//
//		if(accountList().length==1){			
//			var model = new AccountsViewModel(param);
//			model.CreditScoreHome();		
//		}
//		else{			
//		
//			$("#contentData").load("Views/CreditScore/CreditScoreSelectAccount.html", null, function (response, status, xhr) {
//				if (status != "error") {}
//
//				var model = new AccountsViewModel(param);
//				model.getAccountsList();
//				ko.applyBindings(model, $(".content").get(0));                   
//			});			
//		}
//	}
	
	else if (viewId == 'CreditScoreHome') {		

				var model = new AccountsViewModel(param);
				creditscoremodelcopy = model;
				model.getAccountsListForCreditScore();
	}
	
	//Aadhar Card
else if(viewId == 'aadharUpdate'){
		
		$("#note_joint_acc").css("display","block");	
		       var model = new AccountsViewModel(param);		      
		       model.getAadharValidAccountsList();		    
		 }

	else {
		navigator.notification.alert("no appState method");
	}

};

this.AdapterFail = function(error) {
	console.log(JSON.stringify(error));
	busyInd.hide();
	if (error.errorCode == 'REQUEST_TIMEOUT'
			|| error.errorCode == 'UNEXPECTED_ERROR') {
		// navigator.notification.alert(serverConnError);
		navigator.notification.alert(error.errorMsg);
		// navigator.notification.alert(serverConnError);
		return;
	} else if (error.errorCode == 'PROCEDURE_ERROR') {
		navigator.notification
				.alert("Technical error while interacting with RBL Host");
		return;
	} else if (error.errorCode == 'UNRESPONSIVE_HOST') {
		navigator.notification.alert(error.errorMsg);
		// navigator.notification.alert("We are having challenge connecting to
		// RBL Host");
		return;
	} else {
		// navigator.notification.alert("Please check your Network connection in
		// setting");
		navigator.notification
				.alert("Please check your Network connection in setting");
	}
	busyInd.hide();
};

this.handleError = function(errdata, errorobj) {
	busyInd.hide();
	if (errorobj.hashid == 'logout') {
		navigator.notification
				.alert("Invalid message. Please retry or contact support.");
		window.location.hash = 'logout';
		return false;
	}

	if (errdata == "" || errdata == null) {
		busyInd.hide();
		// navigator.notification.alert(NoDataError);
		navigator.notification.alert(NoDataError);
	}
	if (errdata == "500") {
		busyInd.hide();
		// navigator.notification.navigator.notification.alert('We are facing a
		// temporary problem, please retry');
		navigator.notification
				.alert('We are facing a temporary problem, please retry');
	}
	busyInd.hide();
};
this.invalidresponse = function() {
	busyInd.hide();
	navigator.notification.alert(Invalidmsg);
};
this.handleErrorNoResponse = function() {
	busyInd.hide();
	navigator.notification.alert(NoResponseError);
};

this.handleErrorP2C = function(errorobj) {
	if (errorobj.hashid == 'logout') {
		// navigator.notification.alert("Invalid message. Please retry or
		// contact support.");
		window.location.hash = 'logout';
		return false;
	}
};