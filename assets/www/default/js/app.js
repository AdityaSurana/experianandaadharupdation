
/* JavaScript content from js/app.js in folder common */
	var CurrentSessionId = "";
	var LoggedInUserID = "";
	var LoggedInUserName = "";
	var Device_Platform = "Web";
	
	var Device_Version = "1.0";
	var RQClientAPIVer= "1.0";
	
	var Device_Model = "Web";
	document.addEventListener("deviceready", onDeviceReady, false);
		function onDeviceReady() {
			Device_Platform = device.platform;
			var deviceuuid = device.uuid;
			
			Device_Version = device.version;
			RQClientAPIVer = device.version;
			
			Device_Model = device.model;
	}
	
function RBLViewModel(){
	var self = this;

    self.loggedIn = ko.observable(false);
    self.backVisible = ko.observable(false);
    rsacheck = ko.observable(false);
    window.localStorage["loginflag"] = false;
    window.localStorage["loggeduserid"] = '';
    self.loggedinUserId = ko.observable();
    userID = ko.observable();
    userSecureImg = ko.observable();
    secureText = ko.observable();
    rsaEnrollReq = ko.observable();
    MyMenus = ko.observable();
    MmenuList =  ko.observable();
	MmenuList1 =  ko.observableArray([]);
	MmenuList11 =  ko.observableArray();
	MmenuList2 =  ko.observableArray();
	MmenuList3 =  ko.observableArray();
	MmenuList4 =  ko.observableArray();
	MmenuList5 =  ko.observableArray();
	MmenuList6 =  ko.observableArray();
	MmenuList7 =  ko.observableArray();
	MmenuList8 =  ko.observableArray();
	MmenuList9 =  ko.observableArray();
	MyPerlzdMenus = ko.observable();
    PerLzdmenuList =  ko.observable();
    selALLBiller = ko.observable();
    responseData = ko.observable();
    selectedAccount = ko.observableArray([]);
	selectedAccountcc = ko.observableArray([]);
    accountList = ko.observableArray([]);
	FDaccountList = ko.observableArray([]);
	RDaccountList = ko.observableArray([]);
	CCcardtrans = ko.observableArray([]);
	CCEMIdetails = ko.observableArray([]);
	CCEMIdetailPlan = ko.observableArray([]); 
	LoanaccountList = ko.observableArray([]);
	FDaccountDetails = ko.observableArray([]);
	RDaccountDetails = ko.observableArray([]);
	LoanaccountDetails  = ko.observableArray([]);
	CCcardinfo = ko.observableArray([]);
	FDaccountSchemes = ko.observableArray([]);
	RDaccountSchemes = ko.observableArray([]);
	accountListFt = ko.observableArray([]);
	accountListCC = ko.observableArray([]);
	mobileList = ko.observableArray([]);
	dthList = ko.observableArray([]);
    accSlider = ko.observable(false);
    ccaccountList = ko.observableArray([]);
	ccAccountsPay = ko.observableArray([]);
    ccaccountList1 = ko.observableArray([]);
    ccaccountList2 = ko.observableArray([]);
	 ccaccountList3 = ko.observableArray([]);
    ccaccountList4 = ko.observableArray([]);
    self.error = ko.observable();
    self.errormsg = ko.observable();
   mfSlider = ko.observable(false);
  mfaccountList = ko.observableArray([]);
  mfaccountList1 = ko.observableArray([]);  
    accStmtData = ko.observable();
    rdAccountList = ko.observableArray([]);
    dbtAccountList= ko.observableArray([]);
	dbtHotlistAcc= ko.observableArray([]);
	dbtErrText = ko.observable();
    
	demataccountList = ko.observableArray([]);
	SettlementList = ko.observableArray([]);
    
    /* RSA variables */
    self.rsaStmtData = ko.observable();
    self.quesList = ko.observableArray([]);
    self.rsaFields = ko.observableArray([]);
    self.rsaJSONdata = ko.observableArray([]);
    self.oobPhoneList = ko.observableArray([]);
    self.fldSelRsaOOBPhone = ko.observable();
    
    benefno_acct_no = ko.observable('');
	benef_IFSC = ko.observable('');
	benef_amnt = ko.observable('');
	benef_name = ko.observable('');
	benefdesc = ko.observable('');
	beneffrom_accnt_no = ko.observable('');
	benef_bankname = ko.observable('');
	benef_currency = ko.observable('');
	stopChqReason = ko.observable('');
	stopChqReasoncode = ko.observable('');
	startingChqNo = ko.observable('');
	endingChqNo = ko.observable('');
	benefCheqNO = ko.observable('');
	sender_name = ko.observable('');
	benef_addr = ko.observable('');
	nofd = '';
	nord = '';
	unbill = '';
	rhgfrmacnt = ko.observable('');
				
				subscriber_no = ko.observable('');
				recharge_amnt = ko.observable('');;
				biller_id = ko.observable('');
				biller_name = ko.observable('');
				payment_id = ko.observable('');
				rchgotp_key = ko.observable('');
	
				utilityname = ko.observable('');
				billaccountno = ko.observable('');
				billId = ko.observable('');
				billCode1 = ko.observable('');
				billoutstanding = ko.observable('');
				billpaymentdate = ko.observable('');
				billdate = ko.observable('');
				billno = ko.observable('');
				billPayType = ko.observable('');
				selectedacntavlbal  = ko.observable('');

    /* RSA variables */
    self.commonData = function(){
    	$(window).scrollTop(0);
    	
	if(window.location.hash == '#mymenu'){	
    		$('.footer').height(109);
    		$("#save").show();
    	}else{
    		$('.footer').height(54);
    		$("#save").hide();
    	}
    	if(window.location.hash == '#login' || window.location.hash == '#loginCustPass' || window.location.hash == '#menu'|| window.location.hash == '#aadhar' || window.location.hash == '#new_user_reg' || window.location.hash == '#new_user_reg1' || window.location.hash == '#new_user_reg2' || window.location.hash == '#new_user_reg3' || window.location.hash == '#PersonalReg' || window.location.hash == '#DebitReg' || window.location.hash == '#ChangePwd' || window.location.hash == '#PwdChangeSucess' || window.location.hash == '#contact'){
    		$(".footer").hide();
    		$(".logout .logout").hide();
    	}else{
    		$(".footer").show();
    		$(".logout .logout").show();
    	}
    	
    	if(window.location.hash == '#Home')
    	{	$(".logout .logout").show();
			$(".back .back").hide();
    		$(".footer").hide();
    		//$(".logout .logout").show();
			self.backVisible(false);
    	}
    	
    	if(window.location.hash == '#menu') $(".h_title").hide();
    	else $(".h_title").show();
    		
    	
    	if(WL.Client.getUserPref("loginFlag") == 'yes' && window.location.hash != '#login'){
    		$('.footer li').removeClass('active');   
  		   $(MyParentPage+"I").addClass('active');
    		//$(".back .back").show();
		if( window.location.hash == '#rrasm01' || window.location.hash == '#Home' || window.location.hash == '#rrftr01'|| window.location.hash == '#debithome'|| window.location.hash == '#rracs01'|| window.location.hash == '#rrblp01'|| window.location.hash == '#Demat'|| window.location.hash == '#others')
    			{	
    			self.backVisible(false);
    		}else{ self.backVisible(true);}
    	}
    	else{
    		self.backVisible(false);
    		//$(".back .back").hide();
    	}
			if( window.location.hash == '#P2C_Details' || window.location.hash == '#P2C_History' || window.location.hash == '#rrasm01' || window.location.hash == '#Home' || window.location.hash == '#rrftr01'|| window.location.hash == '#menu'|| window.location.hash == '#billpayment'|| window.location.hash == '#Demat'|| window.location.hash == '#others' || window.location.hash == '#Credit_card_acnts'){
				$(".back .back").hide();
			}
			else{
				$(".back .back").show();
			}
		if(window.location.hash == '#Home')
    	{	
    		$(".logout .logout").show();
			$(".back .back").hide();
    		$(".footer").hide();
    		//$(".logout .logout").show();
			self.backVisible(false);
    	}
    };
    
    formatAmt = function(val,tofix) {
	if(typeof(tofix)==='undefined') tofix=2;
    	val = parseFloat(val);
    	return val.toFixed(tofix).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");        
    };
    
    
    callLocate = function(){
    	WL.NativePage.show('com.snapwork.mapview', function(data){alert(data);}, {key1 : 'value1'});
    };
    
    
    Sammy(function() {
        this.disable_push_state = true; 
        	
        this.get("#menu",function() {        	
        	self.commonData();
        	
        	$("#contentData").load("Views/Menu/menu.html", null, function (response, status, xhr) {
                if (status != "error") {}
               
            });
        	
        });
		
		this.get("#new_user_reg",function() {        	
        	MyParentPage="#menu";
			self.commonData();			
			$(".h_title").html("Register");
		    loadViewModel("new_user_reg");
			
        });
		this.get("#new_user_reg1",function() {        	
        	MyParentPage="#menu";
			self.commonData();			
			$(".h_title").html("Register");
		    loadViewModel("new_user_reg1");
        }); 
		
		this.get("#new_user_reg2",function() {        	
        	MyParentPage="#menu";
			self.commonData();			
			$(".h_title").html("Register");
		    loadViewModel("new_user_reg2");
			
        });
		this.get("#new_user_reg3",function() {        	
        	MyParentPage="#menu";
			self.commonData();			
			$(".h_title").html("Register");
		    loadViewModel("new_user_reg3");
			
        });
		this.get("#PersonalReg",function() {        	
        	MyParentPage="#menu";
			self.commonData();			
			$(".h_title").html("Register");
		    loadViewModel("PersonalReg");
			
        });
		this.get("#DebitReg",function() {        	
        	MyParentPage="#menu";
			self.commonData();			
			$(".h_title").html("Register");
		    loadViewModel("DebitReg");
			
        });
		this.get("#ChangePwd",function() {        	
        	MyParentPage="#login";
			self.commonData();			
			$(".h_title").html("Change MPin");
		    loadViewModel("ChangePwd");
			
        });
		this.get("#PwdChangeSucess",function() {        	
        	MyParentPage="#login";
			self.commonData();			
			$(".h_title").html("Change Password");
		    loadViewModel("PwdChangeSucess");
			
        });
		this.get("#contact",function() {
        	self.commonData();
        	$(".h_title").html("Contact");
        	loadViewModel("contact");
        });

		// login 
        this.get("#login",function() {
        	MyParentPage="#menu"; 
        	/* self.commonData();
        	userSecureImg("");
			secureText(""); */
        	//self.commonData();
			$(".h_title").html("Login");
		    loadViewModel("login");
		 
        });        
        this.get("#loginCustPass",function() {        	
        	self.commonData();
            $(".h_title").html("Login");
            loadViewModel("logincustpass");
            
        });
        
        this.get("#logout",function() {        	
        	self.commonData();
            //$(".h_title").html("Login");
            loadViewModel("logout");
        });
        
		this.get("#Home",function() {
			MyParentPage="#Home";
        	//self.commonData();
        	//$(".h_title").html("Home");
        	loadViewModel("Home");
        });
		
        this.get("#rrper01",function() {        	
        	self.commonData();
            $(".h_title").html("Personalized Menu");
            loadViewModel("rrper01");
        });
         //IMPS fund transfer using MMID
		
		this.get("#impsFndTrnsList",function() {  
        	$(".h_title").html("Third Party Fund Transfer - IMPS)");
        	self.commonData();
        	loadViewModel("impsFndTrnsList");
        });
		this.get("#rrift01",function() {  
			$(".h_title").html("Fund Transfer to Mobile No.");
        	self.commonData();
        	loadViewModel("rrift01");
        });
		
		this.get("#rrift02",function() {  
        	$(".h_title").html("Fund Transfer to Mobile No.");
        	self.commonData();
        	loadViewModel("rrift02");
        });
		this.get("#rrift03",function() {  
			$(".h_title").html("Fund Transfer to Mobile No.");
        	self.commonData();
        	loadViewModel("rrift03");
        });
		//end of IMPS fund transfer
        this.get("#rrasm01",function() {
        	activecreditpage=false;
        	creditscoremodelcopy=null;
        	MyParentPage="#rrasm01";
        	self.commonData();
        	$(".h_title").html("Accounts");
        	loadViewModel("rrasm01");
        });
		
        
        this.get("#accountSummary",function() {        	
        	self.commonData();
        	$(".h_title").html("Account Summary");
        	loadViewModel("accountSummary");
        });
        this.get("#fdSummary",function() { 
        	self.commonData();
            $(".h_title").html("Fixed Deposits Summary");
            loadViewModel("rrloa01");
        	
        });
        
        this.get("#rdSummary",function() { 
        	
        	self.commonData();
            $(".h_title").html("Recurring Deposits Summary");
            loadViewModel("rrrds01");
         });
        
        
        this.get("#rrsin01",function() {           	
        	self.commonData();
            $(".h_title").html("View Account Statement");
            loadViewModel("rrsin01");            
        });
        
        this.get("#rrsin02",function() {
        	self.commonData();
        	$(".h_title").html("View Account Statement");
            loadViewModel("rrsin02");            
        });
        
        this.get("#accountStatment",function() {
        	self.commonData();
        	$(".h_title").html("View Account Statement");
        	loadViewModel("accountStatment");
        	 
        });	
        
        this.get("#rrasr02",function() { 
        	self.commonData();
            $(".h_title").html("Request Account Statement");
            loadViewModel("rrasr02");
        });
        
        this.get("#rrasr03",function() {  
        	
        	self.commonData();
            $(".h_title").html("Request Account Statement");
            loadViewModel("rrasr03");
         });
        
        this.get("#rrasr04",function() { 
        	
        	self.commonData();
            $(".h_title").html("Request Account Statement");
            loadViewModel("rrasr04");
            
        });
        
        this.get("#rrcbr02",function() {    
        	
        	self.commonData();
            $(".h_title").html("Request Cheque Book");
            loadViewModel("rrcbr02");
            
        });
        
        this.get("#rrcbr03",function() {    
        	
        	self.commonData();
            $(".h_title").html("Request Cheque Book");
            loadViewModel("rrcbr03");
            
        });
        
        this.get("#rrcbr04",function() {     
        	self.commonData();
            $(".h_title").html("Request Cheque Book");
            loadViewModel("rrcbr04");
            
        });
        
        // stop payment of cheque
        this.get("#rrsch01",function() {   
        	
        	self.commonData();
            $(".h_title").html("Stop Payment of Cheque");
            loadViewModel("rrsch01");
            
            
        });

        this.get("#rrsch02",function() {        
        	self.commonData();
            $(".h_title").html("Stop Payment of Cheque");
            loadViewModel("rrsch02");
            
        });
        
        this.get("#rrsch03",function() {        	
        	
        	self.commonData();
            $(".h_title").html("Stop Payment of Cheque");
            loadViewModel("rrsch03");
        	
        });
        
     // view cheque status
        this.get("#rrcsi01",function() {    
        	
        	self.commonData();
            $(".h_title").html("View Cheque Status");
            loadViewModel("rrcsi01");
            
        });
        
        this.get("#rrcsi02",function() { 
        	
        	self.commonData();
            $(".h_title").html("View Cheque Status");
            loadViewModel("rrcsi02");
        	
        });
        /* Start - Account section */
        
        this.get("#rrftr01",function() {
        	activeP2Cpage=false;
        	activeAddBenpage=false;
        	$(".h_title").html("Third Party Fund Transfer");
        	MyParentPage="#rrftr01";
           $('.menutt').attr('id', "rrftr01");
        	self.commonData();
        	busyInd.show();        	
		    $("#contentData").load("Views/Accounts/rrftr01.html", null, function (response, status, xhr) {
                if (status != "error") {}	
					$('.footer li').removeClass('active');
					$('.footer').find('#rrftr01I').addClass('active');
                	busyInd.hide();
                    ko.applyBindings(self, $(".dynamic-page-content").get(0));                   
            });            
        });

        this.get("#rrftr02",function() {  
        	$(".h_title").html("Own Accounts Fund Transfer");
        	self.commonData();
        	loadViewModel("rrftr02");
        });
        
        this.get("#rrftr03",function() {        	
        	self.commonData();
        	$(".h_title").html("Own Accounts Fund Transfer");
        	loadViewModel("rrftr03");            
        });
        
        this.get("#rrftr04",function() {        	
        	self.commonData();
        	$(".h_title").html("Between My Accounts");
        	accstmtdata = accStmtData(); 
		    $("#contentData").load("Views/Accounts/rrftr04.html", null, function (response, status, xhr) {
                if (status != "error") {}              
                ko.applyBindings(self, $(".dynamic-page-content").get(0));                   
            });
            
        });
        
        
        //CHnage for TPD
		this.get("#rrtpi01",function() {  
        	$(".h_title").html("View List of Beneficiaries");
        	self.commonData();
        	loadViewModel("rrtpi01");
        });	
		this.get("#rrtpn04",function() {  
        	$(".h_title").html("Third Party Fund Transfer - NEFT");
        	self.commonData();
        	loadViewModel("rrtpn04");
        });
		this.get("#rrtpn05",function() {  
        	$(".h_title").html("Third Party Fund Transfer - NEFT");
        	self.commonData();
        	loadViewModel("rrtpn05");
        });
        
		//RTGS
		this.get("#rrrtg04",function() {  
        	$(".h_title").html("Third Party Fund Transfer - RTGS");
        	self.commonData();
        	loadViewModel("rrrtg04");
        });
		this.get("#rrrtg05",function() {  
        	$(".h_title").html("Third Party Fund Transfer - RTGS");
        	self.commonData();
        	loadViewModel("rrrtg05");
        });
		//billpay
		this.get("#billpayment",function() {
			MyParentPage="#billpayment";
	        	self.commonData();
	        	$(".h_title").html("Bill Payment");
	        	loadViewModel("billpayment");
	    });
		this.get("#rrblp01",function() {  
        	$(".h_title").html("Bill Payment");
        	self.commonData();
        	loadViewModel("rrblp01");
        });
		this.get("#rrblp02",function() {  
        	$(".h_title").html("Bill Payment");
        	self.commonData();
        	loadViewModel("rrblp02");
        });
		this.get("#rrblp03",function() {  
        	$(".h_title").html("Bill Payment");
        	self.commonData();
        	loadViewModel("rrblp03");
        });
		this.get("#rrblp04",function() {  
        	$(".h_title").html("Bill Payment");
        	self.commonData();
        	loadViewModel("rrblp04");
        });
		//mobile recharge section 
		this.get("#rrpmb01",function() {  
        	$(".h_title").html("Mobile Recharge");
        	self.commonData();
        	loadViewModel("rrpmb01");
        });
		this.get("#rrpmb02",function() {  
        	$(".h_title").html("Mobile Recharge");
        	self.commonData();
        	loadViewModel("rrpmb02");
        });
		this.get("#rrpmb03",function() {  
        	self.commonData();
        	$(".h_title").html("Mobile Recharge");
		    $("#contentData").load("Views/recharge/rrpmb03.html", null, function (response, status, xhr) {
                if (status != "error") {}              
                ko.applyBindings(self, $(".dynamic-page-content").get(0));                   
            });
        });
		//DTH recharge section
		this.get("#rrdth01",function() {  
        	$(".h_title").html("DTH Recharge");
        	self.commonData();
        	loadViewModel("rrdth01");
        });
		this.get("#rrdth02",function() {  
        	$(".h_title").html("DTH Recharge");
        	self.commonData();
        	loadViewModel("rrdth02");
        });
		this.get("#rrdth03",function() {  
        	self.commonData();
        	$(".h_title").html("DTH Recharge");
		    $("#contentData").load("Views/recharge/rrdth03.html", null, function (response, status, xhr) {
                if (status != "error") {}              
                ko.applyBindings(self, $(".dynamic-page-content").get(0));                   
            });
        });
		//Recharge status
		this.get("#rrcrs01",function() {
			$(".h_title").html("Recharge Status");
        	self.commonData();
        	loadViewModel("rrcrs01");
			
        });
		this.get("#rrcrs02",function() {  
        	$(".h_title").html("Recharge Status");
        	self.commonData();
        	loadViewModel("rrcrs02");
        });
		 /* Deposits */
		this.get("#FD_opening",function() {  
        	$(".h_title").html("Open Fixed Deposit");
        	self.commonData();
        	loadViewModel("FD_opening");
        });
		this.get("#RD_opening",function() {  
        	$(".h_title").html("Open Recurring Deposit");
        	self.commonData();
        	loadViewModel("RD_opening");
        });
		this.get("#RD_summary",function() {  
        	$(".h_title").html("Recurring Deposits Summary");
        	self.commonData();
        	loadViewModel("RD_summary");
        });
		this.get("#RD_Acnts",function() {  
        	$(".h_title").html("Recurring Deposits Summary");
        	self.commonData();
        	loadViewModel("RD_Acnts");
        });
		this.get("#FD_Acnts",function() {  
        	$(".h_title").html("Fixed Deposits Summary");
        	self.commonData();
        	loadViewModel("FD_Acnts");
        });
		this.get("#FD_Accounts",function() {  
        	$(".h_title").html("Fixed Deposits Summary");
        	self.commonData();
        	loadViewModel("FD_Accounts");
        });
		this.get("#RD_Accounts",function() {  
        	$(".h_title").html("Recurring Deposits Summary");
        	self.commonData();
        	loadViewModel("RD_Accounts");
        });
		this.get("#FD_summary",function() {  
        	$(".h_title").html("Fixed Deposits Summary");
        	self.commonData();
        	loadViewModel("FD_summary");
        });
		this.get("#aadhar",function() {
			MyParentPage="#menu";			
        	$(".h_title").html("Aadhaar status Look-Up");
        	self.commonData();
        	loadViewModel("aadhar");
        });
		this.get("#Loan_Acnts",function() {
			MyParentPage="#rrasm01";			
        	$(".h_title").html("Loan Summary");
        	self.commonData();
        	loadViewModel("Loan_Acnts");
        }); 
		this.get("#Loan_Acnt",function() {
			MyParentPage="#rrasm01";			
        	$(".h_title").html("Loan Summary");
        	self.commonData();
        	loadViewModel("Loan_Acnt");
        });
		this.get("#Loan_summary",function() {
			MyParentPage="#rrasm01";			
        	$(".h_title").html("Loan Summary");
        	self.commonData();
        	loadViewModel("Loan_summary");
        });
        /* Start - TPT section */
        this.get("#rrtpt03",function() {  
        	$(".h_title").html("Within RBL Bank");
        	self.commonData();
        	loadViewModel("rrtpt03");        	         
        });
        
        this.get("#rrtpt04",function() {        	
        	self.commonData();
        	$(".h_title").html("Within RBL Bank");
        	loadViewModel("rrtpt04");
        });
        
        // View RTGS Funds Transfer
        this.get("#rrtpv01",function() {  
        	$(".h_title").html("View NEFT Funds Transfer");
        	self.commonData();
        	loadViewModel("rrtpv01");        	         
        });
        this.get("#rrtpv02",function() {  
        	$(".h_title").html("View NEFT Funds Transfer");
        	self.commonData();
        	loadViewModel("rrtpv02");        	         
        });
        
        
        // view imps fund transfer
        this.get("#rrvft01",function() {  
        	$(".h_title").html("View IMPS Funds Transfer");
        	self.commonData();
        	loadViewModel("rrvft01");        	         
        });
        this.get("#rrvft02",function() {  
        	$(".h_title").html("View IMPS Funds Transfer");
        	self.commonData();
        	loadViewModel("rrvft02");        	         
        });
        this.get("#rrvft03",function() {  
        	$(".h_title").html("View IMPS Funds Transfer");
        	self.commonData();
        	loadViewModel("rrvft03");        	         
        });
        /* End - TPT section */
		//Credit Card Section 
		this.get("#CreditCard",function() { 
			$(".h_title").html("Credit Card");
        	self.commonData();
        	loadViewModel("CreditCard");
        });
		this.get("#Credit_card_acnts",function() { 
			$(".h_title").html("Credit Card");
        	self.commonData();
        	loadViewModel("Credit_card_acnts");
        });
		this.get("#ccsummary",function() {
			MyParentPage="#CreditCard";			
			$(".h_title").html("Card Summary");
        	self.commonData();
        	loadViewModel("ccsummary");
        });
		this.get("#CC_register",function() {
			MyParentPage="#CreditCard";			
			$(".h_title").html("Register Other Credit Card");
        	self.commonData();
        	loadViewModel("CC_register");
        });
		this.get("#Credit_card_info",function() {
				MyParentPage="#CreditCard";	
			$(".h_title").html("Card Information");
        	self.commonData();
        	loadViewModel("Credit_card_info");
        });
		this.get("#cardInformation",function() {
				MyParentPage="#CreditCard";	
			$(".h_title").html("Card Information");
        	self.commonData();
        	loadViewModel("cardInformation");
        });
		this.get("#register_sucess",function() { 
			MyParentPage="#CreditCard";	
			$(".h_title").html("Credit Card Registration");
        	self.commonData();
        	loadViewModel("register_sucess");
        });
		this.get("#CCconverEMI",function() {
			MyParentPage="#CreditCard";	
			unbill = 'false';			
			$(".h_title").html("Convert into EMI instantly");
        	self.commonData();
        	loadViewModel("CCconverEMI");
        });
		this.get("#Credit_card_Trans",function() {
				MyParentPage="#CreditCard";	
		if(unbill == 'false'){$(".h_title").html("Convert into EMI instantly");}
		if(unbill == 'true'){$(".h_title").html("View Un-billed transactions");}
			
        	self.commonData();
        	loadViewModel("Credit_card_Trans");
        });
		this.get("#Credit_card_EMIdet",function() {
				MyParentPage="#CreditCard";	
			$(".h_title").html("Convert into EMI instantly");
        	self.commonData();
        	loadViewModel("Credit_card_EMIdet");
        });
		this.get("#CCUnbilled",function() {
			MyParentPage="#CreditCard";	
			unbill = 'true';
			$(".h_title").html("View Un-billed transactions");
        	self.commonData();
        	loadViewModel("CCUnbilled");
        });
		this.get("#CCLoanEnquiry",function() { 
			MyParentPage="#CreditCard";	
			$(".h_title").html("Your EMI requests");
        	self.commonData();
        	loadViewModel("CCLoanEnquiry");
        });
		this.get("#Credit_card_LoanTrans",function() { 
			MyParentPage="#CreditCard";	
			$(".h_title").html("Your EMI request");
        	self.commonData();
        	loadViewModel("Credit_card_LoanTrans");
        });
		this.get("#CCPaymentHome",function() { 
			MyParentPage="#CreditCard";	
			$(".h_title").html("Make a payment");
        	self.commonData();
        	loadViewModel("CCPaymentHome");
        });
		this.get("#rrp2a01",function() { 
			$(".h_title").html("Third Party Fund Transfer - IMPS");
        	self.commonData();
        	loadViewModel("rrp2a01");
        });
		this.get("#rrp2a02",function() {  
        	$(".h_title").html("Third Party Fund Transfer - IMPS");
        	self.commonData();
        	loadViewModel("rrp2a02");
        });
		
		//P2C 
		this.get("#P2C_Details",function() {  
//        	$(".h_title").html("P2C");
        	self.commonData();
        	loadViewModel("P2C_Details");
        });
		
		this.get("#P2C_History",function() {  
//        	$(".h_title").html("P2C Transaction History");
        	self.commonData();
        	loadViewModel("P2C_History");
        });
		
		//Add Payee
		
		this.get("#Payee_Home",function() {
        	self.commonData();
        	tptViewModelCopy = null;
        	$(".h_title").html("Third Party Fund Transfer");
        	loadViewModel("Payee_Home");
        });
		
		this.get("#PayeeWithinBank",function() {
        	self.commonData();
        	$(".h_title").html("Within RBL Bank");
        	loadViewModel("PayeeWithinBank");
        });
		
		this.get("#WithinBankDetails",function() {
        	self.commonData();
        	$(".h_title").html("Within RBL Bank");
        	loadViewModel("WithinBankDetails");
        	
        });
		
		this.get("#Payee_NEFT_IMPS",function() {
        	self.commonData();
        	$(".h_title").html("NEFT/IMPS");
        	loadViewModel("Payee_NEFT_IMPS");
        });
		
		this.get("#Payee_RTGS",function() {
        	self.commonData();
        	$(".h_title").html("RTGS");
        
        	loadViewModel("Payee_RTGS");
        });
		
		
		//Modify Payee
		
		this.get("#ModifyPayee_Home",function() {
        	self.commonData();
        	$(".h_title").html("Third Party Fund Transfer");
        	loadViewModel("ModifyPayee_Home");
        });	
		
		this.get("#Payee_Within",function() {
        	self.commonData();
        	BeneficieryOptState =  BeneficieryOpt.InternalModification;
        	$(".h_title").html("Third Party Fund Transfer");
        	loadViewModel("PayeeList");
        });
		
		this.get("#Payee_Ext",function() {
        	self.commonData();
        	BeneficieryOptState =  BeneficieryOpt.EXTERNALModification;
        	$(".h_title").html("Third Party Fund Transfer");
        	loadViewModel("PayeeList");
        });
		
		this.get("#Payee_RTGS_Bene",function() {
        	self.commonData();
        	BeneficieryOptState =  BeneficieryOpt.RTGSModification;
        	$(".h_title").html("Third Party Fund Transfer");
        	loadViewModel("PayeeList");
        });
		
		
		this.get("#IFSC_Search",function() {
        	self.commonData();
        	selectedIFSCIndex = -1;
        	$(".h_title").html("IFSC Search");
        	loadViewModel("IFSC_Search");
        });
		
		//Credit Score Module
		
		this.get("#CreditScoreHome",function() {  
        	$(".h_title").html("View your Credit Score");
        	self.commonData();
        	loadViewModel("CreditScoreHome");
        });
		
		//Adhar Updation
		this.get("#aadharUpdate",function() {
			$(".h_title").html("Update your Aadhaar");
			self.backVisible(true);
			loadViewModel("aadharUpdate");
		});
		
    }).run("#menu");
}
/*
$('a.pagerlink').click(function() { 
	alert('clicked');
    var id = $(this).attr('id');
    $container.cycle(id.replace('#', '')); 
   return false; 
});

$('input[id^="totalelement"]').on('click', function() {  
    alert(this.value);
 });*/

//ko.applyBindings(new RBLViewModel());
RBLModel = new RBLViewModel();
ko.applyBindings(RBLModel); 
$(window).on('hashchange', function() {
 $('body').css('height',100+'%');
	//$('body').css('height','auto');

});

function Payee_Beneficiery_Details(i){
	selectedIndexOfBeneficiery = i;

//	$(".h_title").html("Third Party Fund Transfer");

	loadViewModel("Payee_Beneficiery_Details");

}