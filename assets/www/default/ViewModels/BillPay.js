
/* JavaScript content from ViewModels/BillPay.js in folder common */
  var BillPay = function () {

        var self = this;
        
        self.curraccbalval = ko.observable();
        self.selAccount = ko.observable();
        self.accountStmtTxns = ko.observableArray([]);
        self.BillerList = ko.observableArray([]);
        self.BillerList1 = ko.observableArray([]);
		self.selBiller = ko.observable();
		self.billoperlist = ko.observableArray([]);
		
    /* =============== */
    
    this.rrblp01Page = function(){
		if(window.navigator.onLine){

    	reqParams = {};
			reqParams["RQLoginUserId"] = LoggedInUserID;
			reqParams["RQDeviceFamily"] = Device_Platform;
			reqParams["RQDeviceFormat"] = Device_Model;
			reqParams["RQOperationId"] = "BILLPAYRQ";
			reqParams["RQClientAPIVer"] = RQClientAPIVer;
			reqParams["SessionId"] = CurrentSessionId;
			reqParams["RQTransSeq"] = "01";
		
			fldjsessionid="";
			busyInd.show();
			if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
					adapter : "API_Adapter",
					procedure : "GetBillpay",
					parameters : [fldjsessionid,reqParams],
					compressResponse : true
			};
					
			WL.Client.invokeProcedure(invocationData, {
				onSuccess : rrblp01Success,
				onFailure : AdapterFail,	    		
				timeout: timeout
			});
		}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
		}
    };
		
    rrblp01Success = function(result){
		invocationResult = result.invocationResult; //custbillerinfo
		    if (invocationResult.isSuccessful) {
                if (invocationResult.RBL) {
					if(invocationResult.RBL.STATUS.CODE == '0') {
                        numcustcomp = invocationResult.RBL.Response.custbillercount;
                        //console.log("Biller nobiller " + numcustcomp);
                        self.BillerList.removeAll();
                        self.BillerList1.removeAll();
						accountListFt.removeAll();
                        if (numcustcomp != 0) {
							//accounts
							acctdtls = invocationResult.RBL.Response.validAcct.acctdetails;
							custdtls = invocationResult.RBL.Response.CustDetails;
							var idx = 1;
							$(acctdtls).each(function(index, obj) {
								strid = "item"+idx;
										
								$(custdtls).each(function(j, obj1) {
									custnames="";
									if(obj.acctindex == obj1.acctindex){
										var actbal = "₹ "+formatAmt(parseFloat(obj.acctbalance));
										displaytxt = $.trim(obj.acctno)+"-"+obj.branchname;
										accountListFt.push({ codacctno: obj.acctno+"#"+obj1.custName+'#'+obj.acctbalance, acctType: obj.acctType, acctbalance: actbal, acctbranch: obj.branchname, custnames: custnames, namccyshrt: obj.currency, displaytxt: displaytxt, strid:strid });
									}
								});
								idx++;
							});
							// biller accounts
                            custcompinfo = invocationResult.RBL.Response.custbillerinfo; //.custcompinfo;//custbillerinfo
                            var idx = 1;
                            $(custcompinfo).each(function (index, obj) {
                                //alert(obj.noofbills);
                                if (typeof (obj.custAcctBal) != "undefined") {
                                    custAcctBal = obj.custAcctBal;
                                } else {
                                    custAcctBal = '';
                                }

                                if (typeof (obj.compcurr) != "undefined") {
                                    compcurr = obj.compcurr;
                                } else {
                                    compcurr = '';
                                }
                                if (typeof (obj.acctCurr) != "undefined") {
                                    acctCurr = obj.acctCurr;
                                } else {
                                    acctCurr = '';
                                }
                                if (typeof (obj.billdetails) != "undefined") {
                                    displayValue = obj.billerName + "#" + obj.billAcctno + "#" + custAcctBal + "#" + obj.billcompacctno + "#" + obj.billid + "#" + obj.billCode1 + "#" + obj.billInfoAvailable + "#" + obj.paytype + "#" + obj.billerNickname + "#" + obj.billdetails.billoutstanding + "#" + obj.billdetails.billdate + "#" + obj.billdetails.billno + "#" + obj.billdetails.billpaymentdate + "#" + obj.noofbills + "#" + acctCurr + "#" + compcurr;
                                } else {
                                    displayValue = obj.billerName + "#" + obj.billAcctno + "#" + custAcctBal + "#" + obj.billcompacctno + "#" + obj.billid + "#" + obj.billCode1 + "#" + obj.billInfoAvailable + "#" + obj.paytype + "#" + obj.billerNickname + "#####" + obj.noofbills + "#" + acctCurr + "#" + compcurr;

                                }
                                displaytxt = $.trim(obj.billerName) + "-" + obj.billerNickname;
                                if (obj.noofbills != '0' || obj.billInfoAvailable == 'N') {
                                    strid = "item" + idx;
                                    custnames = "";
                                    self.BillerList.push({
                                        displayvalue1: displayValue,
                                        displaytxt: displaytxt,
                                        strid: strid,
                                        billcompname: obj.billerName,
                                        mnemname: obj.billerNickname
                                    });
                                    idx++;
                                }
                                if (obj.noofbills == '0' && obj.billInfoAvailable != 'N') {
									strid = "item" + idx;
                                    self.BillerList1.push({
                                        displayvalue1: displayValue,
                                        displaytxt: displaytxt,
                                        strid: strid,
                                        billcompname: obj.billerName,
                                        mnemname: obj.billerNickname
                                    });
                                    idx++;
                                }
                            });
                        }

                        $("#contentData").load("Views/BillPay/rrblp01.html", null, function (response, status, xhr) {
                            if (status != "error") {}
                            ko.applyBindings(self, $(".dynamic-page-content").get(0));
                            if (numcustcomp == 0) {
                                $("#Nobiller").show();
                                $("#biller").hide();
                            } else {
								//alert(self.BillerList1.length);
								if(self.BillerList1().length == '0'){
									$("#info").hide();
								}
                                $("#Nobiller").hide();
                                $("#biller").show();
                            }
                        });

                    } else {
						busyInd.hide();
						  $("#contentData").load("Views/BillPay/rrblp01.html", null, function (response, status, xhr) {
                            if (status != "error") {}
                            ko.applyBindings(self, $(".dynamic-page-content").get(0));
							$("#biller").hide();
							$("#labelname").hide();
							$("#error").html(invocationResult.RBL.STATUS.CODE+": "+invocationResult.RBL.STATUS.MESSAGE);
							$("#Nobiller").show();
						 });
					}
                }else{
						busyInd.hide();
						handleError(invocationResult.RBL.Response, invocationResult.RBL);
				}
            }
            busyInd.hide();
		
    };
	
	this.rrblp01Submit = function(){
		if($("#fbiller").valid()){
			busyInd.show();
				selectedacntavlbal($('#fldAcctNo').val().split("#")[2]);
				beneffrom_accnt_no($('#fldAcctNo').val().split("#")[0]);
				utilityname($('#fldBillDetails').val().split("#")[0]);
				billaccountno($('#fldBillDetails').val().split("#")[1]);
				billId($('#fldBillDetails').val().split("#")[4]);
				billCode1($('#fldBillDetails').val().split("#")[5]);
				billoutstanding($('#fldBillDetails').val().split("#")[9]);
				billpaymentdate($('#fldBillDetails').val().split("#")[12]);
				billdate($('#fldBillDetails').val().split("#")[10]);
				billno($('#fldBillDetails').val().split("#")[11]);
				billPayType($('#fldBillDetails').val().split("#")[7]);
					
				busyInd.hide();
			// $("#contentData").load("Views/BillPay/rrblp02.html", null, function (response, status, xhr) {
                // if (status != "error") {}
                // ko.applyBindings(self, $(".dynamic-page-content").get(0));
				
			// });
			window.location.hash = "#rrblp02";
		}
	};
	
	this.rrblp02Page = function(){
			$("#contentData").load("Views/BillPay/rrblp02.html", null, function (response, status, xhr) {
	    	    if (status != "error") {}
				if(billPayType()== "F" || billPayType()== "f"){
					$('#fldCompanyName').html(utilityname());
					$('#fldBillCompId').html(billId());
					$('#fldConsumerNo').html(billaccountno());
					$('#fldFromAcctNo').html(beneffrom_accnt_no());
					$('#fldCustAcctBal').html("₹ " +formatAmt(parseFloat(selectedacntavlbal())));
					$('#billamount1').show();
					$('#billamount').html("₹ " +formatAmt(parseFloat(billoutstanding())));
					$('#billOutStandingDiv').hide();
				}
				else{
					$('#fldCompanyName').html(utilityname());
					$('#fldBillCompId').html(billId());
					$('#fldConsumerNo').html(billaccountno());
					$('#fldFromAcctNo').html(beneffrom_accnt_no());
					$('#fldCustAcctBal').html("₹ " +formatAmt(parseFloat(selectedacntavlbal())));
					$('#billOutStandingDiv').show();
				}
				ko.applyBindings(self, $(".dynamic-page-content").get(0));
	    	});
		busyInd.hide();		
	};
	
	this.rrblp02Submit = function(){
		if($("#rrblp02").valid()){
			$('#confirm_button1').prop('disabled', true);
			var paytype = billPayType();
			if(paytype == "P" || paytype == "p"){
				billoutstanding($('#fldTxnAmt').val());
			}
			//GetOTP();
			//busyInd.hide();
			GetOTP();
			//window.location.hash = "#rrblp03";
		}
	};
		
	this.rrblp03Page = function(){
			
		$("#contentData").load("Views/BillPay/rrblp03.html", null, function (response, status, xhr) {
	    	if (status != "error") {}	
	    	OTP_number = invocationResult1.RBL.Response.otprefno;
			$('#OTP_Ref').val(OTP_number);
			if(invocationResult1.RBL.Response.otprequired == 'Y'){
				$('#OTP_No').show();
			}
			else{
				$('#OTP_No').hide();
			}
			$('#fldCompanyName').html(utilityname());
			$('#fldBillCompId').html(billId());
			$('#fldConsumerNo').html(billaccountno());
			$('#fldFromAcctNo').html(beneffrom_accnt_no());
			$('#fldCustAcctBal').html("₹ " +formatAmt(parseFloat(selectedacntavlbal())));
			$('#billamount').html("₹ " +formatAmt(parseFloat(billoutstanding())));
			ko.applyBindings(self, $(".dynamic-page-content").get(0));
			
		});
		busyInd.hide();	
	};
	
	this.rrblp03Submit = function(){
	if($("#frmftr03").valid()){
		if(window.navigator.onLine){
				reqParams = {};
				reqParams["RQLoginUserId"] = LoggedInUserID;
				reqParams["RQDeviceFamily"] = Device_Platform;
				reqParams["RQDeviceFormat"] = Device_Model;
				reqParams["RQOperationId"] = "BILLPAYRQ";
				reqParams["RQClientAPIVer"] = RQClientAPIVer;
				reqParams["SessionId"] = CurrentSessionId;
				reqParams["RQFromAcctNo"] = beneffrom_accnt_no();
				reqParams["RQBillAccNo"] = billaccountno();
				reqParams["RQBillId"] = billId();
				reqParams["billCode"] = billCode1();
				reqParams["RQBillerName"] = utilityname();
				reqParams["RQBillerNickName"] = utilityname();
				reqParams["billoutstanding"] = billoutstanding();
				reqParams["billpaymentdate"] = billpaymentdate();
				reqParams["billdate"] = billdate();
				reqParams["billno"] = billno();
				reqParams["RQPayType"] = billPayType();
				reqParams["RQTransSeq"] = "03";
				reqParams["RQDesc"] = $('#billdesc').val();
				reqParams["RQOTP"] = $('#OTP_No_Val').val();
				reqParams["RQOTPReq"] = invocationResult1.RBL.Response.otprequired;
				reqParams["RQRefNo"] = $('#OTP_Ref').val();
				
				busyInd.show();
				if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
					adapter : "API_Adapter",
					procedure : "billpaysubmit",
					parameters : [fldjsessionid,reqParams],
					compressResponse : true
				};
    	
				WL.Client.invokeProcedure(invocationData, {
					onSuccess : rrblp03SubmitSuccess,
					onFailure : AdapterFail,
					timeout: timeout
				});

		}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
		}
	}
	};
	
	rrblp03SubmitSuccess = function(result){
			busyInd.hide();
        	invocationResult = result.invocationResult;
        	if(invocationResult.isSuccessful) {
        		if(invocationResult.RBL){	
					if(invocationResult.RBL.Response || invocationResult.RBL.Response == ''){
							selALLBiller(invocationResult.RBL);    
							window.location = "#rrblp04";
					}else{
						busyInd.hide();
						handleError(invocationResult.RBL.Response, invocationResult.RBL);
					}
        		}else{
					busyInd.hide();
					handleErrorNoResponse();
				}
        	}
	};
	
	this.rrblp04Page = function(){
			busyInd.hide();
			selBillerValue = selALLBiller();
			$("#contentData").load("Views/BillPay/rrblp04.html", null, function (response, status, xhr) {
				if(invocationResult.RBL.STATUS.CODE == '0'){
						$(".success_msg").show();
						$("#tpn06success").show();
						busyInd.hide();
						accountList.removeAll();	
						
						$('#fldCompanyName').html(selBillerValue.Response.custbillerinfo.billerName);
						$('#fldFromAcctNo').html(selBillerValue.RequestParams.RQFromAcctNo);
						//$('#fldCustAcctBal').html(selBillerValue.RequestParams.fldCustAcctBal);
						$('#fldBillCompId').html(selBillerValue.RequestParams.RQBillId);
						$('#fldConsumerNo').html(selBillerValue.Response.custbillerinfo.billAcctno);
						$('#refno').html(selBillerValue.Response.txnrefno);
						//$('#balavailable').html(selBillerValue.response.balavailable);
						//$('#billamount').html(selBillerValue.response.paymentdtls.billamount);
						//balavailableS
						// accountList.removeAll();
						// accountSummList.removeAll(); ("₹ " +formatAmt(parseFloat(billoutstanding())));
						
						$('#balavailableS').html("₹ " +formatAmt(parseFloat(selBillerValue.RequestParams.billoutstanding)));
						$('#billamountS').html("₹ " +formatAmt(parseFloat(selBillerValue.RequestParams.billoutstanding)));
				}
				else{
					busyInd.hide();
					$(".rsaOOBErr").show();
					$(".rsaOOBErr p").html(invocationResult.RBL.STATUS.CODE+": "+invocationResult.RBL.STATUS.MESSAGE);
					$("#tpn06success").hide();
				}
			busyInd.hide();
			if (status != "error") {}	
			ko.applyBindings(self, $(".dynamic-page-content").get(0)); 

			// billOutStanding = selBillerValue.request.fldBillAmt;
			// fldcustacctbal  = selBillerValue.request.fldcustacctbal;
			});
	};
	
	//OTP	
	GetOTP = function(){
		if(window.navigator.onLine){
				reqParams = {};
				reqParams["RQLoginUserId"] = LoggedInUserID;
				reqParams["RQDeviceFamily"] = Device_Platform;
				reqParams["RQDeviceFormat"] = Device_Model;
				reqParams["RQOperationId"] = "OTPGENREQ";
				reqParams["RQClientAPIVer"] = RQClientAPIVer;
				reqParams["SessionId"] = CurrentSessionId;
				reqParams["RQTransSeq"] = "01";
				fldjsessionid="";
				
				busyInd.show();
				if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
					adapter : "API_Adapter",
					procedure : "GetOTP",
					parameters : [fldjsessionid,reqParams],
					compressResponse : true
				};
    	
				WL.Client.invokeProcedure(invocationData, {
					onSuccess : GetOTPResponse,
					onFailure : AdapterFail,
					timeout: timeout
				});
			}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
		}
	};	

		GetOTPResponse = function(result){
			invocationResult1 = result.invocationResult;
			if(invocationResult1.RBL){
				if(invocationResult1.isSuccessful) {
					if(invocationResult1.RBL.Response){	
						if(window.location.hash == "#rrblp02"){
							busyInd.hide();
							$('#confirm_button1').prop('disabled', false);
							OTP_number = invocationResult1.RBL.Response.otprefno;
							$('#OTP_Ref').val(OTP_number);
							window.location = '#rrblp03';
						}
						else{
							busyInd.hide();
							OTP_number = invocationResult1.RBL.Response.otprefno;
							$('#OTP_Ref').val(OTP_number);
						}
					}else{
							busyInd.hide();
							handleError(invocationResult1.RBL.Response, invocationResult1.RBL);
					}		
				}
			}else{
        		navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
        		busyInd.hide();
			}
			busyInd.hide();
		};
		
		//mobile Recharge
		this.rrpmb01Page = function(){
			if(window.navigator.onLine){
				busyInd.show();
				if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
						adapter : "API_Adapter",
						procedure : "GetOperator",
						parameters : [],
						compressResponse : true
				};
						
				WL.Client.invokeProcedure(invocationData, {
					onSuccess : rrpmb01Success,
					onFailure : AdapterFail,	    		
					timeout: timeout
				});
			}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
			}
		};
		
		rrpmb01Success = function (result){
				invocationResult2 = result.invocationResult;
				reqParams = {};
				reqParams["RQLoginUserId"] = LoggedInUserID;
				reqParams["RQDeviceFamily"] = Device_Platform;
				reqParams["RQDeviceFormat"] = Device_Model;
				reqParams["RQOperationId"] = "VIWCHQSTS";
				reqParams["RQClientAPIVer"] = RQClientAPIVer;
				reqParams["SessionId"] = CurrentSessionId;
				reqParams["RQTransSeq"] = "01";
				fldjsessionid="";
				if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
					adapter : "API_Adapter",
					procedure : "GetAccounts",
					parameters : [fldjsessionid,reqParams],
					compressResponse : true
				};
    	
				WL.Logger.debug(invocationData, '');
				WL.Client.invokeProcedure(invocationData, {
					onSuccess : bankacntSuccess,
					onFailure : AdapterFail,
					timeout: timeout
				});
		};
		
		bankacntSuccess = function(result){
			invocationResult = result.invocationResult; //custbillerinfo
		    if (invocationResult.isSuccessful) {
                if (invocationResult.RBL) {
				accountListFt.removeAll();
				mobileList.removeAll();
					if(invocationResult.RBL.STATUS.CODE == '0') {
						acctdtls = invocationResult.RBL.Response.acctdetails;
						custdtls = invocationResult.RBL.Response.CustDetails;
						var idx = 1;
							$(acctdtls).each(function(index, obj) {
								strid = "item"+idx;		
								$(custdtls).each(function(j, obj1) {
									custnames="";
									if(obj.acctindex == obj1.acctindex){
										var actbal = "₹ "+formatAmt(parseFloat(obj.acctbalance));
										displaytxt = $.trim(obj.acctno)+"-"+obj.branchname;
										accountListFt.push({ codacctno: obj.acctno+"#"+obj1.custName+'#'+obj.acctbalance, acctType: obj.acctType, acctbalance: actbal, acctbranch: obj.branchname, custnames: custnames, namccyshrt: obj.currency, displaytxt: displaytxt, strid:strid });
									}
								});
								idx++;
						});
						
						$(invocationResult2.RBL.response.Mobile).each(function(index, obj) {
							displaytxt = obj.BillerName;
							value = obj.BillerID+"#"+obj.BillerName;
							mobileList.push({displaytxt:displaytxt,values:value});
						});
						
							
						$("#contentData").load("Views/recharge/rrpmb01.html", null, function (response, status, xhr) {
                            if (status != "error") {}
							
                            ko.applyBindings(self, $(".dynamic-page-content").get(0));
                 
						});
					}
				}else{
						busyInd.hide();
						handleError(invocationResult.RBL.Response, invocationResult.RBL);
				}
			}
			busyInd.hide();
		};
		
		rrpmb01Submit = function(){
			if($("#frmrrpmb01").valid()){
				$('#confirm_button1').prop('disabled', true);
				busyInd.show();
				var billerID = $('#fldoperator').val().split('#')[0];
				rhgfrmacnt($('#fldFromAcctNo').val().split('#')[0]);
				var subscriberID = $('#fldmobno').val();
				var rechargeAmount = parseFloat($('#fldAmtTxn').val());
				var customerID = LoggedInUserID;
				subscriber_no(subscriberID);
				recharge_amnt(rechargeAmount);
				biller_id(billerID);
				biller_name($('#fldoperator').val().split('#')[1]);
				dbtaccntnumber = rhgfrmacnt();
				if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
					adapter : "API_Adapter",
					procedure : "validateRecharge",
					parameters : [customerID, billerID, subscriberID, rechargeAmount,dbtaccntnumber]
					
				};
    	
				WL.Logger.debug(invocationData, '');
				WL.Client.invokeProcedure(invocationData, {
					onSuccess : validateRechargeSuccess,
					onFailure : AdapterFail,
					timeout: timeout
				});
			}
		};
		
		validateRechargeSuccess = function(result){
			invocationResult = result.invocationResult;
			if(invocationResult.validateRechargeResponse){
				if(invocationResult.validateRechargeResponse.validateRechargeReply){
						busyInd.hide();
						payment_id(invocationResult.validateRechargeResponse.validateRechargeReply.paymentID);
						rchgotp_key(invocationResult.validateRechargeResponse.validateRechargeReply.otpKey);
						window.location = "#rrpmb02";
				}
				else if(invocationResult.Fault){
					busyInd.hide();
					$('#confirm_button1').prop('disabled', false);
					navigator.notification.alert(invocationResult.Fault.Reason.Text);
				}else{
					$('#confirm_button1').prop('disabled', false);
					navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
					busyInd.hide();
				}
			}else if(invocationResult.Fault){
				busyInd.hide();
				navigator.notification.alert(invocationResult.Fault.Reason.Text);
			}else if(invocationResult.RBL){
				busyInd.hide();
				handleError(invocationResult.RBL.Response, invocationResult.RBL);
			}else{
				$('#confirm_button1').prop('disabled', false);
        		navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
        		busyInd.hide();
			}			
		};
				
		this.rrpmb02Page = function(){
			$("#contentData").load("Views/recharge/rrpmb02.html", null, function (response, status, xhr) {
                if (status != "error") {}
					busyInd.hide();
					$('#fldfromacctno').html(rhgfrmacnt());
					$('#fldmobno').html(subscriber_no());
					$('#fldmoboperator').html(biller_name());
					$('#fldamttxnamnt').html(recharge_amnt());
					$('#OTP_Ref').val(rchgotp_key());
                ko.applyBindings(self, $(".dynamic-page-content").get(0));
			});
		};
		
		rrpmb02Submit  = function(){
			if($("#frmrrpmb02").valid()){
				busyInd.show();
				var billerID = biller_id();
				var debitAccountID = $('#fldfromacctno').html();
				var subscriberID = $('#fldmobno').html();
				var rechargeAmount = parseFloat($('#fldamttxnamnt').html());
				
				var customerID = LoggedInUserID;
				var paymentID = payment_id();
				var otp = $('#OTP_No_Val').val();
				var otpKey = $('#OTP_Ref').val();
				
				if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
					adapter : "API_Adapter",
					procedure : "debitAccountAndRecharge",
					parameters : [customerID, billerID, subscriberID, rechargeAmount,paymentID,debitAccountID,otp,otpKey]
					
				};
    	
				WL.Logger.debug(invocationData, '');
				WL.Client.invokeProcedure(invocationData, {
					onSuccess : RechargeSuccess,
					onFailure : AdapterFail,
					timeout: timeout
				});
			}
		};
		
		RechargeSuccess = function(result){
		busyInd.hide();
			invocationResult = result.invocationResult;
			if(invocationResult.debitAccountAndRechargeResponse){
				if(invocationResult.debitAccountAndRechargeResponse.debitAccountAndRechargeReply){
					Rchgresponse = invocationResult.debitAccountAndRechargeResponse.debitAccountAndRechargeReply;
						$("#contentData").load("Views/recharge/rrpmb03.html", null, function (response, status, xhr) {
							if (status != "error") {}
							if(Rchgresponse.rechargeStatus.code == 'SUCCESS'){
								busyInd.hide();
								$('.success_msg').show();
								$('.bankacctno').html(rhgfrmacnt());
								$('.referenceno').html(Rchgresponse.TransactionID);
								$('.operator').html(biller_name());
								$('.subnumber').html(subscriber_no());
								$('.txnamnt').html(recharge_amnt());
								$('.txnStatus').html(Rchgresponse.rechargeStatus.description);
							}
							else{
								$(".rsaOOBErr").show();
								$(".rsaOOBErr p").html(Rchgresponse.rechargeStatus.description);
								$('.bankacctno').html(rhgfrmacnt());
								$('.referenceno').html(Rchgresponse.TransactionID);
								$('.operator').html(biller_name());
								$('.subnumber').html(subscriber_no());
								$('.txnamnt').html(recharge_amnt());
								$('.txnStatus').html(Rchgresponse.rechargeStatus.description);
							}
							ko.applyBindings(self, $(".dynamic-page-content").get(0));
							busyInd.hide();
						});
				}else{
					navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
					busyInd.hide();
				}
			}else if(invocationResult.Fault){
				busyInd.hide();
				navigator.notification.alert(invocationResult.Fault.Reason.Text);
			}else if(invocationResult.RBL){
				busyInd.hide();
				handleError(invocationResult.RBL.Response, invocationResult.RBL);
			}else{
        		navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
        		busyInd.hide();
			}	
		};
		
		//dth recharge
		this.rrdth01Page = function(){
			if(window.navigator.onLine){
				busyInd.show();
				if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
						adapter : "API_Adapter",
						procedure : "GetOperator",
						parameters : [],
						compressResponse : true
				};
						
				WL.Client.invokeProcedure(invocationData, {
					onSuccess : rrdth01Success,
					onFailure : AdapterFail,	    		
					timeout: timeout
				});
			}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
			}
		};
		
		rrdth01Success = function(result){
			invocationResult2 = result.invocationResult;
				busyInd.show();
				reqParams = {};
				reqParams["RQLoginUserId"] = LoggedInUserID;
				reqParams["RQDeviceFamily"] = Device_Platform;
				reqParams["RQDeviceFormat"] = Device_Model;
				reqParams["RQOperationId"] = "VIWCHQSTS";
				reqParams["RQClientAPIVer"] = RQClientAPIVer;
				reqParams["SessionId"] = CurrentSessionId;
				reqParams["RQTransSeq"] = "01";
				fldjsessionid="";
				if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
					adapter : "API_Adapter",
					procedure : "GetAccounts",
					parameters : [fldjsessionid,reqParams],
					compressResponse : true
				};
    	
				WL.Logger.debug(invocationData, '');
				WL.Client.invokeProcedure(invocationData, {
					onSuccess : bankdthacntSuccess,
					onFailure : AdapterFail,
					timeout: timeout
				});
		};
		
		bankdthacntSuccess = function(result){
			invocationResult = result.invocationResult; 
		    if (invocationResult.isSuccessful) {
                if (invocationResult.RBL) {
					accountListFt.removeAll();
					dthList.removeAll();
					if(invocationResult.RBL.STATUS.CODE == '0') {
						acctdtls = invocationResult.RBL.Response.acctdetails;
						custdtls = invocationResult.RBL.Response.CustDetails;
						var idx = 1;
							$(acctdtls).each(function(index, obj) {
								strid = "item"+idx;		
								$(custdtls).each(function(j, obj1) {
									custnames="";
									if(obj.acctindex == obj1.acctindex){
										var actbal = "₹ "+formatAmt(parseFloat(obj.acctbalance));
										displaytxt = $.trim(obj.acctno)+"-"+obj.branchname;
										accountListFt.push({ codacctno: obj.acctno+"#"+obj1.custName+'#'+obj.acctbalance, acctType: obj.acctType, acctbalance: actbal, acctbranch: obj.branchname, custnames: custnames, namccyshrt: obj.currency, displaytxt: displaytxt, strid:strid });
									}
								});
								idx++;
						});
						
						$(invocationResult2.RBL.response.DTH).each(function(index, obj) {
							displaytxt = obj.BillerName;
							value = obj.BillerID+"#"+obj.BillerName;
							dthList.push({displaytxt:displaytxt,values:value});
						});
							
						$("#contentData").load("Views/recharge/rrdth01.html", null, function (response, status, xhr) {
                            if (status != "error") {}		
                            ko.applyBindings(self, $(".dynamic-page-content").get(0));
						});
						busyInd.hide();
					}
				}else{
						busyInd.hide();
						handleError(invocationResult.RBL.Response, invocationResult.RBL);
				}
			}
			busyInd.hide();
		};
		
		rrdth01Submit = function(){
			if($("#frmrrdth01").valid()){
				$('#confirm_button1').prop('disabled', true);
				var billerID = $('#fldoperator').val().split('#')[0];
				rhgfrmacnt($('#fldFromAcctNo').val().split('#')[0]);
				var subscriberID = $('#fldsubno').val();
				var rechargeAmount = parseFloat($('#fldAmtTxn').val());
				var customerID = LoggedInUserID;
				subscriber_no(subscriberID);
				recharge_amnt(rechargeAmount);
				biller_id(billerID);
				biller_name($('#fldoperator').val().split('#')[1]);
				dbtaccntnumber = rhgfrmacnt();
				busyInd.show();
				if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
					adapter : "API_Adapter",
					procedure : "validateRecharge",
					parameters : [customerID, billerID, subscriberID, rechargeAmount,dbtaccntnumber]
					
				};
    	
				WL.Logger.debug(invocationData, '');
				WL.Client.invokeProcedure(invocationData, {
					onSuccess : validateDTHRechargeSuccess,
					onFailure : AdapterFail,
					timeout: timeout
				});
			}
		};
		
		validateDTHRechargeSuccess = function(result){
			invocationResult = result.invocationResult;
			if(invocationResult.validateRechargeResponse){
				if(invocationResult.validateRechargeResponse.validateRechargeReply){
					busyInd.hide();
						payment_id(invocationResult.validateRechargeResponse.validateRechargeReply.paymentID);
						rchgotp_key(invocationResult.validateRechargeResponse.validateRechargeReply.otpKey);
						window.location = "#rrdth02";
						
				}else{
					navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
					busyInd.hide();
				}
			}
			else if(invocationResult.Fault){
				busyInd.hide();
				$('#confirm_button1').prop('disabled', false);
				navigator.notification.alert(invocationResult.Fault.Reason.Text);
			}else if(invocationResult.RBL){
				busyInd.hide();
				handleError(invocationResult.RBL.Response, invocationResult.RBL);
			}else{
				$('#confirm_button1').prop('disabled', false);
        		navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
        		busyInd.hide();
			}				
		};
		
		this.rrdth02Page = function(){
			$("#contentData").load("Views/recharge/rrdth02.html", null, function (response, status, xhr) {
                if (status != "error") {}
					$('#fldfromacctno').html(rhgfrmacnt());
					$('#fldsubno').html(subscriber_no());
					$('#flddthoperator').html(biller_name());
					$('#fldamttxnamnt').html(recharge_amnt());
					$('#OTP_Ref').val(rchgotp_key());
                ko.applyBindings(self, $(".dynamic-page-content").get(0));
				busyInd.hide();
			});
		};
		
		rrdth02Submit  = function(){
			if($("#frmrrdth02").valid()){
				var billerID = biller_id();
				var debitAccountID = $('#fldfromacctno').html();
				var subscriberID = $('#fldsubno').html();
				var rechargeAmount = parseFloat($('#fldamttxnamnt').html());
				var customerID = LoggedInUserID;
				var paymentID = payment_id();
				var otp = $('#OTP_No_Val').val();
				var otpKey = $('#OTP_Ref').val();
				busyInd.show();
				if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
					adapter : "API_Adapter",
					procedure : "debitAccountAndRecharge",
					parameters : [customerID, billerID, subscriberID, rechargeAmount,paymentID,debitAccountID,otp,otpKey]
					
				};
    	
				WL.Logger.debug(invocationData, '');
				WL.Client.invokeProcedure(invocationData, {
					onSuccess : RechargedthSuccess,
					onFailure : AdapterFail,
					timeout: timeout
				});
			}
		};
		
		RechargedthSuccess = function(result){
		busyInd.hide();
			invocationResult = result.invocationResult;
			if(invocationResult.debitAccountAndRechargeResponse){
				if(invocationResult.debitAccountAndRechargeResponse.debitAccountAndRechargeReply){
					Rchgresponse = invocationResult.debitAccountAndRechargeResponse.debitAccountAndRechargeReply;
						$("#contentData").load("Views/recharge/rrdth03.html", null, function (response, status, xhr) {
							if (status != "error") {}
							if(Rchgresponse.rechargeStatus.code == 'SUCCESS'){
								$('.success_msg').show();
								$('.bankacctno').html(rhgfrmacnt());
								$('.referenceno').html(Rchgresponse.TransactionID);
								$('.operator').html(biller_name());
								$('.subnumber').html(subscriber_no());
								$('.txnamnt').html(recharge_amnt());
								$('.txnStatus').html(Rchgresponse.rechargeStatus.description);
								busyInd.hide();
							}
							else{
								$(".rsaOOBErr").show();
								$(".rsaOOBErr p").html(Rchgresponse.rechargeStatus.description);
								$('.bankacctno').html(rhgfrmacnt());
								$('.referenceno').html(Rchgresponse.TransactionID);
								$('.operator').html(biller_name());
								$('.subnumber').html(subscriber_no());
								$('.txnamnt').html(recharge_amnt());
								$('.txnStatus').html(Rchgresponse.rechargeStatus.description);
								busyInd.hide();
							}
							ko.applyBindings(self, $(".dynamic-page-content").get(0));
							busyInd.hide();
						});
						busyInd.hide();
				}else{
					navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
					busyInd.hide();
				}
			}else if(invocationResult.Fault){
				busyInd.hide();
				navigator.notification.alert(invocationResult.Fault.Reason.Text);
			}else if(invocationResult.RBL){
				busyInd.hide();
				handleError(invocationResult.RBL.Response, invocationResult.RBL);
			}else{
        		navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
        		busyInd.hide();
			}	
		};
	
		//Recharge Status
		this.rrcrs01Page = function(){
			$("#contentData").load("Views/recharge/rrcrs01.html", null, function (response, status, xhr) {
                if (status != "error") {}                         
                ko.applyBindings(self, $(".dynamic-page-content").get(0));                   
            });
		};
		
		rrcrs01Submit = function(){
			if(window.navigator.onLine){
				if($("#frmstatus01").valid()){
					var customerID = LoggedInUserID;
					busyInd.show();
					var TransactionID = $('#fldTxnRefNo').val().toUpperCase();
					if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
							adapter : "API_Adapter",
							procedure : "getRechargeStatus",
							parameters : [customerID,TransactionID],
							compressResponse : true
					};
							
					WL.Client.invokeProcedure(invocationData, {
						onSuccess : rrcsr01Success,
						onFailure : AdapterFail,	    		
						timeout: timeout
					});
				}
			}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
			}
		};
		
		rrcsr01Success = function(result){
			invocationResult = result.invocationResult;
			if(invocationResult.getRechargeStatusResponse){
				if(invocationResult.getRechargeStatusResponse.getRechargeStatusReply){
					busyInd.hide();
					if(window.navigator.onLine){
						busyInd.show();
						if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
							adapter : "API_Adapter",
							procedure : "GetOperator",
							parameters : [],
							compressResponse : true
						};
						WL.Client.invokeProcedure(invocationData, {
							onSuccess : getoperatorsucess,
							onFailure : AdapterFail,	    		
							timeout: timeout
						});
					}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
					}
				}else{
					navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
					busyInd.hide();
				}	
			}else if(invocationResult.Fault){
					busyInd.hide();
					navigator.notification.alert(invocationResult.Fault.Reason.Text);
			}else if(invocationResult.RBL){
				busyInd.hide();
				handleError(invocationResult.RBL.Response, invocationResult.RBL);
			}else{
					navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
					busyInd.hide();
			}
		};
		
		getoperatorsucess = function(result){
			invocationResult3 = result.invocationResult;
			var id = invocationResult.getRechargeStatusResponse.getRechargeStatusReply.billerID;
			var temp = getObjects(invocationResult3, 'BillerID', id);
			biller_name(temp[0].BillerName);
			window.location="#rrcrs02";
		};
		
		this.rrcrs02Page = function(){
			busyInd.hide();
			$("#contentData").load("Views/recharge/rrcrs02.html", null, function (response, status, xhr) {
				busyInd.hide();
                if (status != "error") {}
					$('#operator_name').html(biller_name());
					$('#subcriber_no').html(invocationResult.getRechargeStatusResponse.getRechargeStatusReply.subscriberID);
					$('#rchg_amnt').html(invocationResult.getRechargeStatusResponse.getRechargeStatusReply.rechargeAmount);
					$('#rchg_status').html(invocationResult.getRechargeStatusResponse.getRechargeStatusReply.rechargeStatus.description);
					$('#txn_ref_no').html(invocationResult.getRechargeStatusResponse.getRechargeStatusReply.bankRefNo);
					$('#txn_date').html(invocationResult.getRechargeStatusResponse.getRechargeStatusReply.paymentDate);
                ko.applyBindings(self, $(".dynamic-page-content").get(0));                   
            });
		};
		
		function getObjects(obj, key, val) {
			var objects = [];
			for (var i in obj) {
				if (!obj.hasOwnProperty(i)) continue;
				if (typeof obj[i] == 'object') {
					objects = objects.concat(getObjects(obj[i], key, val));
				} else if (i == key && obj[key] == val) {
					objects.push(obj);
				}
			}
			return objects;
		}
};