
/* JavaScript content from ViewModels/login/loginViewModel.js in folder common */
var loginViewModel = function () {

			var self = this;
			self.selAccount = ko.observable();
			self.accountStmtTxns = ko.observableArray([]);
			self.selAccount = ko.observable();
			self.fromAccountList = ko.observableArray([]);
			self.toAccountList = ko.observableArray([]);
			self.fldFromAcctNo = ko.observable();
			self.fldToAcctNo = ko.observable();
			self.benetype = ko.observable('');
			self.selAccount = ko.observable();
			self.selAcctTemp = ko.observable();
			self.fldmobileemail = ko.observable();
			self.rtgsTxnList = ko.observableArray([]);
			self.fldSelTxnRefNo = ko.observable();

			 
			//login customer id submit function
		self.custIdSubmit = function(){
		if(window.navigator.onLine){
			if($("#frmLogin").valid()){
				var fldloginUser = $("#fldLoginUserId").val();
				if(fldloginUser == "" || fldloginUser == null){	
					navigator.notification.alert('Kindly register to proceed!');
					//$("#frmLogin").clearMpin();
				}
				else{
					isnum = /^\d+$/.test(fldloginUser);
					if(isnum){
						fldloginUser = fldloginUser;
					}
					else{
						fldloginUser = encryptXorNumber(fldloginUser,enckey);
					}
					//if($("#frmLogin").valid()){
						var encodedPwd = btoa($("#fldLoginUserPwd").val());
						var encodedPwd1 = booksStore(encodedPwd);

						reqParams = {};
					//	reqParams["RQDeviceId"] = "9876543217";
						reqParams["RQLoginUserId"] = fldloginUser;
						reqParams["RQDeviceFamily"] = "Android";
						reqParams["RQDeviceFormat"] = "Tablet";
						reqParams["RQOperationId"] = "USRLGNREQ"; 
						//reqParams["RQLoginPwd"] = encodedPwd;
						reqParams["RQClientAPIVer"] = RQClientAPIVer;
						reqParams["SessionId"] = "1990009769OKYLLDDP";
						reqParams["RQTransSeq"] = "01";
						
						if(deviceId != null){
							if(deviceId != ""){
								reqParams["RQDeviceId"] = deviceId;
							}
						}
			
						
						fldjsessionid = "";
						
						busyInd.show();
							var invocationData = {
								adapter : "API_Adapter",
								procedure : "Getlogin",
								parameters : [fldjsessionid,reqParams,encodedPwd1],
								compressResponse : true
							};
						
							WL.Client.invokeProcedure(invocationData, {
								onSuccess : CustIdSuccess,
								onFailure : AdapterFail,	    		
								timeout: timeout
							});
							
					}
				
			}
		}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
		}
		};
			
		CustIdSuccess = function(result){
			//console.log(JSON.stringify(result));
			accountList.removeAll();
			accountListFt.removeAll();
			CurrentSessionId = "";
			LoggedInUserID = "";
			invocationResult = result.invocationResult;
			if (invocationResult.isSuccessful) {
				if(invocationResult.RBL){
					if(invocationResult.RBL.Response){
						ch = noggers(result,invocationResult);
						error = invocationResult.RBL.Response.errors;
						if(ch!=error){
								window.location.href = "#logout";
						}
						if(invocationResult.RBL.STATUS.CODE==0 && ch==error){
							lgnout = true;
							isnum = /^\d+$/.test(localStorage.getItem("RegisteredUser"));
							if(isnum){
								var temp = localStorage.getItem("RegisteredUser");
								localStorage.setItem("RegisteredUser",encryptXorNumber(temp,enckey));
							}
							else{
								var temp = encryptXorNumber(localStorage.getItem("RegisteredUser"),enckey);
							}
							//var temp = encryptXorNumber(localStorage.getItem("RegisteredUser"),enckey);
							var currentuser = temp;
							currentreguserid = currentuser;
							fldloginUser = invocationResult.RBL.Response.customerid;
							if(currentuser == fldloginUser){
								LoggedInUserName = invocationResult.RBL.STATUS.CUSTOMERNAME;
								LoggedInUserID = currentuser;
								var username = fldloginUser;
								
								var invocationData = {
									adapter : "API_Adapter",
									procedure : "submitAuthentication",
									parameters : [ fldloginUser, fldloginUser ]
								};

								adapterAuthRealmChallengeHandler.submitAdapterAuthentication(invocationData, {});
									
									var today = new Date();
									var dd = today.getDate();
									var mm = today.getMonth()+1; //January is 0!
									var yyyy = today.getFullYear();
									if(dd<10) {
										dd='0'+dd
									} 
									if(mm<10) {
										mm='0'+mm
									} 
									today = mm+dd+yyyy;
									function checkTime(i) {
										if (i < 10) {
											i = "0" + i;
										}
										return i;
									}
									 var today1 = new Date();
										var h = today1.getHours();
										var m = today1.getMinutes();
										var s = today1.getSeconds();
										// add a zero in front of numbers<10
										m = checkTime(m);
										s = checkTime(s);
										
										var today1 = h+''+m+''+s;
									
									var sessionID = fldloginUser+today+today1;
									//localStorage.setItem("sessionID", sessionID);
									CurrentSessionId = sessionID;
									//window.location = "#Home";
									busyInd.hide();
							}
							else{
								busyInd.hide();
								navigator.notification.alert('Invalid Userid or Password');
							}
						}
						else{
							busyInd.hide();
							CurrentSessionId = "";
							navigator.notification.alert(invocationResult.RBL.STATUS.CODE+": "+invocationResult.RBL.STATUS.MESSAGE);
						}
					}
					else{
						handleError(invocationResult.RBL.response);
					}
				}else{
						busyInd.hide();
						navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
				}
			}
			else{
				busyInd.hide();
			}
			busyInd.hide();
		};
		
		self.logout = function(){
			busyInd.show();
	        			lgnout = false;
						$(".h_title").html("Login");
	        			WL.Client.setUserPref("fldFCDBSessionId","");
	        			
	        			//localStorage.setItem("sessionID", "");
	        			WL.Client.setUserPref("loginFlag","no");
	        			WL.Client.setUserPref("loggeduserid","");
	        			accountList.removeAll();
						accountListFt.removeAll();
						CurrentSessionId = "";
						LoggedInUserID = "";
	        			window.location = "#login";
						var invocationData = {
								adapter : "API_Adapter",
								procedure : "CLogout",
								parameters : [],
								compressResponse : true
						};
						
						WL.Client.invokeProcedure(invocationData, {
							onSuccess : function LoutSuccess(){lgnout=false;},
							onFailure : function LoutFailure(){lgnout=false;},	    		
							timeout: timeout
						});
						WL.Client.logout('AdapterAuthRealm');
	        			busyInd.hide();
						navigator.notification.alert("Logged out successfully");
						//busyInd.hide();
	        };

		//Customer Registration using Internet Banking
		self.new_user_reg1Submit = function(){
		if(window.navigator.onLine){

			if($("#new_user_reg1").valid()){
			
					var fldloginUser = $("#userId").val();
					var encodedPwd = btoa($("#Userpwd").val());
					var encodedPwd1 = booksStore(encodedPwd);
					
					busyInd.show();
					reqParams = {};
					reqParams["RQLoginUserId"] = fldloginUser;
					reqParams["RQDeviceFamily"] = "Android";
					reqParams["RQDeviceFormat"] = "Tablet";
					reqParams["RQOperationId"] = "INTREGREQ"; 
					//reqParams["RQLoginPwd"] = encodedPwd1;
					reqParams["RQClientAPIVer"] = RQClientAPIVer;
					reqParams["SessionId"] = "1990009769OKYLLDDP";
					reqParams["RQTransSeq"] = "01";
				    
					fldjsessionid = "";
					
						var invocationData = {
							adapter : "API_Adapter",
							procedure : "GetInternetReg",
							parameters : [fldjsessionid,reqParams,encodedPwd1],
							compressResponse : true
						};
					
						WL.Client.invokeProcedure(invocationData, {
							onSuccess : NetRegSucess,
							onFailure : AdapterFail,	    		
							timeout: timeout
						});
			}
		}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
		}
			
		};
		
		NetRegSucess = function(result){
			invocationResult = result.invocationResult;
			if(invocationResult.RBL){
				if(invocationResult.RBL.Response){
					if(invocationResult.RBL.STATUS.CODE=="0"){
						$("#contentData").load("Views/Menu/new_user_reg2.html", null, function (response, status, xhr) {
							var temp = encryptXorNumber(invocationResult.RBL.Response.customerid,enckey);
							localStorage.setItem("Tempcustid",temp);
							$('#cust_id').html(invocationResult.RBL.Response.customerid);
							$('#OTP_Ref').val(invocationResult.RBL.Response.otprefno);
							 ko.applyBindings(self, $(".dynamic-page-content").get(0)); 
						});
					}
					else if(invocationResult.RBL.STATUS.CODE==17000){
							busyInd.hide();
							$('.popWrap.reg').parent().show(0);
							$('.popWrap.reg').parent().addClass('show');
							$('.popWrap.reg').addClass('show');		
						}
					else{
						navigator.notification.alert(invocationResult.RBL.STATUS.CODE+": "+invocationResult.RBL.STATUS.MESSAGE);
					}
				}
				else{
					handleError(invocationResult.RBL.response);
				}
			}else{
				busyInd.hide();
        		navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
        	}
			busyInd.hide();
		};
		
		//Final Registration
		self.new_user_reg2Submit = function(){
				if(window.navigator.onLine){
					if($("#new_user_reg2").valid()){
							var fldloginUser = $("#cust_id").text();
							var mpin = btoa($("#mpin").val());
							var booksmn = booksStore(mpin);
							
							reqParams = {};
							//reqParams["RQDeviceId"] = "9876543217";
							reqParams["RQLoginUserId"] = fldloginUser;
							reqParams["RQDeviceFamily"] = "Android";
							reqParams["RQDeviceFormat"] = "Tablet";
							reqParams["RQOperationId"] = "USRREGREQ"; 
							reqParams["RQOTP"] =  $("#OTP_No_Val").val();
							reqParams["RQRefNo"]=  $("#OTP_Ref").val();
							//reqParams["RQMPIN"]= mpin;
							//reqParams["RQClientAPIVer"] = "1.0";
							reqParams["RQClientAPIVer"] = RQClientAPIVer;
							reqParams["SessionId"] = "1990009769OKYLLDDP";
							reqParams["RQTransSeq"] = "01";
							
							if(deviceId != null){
								if(deviceId != ""){
									reqParams["RQDeviceId"] = deviceId;
								}
							}
				
						    
							fldjsessionid = "";
							
							busyInd.show();
								var invocationData = {
									adapter : "API_Adapter",
									procedure : "GetInternetSubmit",
									parameters : [fldjsessionid,reqParams,booksmn],
									compressResponse : true
								};
							
								WL.Client.invokeProcedure(invocationData, {
									onSuccess : NetRegSubmit,
									onFailure : AdapterFail,	    		
									timeout: timeout
								});
					}
					}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
				}

				};
				
		
				NetRegSubmit = function(result){
					LoggedInUserID = "";
					invocationResult = result.invocationResult;
					if(invocationResult.RBL){
						if(invocationResult.RBL.STATUS.CODE=="0"){
							var temp = encryptXorNumber(localStorage.getItem("Tempcustid"),enckey);
							if(temp == invocationResult.RBL.STATUS.customerid && temp == invocationResult.RBL.RequestParams.RQLoginUserId){
								$("#contentData").load("Views/Menu/new_user_reg3.html", null, function (response, status, xhr) {
									$('.stepTitle').show();
									var RegisteredUser = invocationResult.RBL.RequestParams.RQLoginUserId;
									var temp = encryptXorNumber(RegisteredUser,enckey);
									localStorage.setItem("RegisteredUser", temp);
									ko.applyBindings(self, $(".dynamic-page-content").get(0)); 
									
									PushNotificationWineCall();
									
								});
							}
							else{
								busyInd.hide();
								window.location.hash = '#menu';
							}
						}
						else{
							busyInd.hide();
							navigator.notification.alert(invocationResult.RBL.STATUS.CODE+": "+invocationResult.RBL.STATUS.MESSAGE);
						}
					}else{
						busyInd.hide();
		        		navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
					}
					busyInd.hide();
				};

		
		//Customer Registration using Personal 
		self.PersonalRegSubmit = function(){
			if(window.navigator.onLine){

			if($("#PersonalReg").valid()){
					from = $("#fldFromDate").val().split("/");
					date = from[0]+from[1]+from[2];
					reqParams = {};
					
					reqParams["RQPAN"] = $('#PanNo').val().toUpperCase();
					reqParams["RQDeviceFamily"] = "Android";
					reqParams["RQDeviceFormat"] = "Tablet";
					reqParams["RQOperationId"] = "PERREGREQ"; 
					reqParams["RQPinCode"] = $('#PinCode').val();
					reqParams["RQDOB"] = date;
					reqParams["RQClientAPIVer"] = RQClientAPIVer;
					reqParams["SessionId"] = "1990009769OKYLLDDP";
					reqParams["RQTransSeq"] = "01";
				    
					fldjsessionid = "";
					
					busyInd.show();
						var invocationData = {
							adapter : "API_Adapter",
							procedure : "GetPersonalReg",
							parameters : [fldjsessionid,reqParams],
							compressResponse : true
						};
					
						WL.Client.invokeProcedure(invocationData, {
							onSuccess : NetRegSucess,
							onFailure : AdapterFail,	    		
							timeout: timeout
						});
			}
			}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
		}

		};
		
		//Customer Registration using Debit card
		self.DebitRegSubmit  = function(){
			if(window.navigator.onLine){
			if($("#Debit_reg").valid()){					
					var	customerID=$('#userId').val();
					var cardNumber=$('#Debitcard').val();
					var pin=btoa($('#PinNo').val());
					var pinBooksStore=booksStore(pin);
					
				reqParams = {};
				reqParams["RQLoginUserId"] = customerID;
				reqParams["RQDeviceFamily"] = "Android";
				reqParams["RQDeviceFormat"] = "Tablet";
				reqParams["RQOperationId"] = "PERREGREQ";
				reqParams["RQClientAPIVer"] = RQClientAPIVer;
				reqParams["SessionId"] = "1990009769OKYLLDDP";
				reqParams["RQTransSeq"] = "01";
				//reqParams["RQPIN"] = pin;
				reqParams["RQDC"] = cardNumber;
				fldjsessionid="";
			
				busyInd.show();			
				var invocationData = {
						adapter : "API_Adapter",
						procedure : "GetDebitReg",
						parameters : [fldjsessionid,reqParams,pinBooksStore],
						compressResponse : true
					};
					WL.Client.invokeProcedure(invocationData, {
						onSuccess : NetRegSucess,
						onFailure : AdapterFail,	    		
						timeout: timeout
					});
			}
			}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
		}

		};


		
		//Password change
		self.PwdChangeSubmit = function(){
		if(window.navigator.onLine){
			if($("#Pwd_change").valid()){
				
				
				var oldmpin = btoa($('#OldPwd').val());
				var oldmpinBooksStore=booksStore(oldmpin);
				
				var mpin = btoa($('#NewPwd').val());
				var mpinBooksStore=booksStore(mpin);
				
					
					reqParams = {};
					reqParams["RQLoginUserId"] = $('#userId').text();
					reqParams["RQDeviceFamily"] = "Android";
					reqParams["RQDeviceFormat"] = "Tablet";
					reqParams["RQOperationId"] = "USRREGREQ"; 
					//reqParams["RQOldMPIN"] = btoa($('#OldPwd').val());
					//reqParams["RQMPIN"] = btoa($('#NewPwd').val());
					reqParams["RQClientAPIVer"] = RQClientAPIVer;
					reqParams["SessionId"] = "1990009769OKYLLDDP";
					reqParams["RQTransSeq"] = "01";
					
					if(deviceId != null){
						if(deviceId != ""){
							reqParams["RQDeviceId"] = deviceId;
						}
					}
		
				    
					fldjsessionid = "";
					
					busyInd.show();
						var invocationData = {
							adapter : "API_Adapter",
							procedure : "PasswordChange",
							parameters : [fldjsessionid,reqParams,oldmpinBooksStore,mpinBooksStore],
							compressResponse : true
						};
					
						WL.Client.invokeProcedure(invocationData, {
							onSuccess : PwdChangeSucess,
							onFailure : AdapterFail,	    		
							timeout: timeout
						});
			}
			}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
		}

		};
		
		PwdChangeSucess = function(result){
			invocationResult = result.invocationResult;
			if(invocationResult.RBL.STATUS.CODE=="0"){
				busyInd.hide();
				$("#contentData").load("Views/login/PwdChangeSucess.html", null, function (response, status, xhr) {
					 ko.applyBindings(self, $(".dynamic-page-content").get(0)); 
				});
			}
			else{
				busyInd.hide();
				navigator.notification.alert(invocationResult.RBL.STATUS.CODE+": "+invocationResult.RBL.STATUS.MESSAGE);
			}
			busyInd.hide();
		};
		
		self.Aadhar = function(){
			$("#contentData").load("Views/Menu/Aadhar.html", null, function (response, status, xhr) {
				if (status != "error") {}
				 ko.applyBindings(self, $(".content").get(0));                   
			});
		};
		
		aadhar_Submit = function(){
		if(window.navigator.onLine){
			if($("#aadhar_Submit").valid()){
					aadhar = $('#input1').val()+""+$('#input2').val()+""+$('#input3').val();
				
					reqParams = {};
					reqParams["RQAadharNo"] = aadhar;
					reqParams["RQDeviceFamily"] = "Android";
					reqParams["RQDeviceFormat"] = "Tablet";
					reqParams["RQOperationId"] = "IMPSP2AFT"; 
					reqParams["RQMobNo"] = $('#fldmobno').val();
					reqParams["RQClientAPIVer"] = RQClientAPIVer;
					reqParams["RQTransSeq"] = "01";
				    
					fldjsessionid = "";
					
					busyInd.show();
						var invocationData = {
							adapter : "API_Adapter",
							procedure : "Aadharcard",
							parameters : [fldjsessionid,reqParams],
							compressResponse : true
						};
					
						WL.Client.invokeProcedure(invocationData, {
							onSuccess : AadharcardSucess,
							onFailure : AdapterFail,	    		
							timeout: timeout
						});
			}
			}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
		}

		};
		
		AadharcardSucess = function(result){
			invocationResult = result.invocationResult;
			if(invocationResult.RBL.aadharlookup.status.code == '0001'){
				if(invocationResult.RBL.aadharlookup.aadharStatusResponse){
					busyInd.hide();
					$("#contentData").load("Views/Menu/Aadhar-summary.html", null, function (response, status, xhr) {
						ko.applyBindings(self, $(".dynamic-page-content").get(0));
							$('.summblock').show();
							$('#aadhaarNumber').html(invocationResult.RBL.aadharlookup.aadharStatusResponse.aadhaarNumber);
							$('#bankName').html(invocationResult.RBL.aadharlookup.aadharStatusResponse.bankName);
							$('#mobileNumber').html(invocationResult.RBL.aadharlookup.aadharStatusResponse.mobileNumber);
							$('#status').html(invocationResult.RBL.aadharlookup.aadharStatusResponse.status);
							$('#lastUpdatedDate').html(invocationResult.RBL.aadharlookup.aadharStatusResponse.lastUpdatedDate.split('.')[0]);
							$('#processedDateTimeStamp').html(invocationResult.RBL.aadharlookup.aadharStatusResponse.processedDateTimeStamp.split('.')[0]);
							$('#requestNumber').html(invocationResult.RBL.aadharlookup.aadharStatusResponse.requestNumber);
							if(invocationResult.RBL.aadharlookup.aadharStatusResponse.mandateFlag == 'Y'){
								$('#mandateflag').html('You have given the mandate to link Aadhaar number to account.');
								$('#mandatedate1').show();
								$('#mandatedate').html(invocationResult.RBL.aadharlookup.aadharStatusResponse.mandateCustDate);
							}
							else{
								$('#mandateflag').html('You have not given the mandate to link Aadhaar number to account.');
							}
							if(invocationResult.RBL.aadharlookup.aadharStatusResponse.ODFlag == 'Y'){
								$('#Overdraftflag').html('Overdraft facility has been given to your account.');
								//$('#Overdraftdate1').show();								//$('#Overdraftdate').html(invocationResult.RBL.aadharlookup.aadharStatusResponse.mandateCustDate);
							}
							else{
								$('#Overdraftflag').html('Overdraft facility has not been given to your account.');
							}
					});
				}
				else{
					 busyInd.hide();
					 navigator.notification.alert(invocationResult.RBL.aadharlookup.status.code+": "+invocationResult.RBL.aadharlookup.status.message);
				}
			}
			else{
					busyInd.hide();
					navigator.notification.alert(invocationResult.RBL.aadharlookup.status.code+": "+invocationResult.RBL.aadharlookup.status.message);
			}
			busyInd.hide();
		};

		
	function onNetworkCheck(){
		return ;
	}
	
		
};
			

		