
/* JavaScript content from ViewModels/CreditCardViewModel.js in folder common */
﻿var CreditCardViewModel = function () {
    var self = this;
	selAccountCC = ko.observable();
	curraccbalvalcc = ko.observable();
	frmacntcc = "";
	ccmonths = "";
	ccyear = "";
	cccvv = "";
	transID = '';
	totalDRbal = 0;
	totalCRbal = 0;
	totalDue = '';
	mindue = '';
	firstcard = 'false';
	checkflag = 'false';
	ccnoval='';
	CCbdate = '';
	GetOTP = function(){
		if(window.navigator.onLine){
			 busyInd.show();
				reqParams = {};
				reqParams["RQLoginUserId"] = LoggedInUserID;
				reqParams["RQDeviceFamily"] = Device_Platform;
				reqParams["RQDeviceFormat"] = Device_Model;
				reqParams["RQOperationId"] = "OTPGENREQ";
				reqParams["RQClientAPIVer"] = RQClientAPIVer;
				reqParams["SessionId"] = CurrentSessionId;
				reqParams["RQTransSeq"] = "01";
				fldjsessionid="";
				
				//busyInd.show();
				if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
					adapter : "API_Adapter",
					procedure : "GetOTP",
					parameters : [fldjsessionid,reqParams],
					compressResponse : true
				};
    	
				WL.Client.invokeProcedure(invocationData, {
					onSuccess : GetOTPResponse,
					onFailure : AdapterFail,
					timeout: timeout
				});
		}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
		}

		};	
	   
		GetOTPResponse = function(result){
			invocationResult1 = result.invocationResult;
				if(invocationResult1.isSuccessful) {
					if(invocationResult1.RBL.Response){	
						if(window.location.hash == "#CC_register"){
							OTP_number = invocationResult1.RBL.Response.otprefno;
							$('#OTP_Ref').val(OTP_number);
							CCRegConfirm(OTP_number);
						}
						if(window.location.hash == '#Credit_card_EMIdet'){
							OTP_number = invocationResult1.RBL.Response.otprefno;
							$('#OTP_Ref').val(OTP_number);
							CCconfirmPlan1(OTP_number);
						}
						if(window.location.hash == '#CCPaymentHome'){
							OTP_number = invocationResult1.RBL.Response.otprefno;
							$('#OTP_Ref').val(OTP_number);
							CCPayConfirm(OTP_number);
						}
						else{
							OTP_number = invocationResult1.RBL.Response.otprefno;
							$('#OTP_Ref').val(OTP_number);
						}
					}else{
							busyInd.hide();
							handleError(invocationResult1.RBL.Response, invocationResult1.RBL);
					}		
				}
			busyInd.hide();
		};

	
	
		self.getCCaccounts = function(){ 
			if(window.navigator.onLine){
				firstcard = 'true';
					busyInd.show();
					var customerID = LoggedInUserID;
					acntnumber = $("#fdAcctNo option:selected").val();
					if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
						adapter : "API_Adapter",
						procedure : "CCgetAllAccounts",
						parameters : [customerID]
					};
			
					WL.Logger.debug(invocationData, '');
					WL.Client.invokeProcedure(invocationData, {
						onSuccess : CCacntsSuccess,
						onFailure : AdapterFail,
						timeout: timeout
					});
			}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
			}
		};
		
		CCacntsSuccess = function(result){
			invocationResult = result.invocationResult;
			if(invocationResult.getAllRegisteredCardsResponse){
				if(invocationResult.getAllRegisteredCardsResponse.numCards > 0){
					busyInd.hide();
					ccaccountList.removeAll();
					ccAccountsPay.removeAll();
					var idx = 1;
					itemdata = invocationResult.getAllRegisteredCardsResponse.cardsArray.card;
						$(itemdata).each(function(index, obj){
							displaytxt = obj.cardNo+"-"+obj.productName;
							availableCreditLimit = "₹ " +formatAmt(parseFloat(obj.availableCreditLimit));
							codacctno = obj.cardNo;
							temp = codacctno.length;
							temp1 = temp-4;
							cardno = "xxxxxxxxxxxx"+codacctno.substring(temp1,temp);
							displaytxt = cardno+"-"+obj.productName;
							if(obj.isPrimary == true){
								var cardtype = "Primary";
								ccAccountsPay.push({codacctno:obj.cardNo,displaytxt:displaytxt,availableCreditLimit:availableCreditLimit,productName:obj.productName,isPrimary:obj.isPrimary,currencyCode:obj.currencyCode,cardno:cardno});
							}
							else{
								var cardtype = "Add on Card";
							}
							ccaccountList.push({codacctno:obj.cardNo,displaytxt:displaytxt,availableCreditLimit:availableCreditLimit,productName:obj.productName,isPrimary:obj.isPrimary,currencyCode:obj.currencyCode,cardno:cardno,cardtype:cardtype});
						});
						if(window.location.hash == '#CC_register'){window.location.hash = '#register_sucess';}
						else{window.location.hash = '#Credit_card_acnts';}
				}
				else if(invocationResult.getAllRegisteredCardsResponse.numCards == "0"){
					busyInd.hide();
					//navigator.notification.alert("You Do not have any Credit Card Mapped.");
					$('.popWrap.cc').parent().show(0);
					$('.popWrap.cc').parent().addClass('show');
					$('.popWrap.cc').addClass('show');		
				}
				else if(invocationResult.Fault){
					busyInd.hide();
					navigator.notification.alert(invocationResult.Fault.Reason.Text);
				}else{
					navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
					busyInd.hide();
				}
			}else if(invocationResult.Fault){
				busyInd.hide();
				navigator.notification.alert(invocationResult.Fault.Reason.Text);
			}else if(invocationResult.RBL.hashid){
				busyInd.hide();
						handleError(invocationResult.RBL.Response, invocationResult.RBL);
			}
			else{
        		navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
        		busyInd.hide();
			}			
		};


	
	    showSelectedAccountCC = function(){
        	selaccno = selAccountCC();
        	accdata = ccaccountList();
        	
        	if(selaccno != '' && selaccno != null && selaccno != undefined){
				$(accdata).each(function(index, accnodet) {
					if(accnodet.codacctno == selaccno){
						selectedAccountcc({ accno: accnodet.codacctno, displaytxt: accnodet.displaytxt, acctbalance: accnodet.availableCreditLimit});
						
						var currAccData = selectedAccountcc();
						fldAcctNo = currAccData.accno;            
						curraccbalvaluecc = currAccData.acctbalance;
						curraccbalvalcc(curraccbalvaluecc);
						busyInd.hide();
					}
				});
        	} busyInd.hide();
        };
		
		self.getccCardinformation = function(){
			cardno = $('#fldAcctNo').val();
			cardinfo(cardno);
		};
		
		cardinfo = function(acntno){
			if(window.navigator.onLine){
					busyInd.show();
					var customerID = LoggedInUserID;
					cardnumber = acntno;
					
					if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
						adapter : "API_Adapter",
						procedure : "CCgetCardDetail",
						parameters : [customerID,cardnumber]
					};
			
					WL.Logger.debug(invocationData, '');
					WL.Client.invokeProcedure(invocationData, {
						onSuccess : CCgetCardDetailSuccess,
						onFailure : AdapterFail,
						timeout: timeout
					});
			}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
			}
		};
		
		CCgetCardDetailSuccess = function(result){
			invocationResult = result.invocationResult;
			if(invocationResult.getCardDetailResponse){
				busyInd.hide();
				CCcardinfo.removeAll();
				cardinfo = invocationResult.getCardDetailResponse
							totalCreditLimit = "₹ " +formatAmt(parseFloat(cardinfo.totalCreditLimit));
							availableCreditLimit = "₹ " +formatAmt(parseFloat(cardinfo.availableCreditLimit));
							availableCashLimit = "₹ " +formatAmt(parseFloat(cardinfo.availableCashLimit));
							minimumAmountDue = "₹ " +formatAmt(parseFloat(cardinfo.minimumAmountDue));
							totalAmountDue = "₹ " +formatAmt(parseFloat(cardinfo.totalAmountDue));
							if(cardinfo.paymentDuedate == "2199-12-31"){
								paymentDuedate = "0000/00/00";
							}
							if(cardinfo.paymentDuedate == "1950-01-01"){
								paymentDuedate = "0000/00/00";
							}
							else{
								dateformatting(cardinfo.paymentDuedate);
								paymentDuedate = dateformat;
							}
							
					codacctno = cardinfo.cardNo;
					temp = codacctno.length;
					temp1 = temp-4;
					cardno = "xxxxxxxxxxxx"+codacctno.substring(temp1,temp);		
				CCcardinfo.push({acctnumber:cardno, produnctname:cardinfo.productName,totalCreditLimit:totalCreditLimit,availableCreditLimit:availableCreditLimit,availableCashLimit:availableCashLimit,minimumAmountDue:minimumAmountDue,paymentDuedate:paymentDuedate,totalUnbilledDebits:cardinfo.totalUnbilledDebits,totalUnbilledCredits:cardinfo.totalUnbilledCredits,rewardsPointBalance:cardinfo.rewardsPointBalance,totalAmountDue:totalAmountDue});
				window.location.hash = '#Credit_card_info';
			}else if(invocationResult.Fault){
				busyInd.hide();
				navigator.notification.alert(invocationResult.Fault.Reason.Text);
			}else if(invocationResult.RBL){
				busyInd.hide();
				handleError(invocationResult.RBL.Response, invocationResult.RBL);
			}else{
        		navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
        		busyInd.hide();
			}			
		};
		
		regCCsubmit = function(){
			if(window.navigator.onLine){
				if($("#frmccreg").valid()){
					var otptest = 'otp';
					CCbdate = '';
					frmacntcc = $('#ccnumber').val();
					ccmonths = $('#cardExpMonth').val();
					ccyear = $('#cardExpYear').val();
					cccvv = $('#cvvnumber').val();
					CCbdate = $('#fldFromDate').val();
					CCRegSubmit(otptest);
				}
			}else{navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
			}
		};
		
		CCRegConfirm = function(result){
			invocationResult = result.invocationResult;
			if(invocationResult.Fault.Detail){
				busyInd.hide();
				$("#contentData").load("Views/Credit/CCReg-Confirm.html", null, function (response, status, xhr){
					if (status != "error") {}
					$('.cardnumber').html(frmacntcc);
					$('.validmontjh').html(ccmonths);  
					$('.validyear').html(ccyear);
					var date = CCbdate.split('/')[2]+"-"+CCbdate.split('/')[1]+"-"+CCbdate.split('/')[0];
					dateformatting(date);
					var birthDate = dateformat;
					$('.dob').html(birthDate);
					$('.creditcvv').html(cccvv);
					$('#OTP_Ref').val(invocationResult.Fault.Detail.otpRequiredFault.otpKey);
					temp = LoggedInUserID+""+Math.floor(Math.random()*9000) + 1000;
					$('.request').html(temp);  					
				ko.applyBindings(self, $(".content").get(0));
			});

			}else if(invocationResult.Fault){
				busyInd.hide();
				navigator.notification.alert(invocationResult.Fault.Reason.Text);
			}else if(invocationResult.RBL){
				busyInd.hide();
				handleError(invocationResult.RBL.Response, invocationResult.RBL);
			}else{
        		navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
        		busyInd.hide();
			}			
			
		};

		
		RedisterSuccess = function(result){
			invocationResult = result.invocationResult;
			if(invocationResult.getCardDetailResponse){
				busyInd.hide();
			}else if(invocationResult.Fault){
				busyInd.hide();
				navigator.notification.alert(invocationResult.Fault.Reason.Text);
			}else{
        		navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
        		busyInd.hide();
			}			
		};
		
		CCRegSubmit = function(otptest){
			if(otptest == 'otp'){
				if(window.navigator.onLine){
						busyInd.show();
						temp = LoggedInUserID+""+Math.floor(Math.random()*9000) + 1000;
						uniqueRequestNo = temp;
						var customerID = LoggedInUserID;
						primaryCardNo = frmacntcc;
						expiryMonth = ccmonths;
						expiryYear = parseFloat(ccyear);
						cardCVV = "400"; //$('.creditcvv').html();
						otpkey = "";
						ottpval = "";
						var CCbdates = CCbdate.split('/')[2]+"-"+CCbdate.split('/')[1]+"-"+CCbdate.split('/')[0]; //yyyy-mm-dd
						//alert(CCbdates);
						if(lgnout == false){ window.location.href = "#logout";}
						var invocationData = {
							adapter : "API_Adapter",
							procedure :"CCregister",
							parameters : [uniqueRequestNo,customerID,primaryCardNo,expiryMonth,expiryYear,cardCVV,otpkey,ottpval,CCbdates]
						};
				
						WL.Logger.debug(invocationData, '');
						WL.Client.invokeProcedure(invocationData, {
							onSuccess : CCRegConfirm,
							onFailure : AdapterFail,
							timeout: timeout
						});
				}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
				}
			}
			else{
				if(window.navigator.onLine){
					if($("#frmrd").valid()){
						busyInd.show();
						uniqueRequestNo = $('.request').html();
						var customerID = LoggedInUserID;
						primaryCardNo = $(".cardnumber").html();
						expiryMonth = $('.validmontjh').html();
						expiryYear = parseFloat($('.validyear').html());
						cardCVV = "400"; //$('.creditcvv').html();
						otpkey = $('#OTP_Ref').val();
						ottpval = $('#OTP_No_Val').val();
						var CCbdates = CCbdate.split('/')[2]+"-"+CCbdate.split('/')[1]+"-"+CCbdate.split('/')[0]; //yyyy-mm-dd
						if(lgnout == false){ window.location.href = "#logout";}
						var invocationData = {
							adapter : "API_Adapter",
							procedure :"CCregister",
							parameters : [uniqueRequestNo,customerID,primaryCardNo,expiryMonth,expiryYear,cardCVV,otpkey,ottpval,CCbdates]
						};
				
						WL.Logger.debug(invocationData, '');
						WL.Client.invokeProcedure(invocationData, {
							onSuccess : CCregisterSuccess,
							onFailure : AdapterFail,
							timeout: timeout
						});
					}
				}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
				}

				
			}
			
		};
		
		CCregisterSuccess = function(result){
			invocationResult = result.invocationResult;
			if(invocationResult.registerCardResponse){
				self.getCCaccounts();
			}else if(invocationResult.Fault){
				busyInd.hide();
				navigator.notification.alert(invocationResult.Fault.Reason.Text);
			}else if(invocationResult.RBL){
				busyInd.hide();
				handleError(invocationResult.RBL.Response, invocationResult.RBL);
			}else{
        		navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
        		busyInd.hide();
			}			
		};
		
		CCTrasnsubmit = function(){
			if(window.navigator.onLine){
				if($("#frmccemi").valid()){
					busyInd.show();
					var customerID = LoggedInUserID;
					cardnumber = $('#fldFromAcctNo').val();
					//loan = 0;
					if(window.location.hash == '#CCUnbilled'){loan = 0;}
					if(window.location.hash == '#CCconverEMI'){loan = 2;}
					if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
						adapter : "API_Adapter",
						procedure : "CCgetCardtransactions",
						parameters : [customerID,cardnumber,loan]
					};
			
					WL.Logger.debug(invocationData, '');
					WL.Client.invokeProcedure(invocationData, {
						onSuccess : CCgetTransSuccess,
						onFailure : AdapterFail,
						timeout: timeout
					});
				}
			}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
			}
		}; 
		
		CCgetTransSuccess = function(result){
			
			invocationResult = result.invocationResult;
			if(invocationResult.getTransactionsResponse.numTransactions > 0){
				if(invocationResult.getTransactionsResponse.transactionsArray.transaction){	
					CCcardtrans.removeAll();
					if(window.location.hash == '#CCUnbilled'){var transtypeval = $('#transactiontype').val();}
					else{var transtypeval = 'B';}
					itemdata = invocationResult.getTransactionsResponse.transactionsArray.transaction;
					$(itemdata).each(function(index, obj){
								index = index+'#'+obj.transactionID;
								amount = "₹ " +formatAmt(parseFloat(obj.amount));
								dateformatting(obj.recordDate);
								recordDate = dateformat;
								if(obj.EMIPlansArray){
									EMI=obj.EMIPlansArray.EMIPlan;
								}  
								else{ EMI='';}
								if(obj.EMISchedule){
									planname = obj.EMISchedule.EMIPlan.planName;
								}
								else{planname = '';}							
								TransType = obj.transactionType;
								if(TransType == 'D'){TransType = 'Dr'}
								if(TransType == 'C'){TransType = 'Cr'}
								amnt = obj.amount;
								var transactionbal = amnt;
											if(TransType == 'Dr'){
												totalDRbal =  parseFloat(totalDRbal) + parseFloat(transactionbal);
											}
											else{
												totalCRbal = parseFloat(totalCRbal) + parseFloat(transactionbal);
											}
								if(transtypeval == 'D'){
									if(obj.transactionType == 'D'){
										CCcardtrans.push({transactionID:obj.transactionID,amount:amount,hasEMIOption:obj.hasEMIOption,narrative:obj.narrative,EMI:EMI,recordDate:recordDate,index:index,planname:planname,TransType:TransType});
									}
								}
								else if(transtypeval == 'C'){
									if(obj.transactionType == 'C'){
										CCcardtrans.push({transactionID:obj.transactionID,amount:amount,hasEMIOption:obj.hasEMIOption,narrative:obj.narrative,EMI:EMI,recordDate:recordDate,index:index,planname:planname,TransType:TransType});
									}
								}
								else{
									CCcardtrans.push({transactionID:obj.transactionID,amount:amount,hasEMIOption:obj.hasEMIOption,narrative:obj.narrative,EMI:EMI,recordDate:recordDate,index:index,planname:planname,TransType:TransType});

								}
								
					});
					busyInd.hide();
					window.location.hash = '#Credit_card_Trans';
				}
			}
			else if(invocationResult.getTransactionsResponse.numTransactions == 0){
					busyInd.hide();
					if(window.location.hash == '#CCconverEMI'){
						navigator.notification.alert("There is no transaction available for EMI conversion");
					}
					else if(window.location.hash == '#CCUnbilled'){
						navigator.notification.alert("There is no unbilled transaction available for this card");
					}
					else{
						navigator.notification.alert("Sorry No transaction found.");

					}
			}
			else if(invocationResult.Fault){
				busyInd.hide();
				navigator.notification.alert(invocationResult.Fault.Reason.Text);
			}else if(invocationResult.RBL){
				busyInd.hide();
				handleError(invocationResult.RBL.Response, invocationResult.RBL);
			}else{
        		navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
        		busyInd.hide();
			}			
		};
		
		EMIplan = function(id){
			ID=id.split('#')[0];
			transID = id.split('#')[1];
			busyInd.show();
			GetEMIObj = CCcardtrans();
			EMIdetails = GetEMIObj[+ID];
			CCEMIdetails.removeAll();
			data = EMIdetails.EMI;
			$(data).each(function(index, obj){
				index = index+'#'+obj.planID;
				Months = obj.tenureInMonths+" Months";
				Interest = obj.rateOfInterest+" %";
				CCEMIdetails.push({Months:obj.tenureInMonths,tenureInMonths:Months,planName:obj.planName,Interest:obj.rateOfInterest,rateOfInterest:Interest,processingFee:obj.processingFee,planID:obj.planID,planCurrency:obj.planCurrency,index:index});
					
			});
			busyInd.hide();
			window.location.hash = '#Credit_card_EMIdet';
		};
		
		EmiPlanDetails = function(id){
			ID = id.split('#')[0];
			planid = id.split('#')[1];
			cardnumber = cardnumber;
			GetEMIObj = CCEMIdetails();
			EMIPlandetail = GetEMIObj[+ID];
			//CCEMIdetailPlan.removeAll();
			//data = EMIPlandetail;
			// $(data).each(function(index, obj){
				// Months = obj.tenureInMonths+" Months";
				// Interest = obj.rateOfInterest+" %";
				// CCEMIdetailPlan.push({Months:obj.tenureInMonths,tenureInMonths:Months,planName:obj.planName,Interest:obj.rateOfInterest,rateOfInterest:Interest,processingFee:obj.processingFee,planID:obj.planID,planCurrency:obj.planCurrency,index:index});
			// });
			if(window.navigator.onLine){
					busyInd.show();
					var customerID = LoggedInUserID;
					
					if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
						adapter : "API_Adapter",
						procedure : "CCcomputeEMI",
						parameters : [customerID,cardnumber,transID,planid]
					};
			
					WL.Logger.debug(invocationData, '');
					WL.Client.invokeProcedure(invocationData, {
						onSuccess : CCgetEMIComute,
						onFailure : AdapterFail,
						timeout: timeout
					});
			}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
			}
		};
		
		
		CCgetEMIComute = function(result){
			invocationResult = result.invocationResult;
			if(invocationResult.computeEMIResponse){
				busyInd.hide();
				$("#contentData").load("Views/Credit/CreditCard-EMIPlan.html", null, function (response, status, xhr){
					if (status != "error") {}
					$('.planName').html(invocationResult.computeEMIResponse.EMIPlan.planName);
					$('.tenureInMonths').html(invocationResult.computeEMIResponse.EMIPlan.tenureInMonths+" Months");
					$('.rateOfInterest').html(invocationResult.computeEMIResponse.EMIPlan.rateOfInterest+" %");
					$('.totalinterest').html("₹ "+formatAmt(parseFloat(invocationResult.computeEMIResponse.totalInterest)));
					$('.EMI').html("₹ "+formatAmt(parseFloat(invocationResult.computeEMIResponse.EMIValue)));
					$('.processingfee').html("₹ "+formatAmt(parseFloat(invocationResult.computeEMIResponse.processingFee)));
					dateformatting(invocationResult.computeEMIResponse.firstEMIOn);
					startDate = dateformat;
					$('.startdate').html(startDate);
					dateformatting(invocationResult.computeEMIResponse.lastEMIOn);
					endDate = dateformat;
					$('.enddate').html(endDate);
					ko.applyBindings(self, $(".content").get(0));                   
				});	

			}else if(invocationResult.Fault){
				busyInd.hide();
				navigator.notification.alert(invocationResult.Fault.Reason.Text);
			}else if(invocationResult.RBL){
				busyInd.hide();
				handleError(invocationResult.RBL.Response, invocationResult.RBL);
			}else{
        		navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
        		busyInd.hide();
			}			

		};
		
		CCconfirmPlan = function(){
			//alert(1);
			//GetOTP();
			var otptest = "otp";
			CCCnfrm(otptest);
		};
		
		CCconfirmPlan1 = function(result){
			invocationResult = result.invocationResult;
			if(invocationResult.Fault.Detail){
				$("#contentData").load("Views/Credit/CCEMI-Confirm.html", null, function (response, status, xhr){
					busyInd.hide();
					if (status != "error") {}
						$('#OTP_Ref').val(invocationResult.Fault.Detail.otpRequiredFault.otpKey);
						temp = LoggedInUserID+""+Math.floor(Math.random()*9000) + 1000;
						$('.request').html(temp);  					
					ko.applyBindings(self, $(".content").get(0));
				});
			}else if(invocationResult.Fault){
				busyInd.hide();
				navigator.notification.alert(invocationResult.Fault.Reason.Text);
			}else if(invocationResult.RBL){
				busyInd.hide();
				handleError(invocationResult.RBL.Response, invocationResult.RBL);
			}else{
        		navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
        		busyInd.hide();
			}			

		};
		
		CCCnfrm = function(otptest){
			if(otptest == "otp"){
				if(window.navigator.onLine){
						busyInd.show();
						
						uniqueRequestNo = LoggedInUserID+""+Math.floor(Math.random()*9000) + 1000;
						var customerID = LoggedInUserID;
						otpkey = "";
						ottpval = "";
						if(lgnout == false){ window.location.href = "#logout";}
						var invocationData = {
							adapter : "API_Adapter",
							procedure :"CCEMIConfirm",
							parameters : [uniqueRequestNo,customerID,cardnumber,transID,planid,otpkey,ottpval]
						};
				
						WL.Logger.debug(invocationData, '');
						WL.Client.invokeProcedure(invocationData, {
							onSuccess : CCconfirmPlan1,
							onFailure : AdapterFail,
							timeout: timeout
						});
				}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
				}

			}
			else{
				if(window.navigator.onLine){
					if($("#confrmEMI").valid()){
						busyInd.show();
						uniqueRequestNo = $('.request').html();
						var customerID = LoggedInUserID;
						otpkey = $('#OTP_Ref').val();
						ottpval = $('#OTP_No_Val').val();
						if(lgnout == false){ window.location.href = "#logout";}
						var invocationData = {
							adapter : "API_Adapter",
							procedure :"CCEMIConfirm",
							parameters : [uniqueRequestNo,customerID,cardnumber,transID,planid,otpkey,ottpval]
						};
				
						WL.Logger.debug(invocationData, '');
						WL.Client.invokeProcedure(invocationData, {
							onSuccess : CCEMISucess,
							onFailure : AdapterFail,
							timeout: timeout
						});
					}
				}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
				}
			}
		};
		
		CCEMISucess = function(result){
			invocationResult = result.invocationResult;
			if(invocationResult.applyForEMIResponse){
				if(invocationResult.applyForEMIResponse.EMISchedule.EMIPlan){
					
				$("#contentData").load("Views/Credit/CCEMI-succes.html", null, function (response, status, xhr){
					busyInd.hide();
					if (status != "error") {}
						busyInd.hide();
					$('.applino').html(invocationResult.applyForEMIResponse.EMISchedule.applicationReferenceNo);
					$('.term').html(invocationResult.applyForEMIResponse.EMISchedule.EMIPlan.tenureInMonths+" Months");
					$('.rateofinterest').html(invocationResult.applyForEMIResponse.EMISchedule.EMIPlan.rateOfInterest+"%");
					$('.totalinterest').html("₹ " +formatAmt(parseFloat(invocationResult.applyForEMIResponse.EMISchedule.totalInterest)));
					$('.EMI').html("₹ " +formatAmt(parseFloat(invocationResult.applyForEMIResponse.EMISchedule.EMIValue)));
					dateformatting(invocationResult.applyForEMIResponse.EMISchedule.firstEMIOn);
					startDate = dateformat;
					$('.startdate').html(startDate);
					dateformatting(invocationResult.applyForEMIResponse.EMISchedule.lastEMIOn);
					enddate = dateformat;
					$('.enddate').html(enddate);					
					ko.applyBindings(self, $(".content").get(0));
				});
					
					
				}
			}else if(invocationResult.Fault){
				busyInd.hide();
				navigator.notification.alert(invocationResult.Fault.Reason.Text);
			}else if(invocationResult.RBL){
				busyInd.hide();
				handleError(invocationResult.RBL.Response, invocationResult.RBL);
			}else{
        		navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
        		busyInd.hide();
			}			
		};
		
		CCLoanEnqSubmit = function(){
			if(window.navigator.onLine){
				if($("#frmccemi").valid()){
					busyInd.show();
					var customerID = LoggedInUserID;
					cardnumber = $('#fldFromAcctNo').val();
					loan = 1; 
					if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
						adapter : "API_Adapter",
						procedure : "CCgetCardtransactions",
						parameters : [customerID,cardnumber,loan]
					};
			
					WL.Logger.debug(invocationData, '');
					WL.Client.invokeProcedure(invocationData, {
						onSuccess : CCLoanEnqSuccess,
						onFailure : AdapterFail,
						timeout: timeout
					});
				}
			}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
			}
		};

		CCLoanEnqSuccess  = function(result){
			invocationResult = result.invocationResult;
			if(invocationResult.getTransactionsResponse.transactionsArray.transaction){
				CCcardtrans.removeAll();
				itemdata = invocationResult.getTransactionsResponse.transactionsArray.transaction;
				$(itemdata).each(function(index, obj){
							index = index+'#'+obj.transactionID;
							amount = "₹ " +formatAmt(parseFloat(obj.amount));
							dateformatting(obj.recordDate);
							recordDate = dateformat;
							//planname = obj.EMISchedule.EMIPlan.planName;
							status = obj.EMIApplicationStatus.decision;
					CCcardtrans.push({transactionID:obj.transactionID,amount:amount,narrative:obj.narrative,recordDate:recordDate,status:status});
							
				});
				busyInd.hide();
				window.location.hash = '#Credit_card_LoanTrans';
			}else if(invocationResult.getTransactionsResponse.transactionsArray){
				if(invocationResult.getTransactionsResponse.numTransactions == 0){
					busyInd.hide();
					navigator.notification.alert("Sorry No EMI request found.");
				}
			}else if(invocationResult.Fault){
				busyInd.hide();
				navigator.notification.alert(invocationResult.Fault.Reason.Text);
			}else if(invocationResult.RBL){
				busyInd.hide();
				handleError(invocationResult.RBL.Response, invocationResult.RBL);
			}else{
        		navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
        		busyInd.hide();
			}			
		};
		
		self.getccCardInfoForPay =  function(){
			if(window.navigator.onLine){
				if(accountListCC().length == 0){
					getCASAacnts();
				}
				else{
					busyInd.show();
					var customerID = LoggedInUserID;
					if(checkflag == 'false'){
						carnumberarray = ccAccountsPay();
						cardnumber = carnumberarray[0].codacctno;
					}
					else{cardnumber = ccnoval;}
						if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
							adapter : "API_Adapter",
							procedure : "CCgetCardDetail",
							parameters : [customerID,cardnumber]
						};
				
						WL.Logger.debug(invocationData, '');
						WL.Client.invokeProcedure(invocationData, {
							onSuccess : CCgetCardInfoSuccess,
							onFailure : AdapterFail,
							timeout: timeout
						});
				}	
			}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
			}
		};
		
		CCgetCardInfoSuccess = function(result){
			invocationResult = result.invocationResult;
			if(invocationResult.getCardDetailResponse){
				busyInd.hide();
				cardinfo = invocationResult.getCardDetailResponse
				minimumAmountDue = "₹ " +formatAmt(parseFloat(cardinfo.minimumAmountDue));
				totalAmountDue = "₹ " +formatAmt(parseFloat(cardinfo.totalAmountDue));
				if(checkflag == 'false'){						
					$("#contentData").load("Views/Credit/CC_CardPayment.html", null, function (response, status, xhr) {
						if (status != "error") {}
						firstcard = 'true';
						$('#fldDepositAmt').val(cardinfo.totalAmountDue);
						$('#1').attr('value',cardinfo.totalAmountDue);
						$('#1').html('Last Statement Bal '+totalAmountDue);
						$('#2').attr('value',cardinfo.minimumAmountDue);
						$('#2').html('Minimum Amount Due '+minimumAmountDue);
						ko.applyBindings(self, $(".content").get(0));                   
					});
					setTimeout(function(){ firstcard = 'false';	 },1000);
				}
				else{
					$('#fldDepositAmt').val(cardinfo.totalAmountDue);
					$('#1').attr('value',cardinfo.totalAmountDue);
					$('#1').html('Last Statement Bal '+totalAmountDue);
					$('#2').attr('value',cardinfo.minimumAmountDue);
					$('#2').html('Minimum Amount Due '+minimumAmountDue);
				}
			}else if(invocationResult.Fault){
				busyInd.hide();
				navigator.notification.alert(invocationResult.Fault.Reason.Text);
			}else if(invocationResult.RBL){
				busyInd.hide();
				handleError(invocationResult.RBL.Response, invocationResult.RBL);
			}else{
        		navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
        		busyInd.hide();
			}			
		};
		
		changeCCbal = function(){
			if(firstcard == 'false'){
				checkflag = 'true';
				ccnoval = $('#ccNo').val();
				self.getccCardInfoForPay();
			}
		};
		
		getCASAacnts = function(){
			if(window.navigator.onLine){
				busyInd.show();
				reqParams = {};
				reqParams["RQLoginUserId"] = LoggedInUserID;
				reqParams["RQDeviceFamily"] = Device_Platform;
				reqParams["RQDeviceFormat"] = Device_Model;
				reqParams["RQOperationId"] = "ACCSUMINQ";
				reqParams["RQClientAPIVer"] = RQClientAPIVer;
				reqParams["SessionId"] = CurrentSessionId;
				reqParams["RQTransSeq"] = "01";
				
				fldjsessionid = '';
				
				if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
						adapter : "API_Adapter",
						procedure : "GetAccounts",
						parameters : [fldjsessionid,reqParams],
						compressResponse : true
				};
				
				WL.Client.invokeProcedure(invocationData, {
					onSuccess : CASASuccess,
					onFailure : AdapterFail,	    		
					timeout: timeout
				});
			 
			}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
			}
        };
		
        CASASuccess = function(result){
        	invocationResult = result.invocationResult;
        	if(invocationResult.isSuccessful){
			if(invocationResult.RBL){
				if(invocationResult.RBL.Response){
					if(invocationResult.RBL.STATUS.CODE == '0'){
						var responsesessionid = invocationResult.RBL.SessionEnv.SessionId;
						var currentuser = currentreguserid;
						var responseuser = invocationResult.RBL.RequestParams.RQLoginUserId;
						if(currentuser == responseuser && CurrentSessionId == responsesessionid){
							custdtls = invocationResult.RBL.Response.CustDetails;
							itemdata = invocationResult.RBL.Response.acctdetails;
							nbrofsavingacc = invocationResult.RBL.Response.savingacctcount;
						    nbrofcurrentacc = invocationResult.RBL.Response.currentacctcount;
						    accountListCC.removeAll();
						    var idx = 1; 
						    $(itemdata).each(function(index, obj) {
								 strid = "item"+idx;
								 custnames = "";
								
								 $(custdtls).each(function(j, obj1) {
								
									 if(obj.acctindex == obj1.acctindex){
										
									 }
								 });
								 
								 displaytxt = $.trim(obj.acctno)+"-"+obj.branchname;
								
								 if(window.location.hash == '#rrasm01'){
									 acctbalance = "₹ " +formatAmt(parseFloat(obj.acctbalance));
								 }else{
									 acctbalance = "₹ " +formatAmt(parseFloat(obj.acctbalance));
								 }
								
								 accountListCC.push({ codacctno: obj.acctno, acctType: obj.acctType, acctbalance: acctbalance, acctbranch: obj.branchname, custnames: custnames, namccyshrt: obj.currency, displaytxt: displaytxt, strid:strid });
								 
								 idx++;
							});
							self.getccCardInfoForPay();
						}
						else{
							busyInd.hide();
							navigator.notification.alert("Your session has timed out!");
							window.location.hash = "#logout";
						}
					}
					else{
						busyInd.hide();
						invalidresponse();
					}
			    }else{
						busyInd.hide();
						handleError(invocationResult.RBL.Response, invocationResult.RBL);
				}
			}else{
        		 navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
        		busyInd.hide();
			}
			}
			 busyInd.hide();
        };

		CCPaySubmit = function(){
			if(window.navigator.onLine){
				if($("#frmccemi").valid()){
					frmacntcc = $('#fldFromAcctNo').val();
					ccmonths = $('#ccNo').val();
					ccyear = $('#fldDepositAmt').val();
					totalDue = $('#1').val();
					mindue = $('#2').val();
					var otptest = 'otp';
					CCPay(otptest);
					//GetOTP();
				}
			}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
			}
		};
		
		CCPayConfirm = function(result){
			invocationResult = result.invocationResult;
			if(invocationResult.Fault.Detail){
				busyInd.hide();
				$("#contentData").load("Views/Credit/CCPay-Confirm.html", null, function (response, status, xhr){
					if (status != "error") {}
						$('.acntnumber').html(frmacntcc);
						$('.Amount').html("₹ " +formatAmt(parseFloat(ccyear)));
						codacctno = ccmonths;
								temp = codacctno.length;
								temp1 = temp-4;
								cardno = "xxxxxxxxxxxx"+codacctno.substring(temp1,temp);
						$('.cardnumber').html(cardno);
						$('#OTP_Ref').val(invocationResult.Fault.Detail.otpRequiredFault.otpKey);
						temp = LoggedInUserID+""+Math.floor(Math.random()*9000) + 1000;
						$('.request').html(temp);  					
					ko.applyBindings(self, $(".content").get(0));
				});
			}else if(invocationResult.Fault){
				busyInd.hide();
				navigator.notification.alert(invocationResult.Fault.Reason.Text);
			}else if(invocationResult.RBL){
				busyInd.hide();
				handleError(invocationResult.RBL.Response, invocationResult.RBL);
			}else{
        		navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
        		busyInd.hide();
			}			
		};
		
		
		CCPay = function(otptest){
			if(otptest == "otp"){
				if(window.navigator.onLine){
						busyInd.show();
						temp = LoggedInUserID+""+Math.floor(Math.random()*9000) + 1000;
						uniqueRequestNo = temp;
						var customerID = LoggedInUserID;
						otpkey = "";
						ottpval = "";
						ccyear1 = parseFloat(ccyear);
						totalDue1 = parseFloat(totalDue);
						mindue1 = parseFloat(mindue);
						if(ccyear1 == mindue1){var check = '2';}
						else if(ccyear1 == totalDue1){var check = '1';}
						else{var check = '3';}
						if(lgnout == false){ window.location.href = "#logout";}
						var invocationData = {
							adapter : "API_Adapter",
							procedure :"CCPaytoCard",
							parameters :[uniqueRequestNo,customerID,ccmonths,ccyear1,frmacntcc,otpkey,ottpval,check]
						};
				
						WL.Logger.debug(invocationData, '');
						WL.Client.invokeProcedure(invocationData, {
							onSuccess : CCPayConfirm,
							onFailure : AdapterFail,
							timeout: timeout
						});
				}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
				}
			}
			else{
					if(window.navigator.onLine){
					if($("#CCPay").valid()){
						busyInd.show();
						temp = LoggedInUserID+""+Math.floor(Math.random()*9000) + 1000;
						uniqueRequestNo = $('.request').html();;
						var customerID = LoggedInUserID;
						otpkey = $('#OTP_Ref').val();
						ottpval = $('#OTP_No_Val').val();
						ccyear = parseFloat(ccyear);
						totalDue = parseFloat(totalDue);
						mindue = parseFloat(mindue);
						if(ccyear == mindue){var check = '2';}
						else if(ccyear == totalDue){var check = '1';}
						else{var check = '3';}
						if(lgnout == false){ window.location.href = "#logout";}
						var invocationData = {
							adapter : "API_Adapter",
							procedure :"CCPaytoCard",
							parameters :[uniqueRequestNo,customerID,ccmonths,ccyear,frmacntcc,otpkey,ottpval,check]
						};
				
						WL.Logger.debug(invocationData, '');
						WL.Client.invokeProcedure(invocationData, {
							onSuccess : CCPaySucess,
							onFailure : AdapterFail,
							timeout: timeout
						});
					}
				}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
				}

				
			}
			
		};
		
		CCPaySucess = function(result){
			invocationResult = result.invocationResult;
			if(invocationResult.payToCardResponse){
				busyInd.hide();
				$("#contentData").load("Views/Credit/CCPay-succes.html", null, function (response, status, xhr){
					if (status != "error") {}
					cardinfo = invocationResult.payToCardResponse;
					$('.Amount').html("₹ " +formatAmt(parseFloat(invocationResult.payToCardResponse.paidAmount)));
					$('.refno').html(invocationResult.payToCardResponse.uniqueResponseNo);
					$('.frmacnt').html(frmacntcc);
					codacctno = ccmonths;
							temp = codacctno.length;
							temp1 = temp-4;
							cardno = "xxxxxxxxxxxx"+codacctno.substring(temp1,temp);
					$('.cardno').html(cardno);
					custccno = ccAccountsPay();
					for (var key in custccno) {
								  var obj = custccno[key].codacctno;
									if(obj == ccmonths){var cardtype = custccno[key].productName; }
								}
					$('.cardtype').html(cardtype);
					ko.applyBindings(self, $(".content").get(0));
				});
			}else if(invocationResult.Fault){
				busyInd.hide();
				navigator.notification.alert(invocationResult.Fault.Reason.Text);
			}else if(invocationResult.RBL){
				busyInd.hide();
				handleError(invocationResult.RBL.Response, invocationResult.RBL);
			}else{
        		navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
        		busyInd.hide();
			}			
		};

};
    
 function getFormattedDate(date)
{
    return date.substring(8,10) + "-" + date.substring(5,7)+"-"+date.substring(0,4);
}

function getFormattedTime(date)
{
    return date.substring(8,16);
}	
     
function onNetworkCheck(){
	return;
} 

function checkButtonSelectionCC(iValue){
	if (iValue == 1){
		location.hash = '#rrasm01';
	}
	else{
		location.hash = '#CC_register';
	}
}      