package com.snapwork.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.GZIPInputStream;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.ProtocolVersion;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.params.ConnManagerPNames;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.conn.params.ConnPerRouteBean;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.AllowAllHostnameVerifier;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Proxy;
import android.net.Uri;
import android.util.Log;
//import android.util.Log;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONException;

@SuppressWarnings("unused")
public class ProxyUrlUtil {

	private String ua = "Mozilla/5.0 (Windows NT; Linux armv7l; rv:2.0.1) Gecko/20100101 Firefox/4.0.1 Snap/2.0.1";

	private String toString(BufferedReader reader) throws IOException {
		StringBuilder sb = new StringBuilder();
		String line;
		while ((line = reader.readLine()) != null) {
			sb.append(line).append('\n');
		}
		return sb.toString();
	}

	public String escape(String s) {
		try {
			return URLEncoder.encode(s, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return s;
		}

	}

	private String getProxyXMLP(URL url, Context c) throws IOException {
		// Log.d("InXMLN", "Start");
		BufferedReader reader = null;
		HttpGet httpRequest = null;
		String retStr = null;
		try {
			URI uri = new URI(url.getProtocol(), url.getHost(), url.getPath(),
					url.getQuery(), null);
			httpRequest = new HttpGet(uri);
			httpRequest.addHeader("Accept-Encoding", "gzip");
		} catch (URISyntaxException e) {
			e.printStackTrace();
			// ReportError.OnErr("ProxyUtil Url", e.getMessage(),
			// e.getClass().toString());
			return null;
		}

		try {

			ConnectivityManager connMgr = (ConnectivityManager) c
					.getSystemService(Context.CONNECTIVITY_SERVICE);

			HttpParams httpParameters = new BasicHttpParams();

			// Turn off stale checking. Our connections break all the time
			// anyway,
			// and it's not worth it to pay the penalty of checking every time.
			HttpConnectionParams.setStaleCheckingEnabled(httpParameters, false);

			// Set the timeout in milliseconds until a connection is
			// established.
			int timeoutConnection = 30 * 1000;
			int timeoutSocket = 30 * 1000;
			try {
				if (connMgr.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_MOBILE) {
					timeoutConnection = 40 * 1000;
					timeoutSocket = 40 * 000;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			HttpConnectionParams.setConnectionTimeout(httpParameters,
					timeoutConnection);
			// Set the default socket timeout (SO_TIMEOUT)
			// in milliseconds which is the timeout for waiting for data.

			HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);

			HttpConnectionParams.setSocketBufferSize(httpParameters, 4096);

			ConnManagerParams.setTimeout(httpParameters, 40 * 1000);

			DefaultHttpClient httpclient = new DefaultHttpClient(httpParameters);

			android.net.NetworkInfo mobile = connMgr
					.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

			try {
				if (connMgr.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_MOBILE) {
					if ((mobile.isAvailable())
							&& (Proxy.getDefaultHost() != null)) {
						// Log.d("InXMLN","inside set Proxy");
						if ((Proxy.getDefaultHost() != null)
								&& (Proxy.getDefaultPort() != -1)) {
							HttpHost proxy = new HttpHost(
									Proxy.getDefaultHost(),
									Proxy.getDefaultPort(), "http");
							httpclient.getParams().setParameter(
									ConnRoutePNames.DEFAULT_PROXY, proxy);
							// Log.d("InXMLN",Proxy.getDefaultHost()+":"+Integer.toString(Proxy.getDefaultPort()));
							// ReportError.OnErr("ProxyUtil SP",
							// Proxy.getDefaultHost()+":"+Integer.toString(Proxy.getDefaultPort()),
							// this.getClass().toString());
						}
					}
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			httpclient.getParams().setParameter(CoreProtocolPNames.USER_AGENT,
					ua);

			HttpResponse response = (HttpResponse) httpclient
					.execute(httpRequest);

			if (response.getStatusLine() == null) {
				// Log.d("InXMLN", "returning nulld");
				// Try with Proxy settings.
				return null;
			}

			int status = response.getStatusLine().getStatusCode();
			if (status == HttpStatus.SC_OK) {
				// Log.d("InXMLN", "Response ok");
				Header contentEncoding = response
						.getFirstHeader("Content-Encoding");
				if (contentEncoding != null
						&& contentEncoding.getValue().equalsIgnoreCase("gzip")) {
					// Log.d("InXMLN", "in GZip");

					InputStream instream = response.getEntity().getContent();
					instream = new GZIPInputStream(instream);
					reader = new BufferedReader(new InputStreamReader(instream));
					retStr = toString(reader);
					reader.close();
					instream.close();
					response.getEntity().consumeContent();

					// httpclient.getConnectionManager().releaseConnection(httpclient,
					// 100, 100);
				} else {
					// Log.d("InXMLN", "in Normal");
					HttpEntity entity = response.getEntity();
					BufferedHttpEntity bufHttpEntity = new BufferedHttpEntity(
							entity);
					// InputStream instream = bufHttpEntity.getContent();
					reader = new BufferedReader(new InputStreamReader(
							bufHttpEntity.getContent()));
					retStr = toString(reader);
				}
			} else {
				// Log.e("getXMLN", "Http response notok " +
				// Integer.toString(status)+":"+url.toString());
				// ReportError.OnErr("ProxyUtil MUrl", url.toString() +
				// ":Http response notok ", this.getClass().toString());
			}

			// retStr = instream.toString();
		} catch (MalformedURLException e) {
			e.printStackTrace();
			// ReportError.OnErr("ProxyUtil MUrl", url.toString() + ":" +
			// e.getMessage(), e.getClass().toString());
		} catch (Exception e) {
			e.printStackTrace();
			// ReportError.OnErr("ProxyUtil", e.getMessage(),
			// e.getClass().toString());
		}

		// Log.d("InXMLN", "End");

		// Try with Proxy Settings

		return retStr;

	}

	private String mapUrl(String code) {
		String returl = "";
		Cryptology c = new Cryptology();
		if (code.equalsIgnoreCase("CS")) {
			// returl =
			// c.decrypt("OrhDWPZlV02BUvwEUixzKLMzM5YyV12lT9MBLLxmVmwDYjxzUr3xZrxy0v3CUahAW9ZyV1gCH5wESL2C01djWyIuH5gz9qsm");
			//String eurl = c
					//.encrypt("http://mtrade.sharekhan.com/getQuote.php?companylist=$0&Rand=$1");
			// Log.d("PUU url",eurl);
			//returl = c.decrypt(eurl);
			// returl =
			// c.decrypt("OrhDWPZlVudmUeZnUedoUidnZ8Yul9YzLrxu19gDL5cCOb3pJ9wBWfMB5XwAZrxpKaJjsfMBK1djXa");

		}

		/*
		 * http://50.17.18.243/SK/chart_1day.php?companyCode=TCS
		 * http://50.17.18.243/SK/chart_1day.php?companyCode=13020033
		 * http://50.17.18.243/SK/niftyChartXML.php
		 * http://50.17.18.243/SK/sensexChartXML.php
		 */
		if (code.equalsIgnoreCase("IDC")) {
			// returl =
			// c.decrypt("OrhDWPZlV02BUvwEUixzKLMzM5YyV12lT9MBLL3lQnhCVmgAHjhDFfdzHLNlQnhC.m2BTbxyUL3qVrwz9qcmMGxBS9LzSf2z9qsm");
			//String eurl = c
					//.encrypt("http://mtrade.sharekhan.com/chart_1day.php?companyCode=$0");
			// Log.d("PUU IDC",eurl);
			// eurl=
			// "OrhDWPZlVudmUeZnUedoUidnZ8Yul9YyOfMC09vmKfwEUahAW9ZyV1gCH5wEd9gzL1djWa";
			//returl = c.decrypt(eurl);

		}

		if (code.equalsIgnoreCase("NIDC")) {
			// returl =
			// c.decrypt("OrhDWPZlVudmUeZnUedoUidnZ8Yul9YyOfMC09vmKfwEUahAW9ZyV1gCH5wEd9gzL1djWa");
			//String eurl = c
					//.encrypt("http://mtrade.sharekhan.com/chart_1day.php?companyCode=$0");
			// Log.d("PUU IDC",eurl);
			// eurl=
			// "OrhDWPZlVudmUeZnUedoUidnZ8Yul9YyOfMC09vmKfwEUahAW9ZyV1gCH5wEd9gzL1djXa";
			//returl = c.decrypt(eurl);
		}

		if (code.equalsIgnoreCase("BSEI")) {
			// returl =
			// c.decrypt("OrhDWPZlV02BUvwEUixzKLMzM5YyV12lV02BUvwEX8YyOfMC09LyZvMBLDNlWHgC");
			//String eurl = c
					//.encrypt("http://mtrade.sharekhan.com/sensexChartXML.php");
			// Log.d("PUU IDC",eurl);
			// eurl=
			// "OrhDWPZlVudmUeZnUedoUidnZ8Yul9YyOfMC09vmKfwEUahAW9ZyV1gCH5wEd9gzL1djXa";
			//returl = c.decrypt(eurl);

		}

		if (code.equalsIgnoreCase("NSEI")) {
			// returl =
			// c.decrypt("OrhDWPZlV02BUvwEUixzKLMzM5YyV12lT9MBLLxmVmgAHjhDF52CL5cCObh");
			//String eurl = c
					//.encrypt("http://mtrade.sharekhan.com/niftyChartXML.php");
			// Log.d("PUU IDC",eurl);
			// eurl=
			// "OrhDWPZlVudmUeZnUedoUidnZ8Yul9YyOfMC09vmKfwEUahAW9ZyV1gCH5wEd9gzL1djXa";
			//returl = c.decrypt(eurl);

		}

		if (code.equalsIgnoreCase("STATS")) {
			returl = c
					.decrypt("OrhDWPZlVywAUfMBJvMl0v2yOvgCVngAUm2BT9sBVixzW9MC0nwyYrMlWHgC.Kgz9qcm");

		}

		if (code.equalsIgnoreCase("AL")) {
			returl = c
					.decrypt("OrhDWPZlVywAUfMBJvMl0v2yOvgCVngAUm2BT9syUfgB5nhDVqcmVeMBHXwEZrNl41gB");

		}

		if (code.equalsIgnoreCase("ET")) {
			returl = c
					.decrypt("OrhDWPZlVywAUfMBJvMl0v2yOvgCVngAUm2BT9syUfgB5nhDVqcmVugEWvMC0rxAWnNl41gB");

		}

		if (code.equalsIgnoreCase("ANN")) {
			returl = c
					.decrypt("OrhDWPZlV02BUvwEUixzKLMzM5YyV12lT9MBLL3lQnhCVCwz0fKBU9wDUnwzTvMB0nNlQnhC.m2BTbxyUL3qVrwz9qcmMqwzZnMCPbhDVjxsK1tmWaJjJvNCYvMB0bvyNvMtV1djXyICVDhuLjhuHDwz9qIm");

		}

		if (code.equalsIgnoreCase("COM")) {
			returl = c
					.decrypt("OrhDWPZlVywAUfMBJvMl0v2yOvgCVngAUm2BT9cBPzxzVm2BT1wzUrxyYLNl41gB");

		}

		if (code.equalsIgnoreCase("NEWS")) {
			// returl =
			// c.decrypt("OrhDWPZlVywAUfMBJvMl0v2yOvgCVngAUm2BT9sBVCuz05uz3nNlWHgC.mhDHjhD9qcmM4wDT1djXa");

			//String eurl = c
					//.encrypt("http://mtrade.sharekhan.com/newsAPI.php?start=$0&num=$1");
			// Log.d("PUU News",eurl);
			// eurl=
			// "OrhDWPZlVudmUeZnUedoUidnZ8Yul9IBLD3CbbvsUahAW9ZC0fMC01djWyIB11wpKed";
			//returl = c.decrypt(eurl);
		}

		if (code.equalsIgnoreCase("BGL")) {
			returl = c
					.decrypt("OrhDWPZlVywAUfMBJvMl0v2yOvgCVngAUm2BT9cBPzxzVi2CL91zHLMBLj3CFX2BZvMCZ5cETXg");

		}

		if (code.equalsIgnoreCase("NGL")) {
			returl = c
					.decrypt("OrhDWPZlVywAUfMBJvMl0v2yOvgCVngAUm2BT9cBPzxzV42CL91zHLMBLj3CFX2BZvMCZ5cETXg");

		}

		if (code.equalsIgnoreCase("GBL")) {
			returl = c
					.decrypt("OrhDWPZlVywAUfMBJvMl0v2yOvgCVngAUm2BT9cBPzxzVCgBVjwyS9vAUrwAJfgDVj3xUv2DUGxBSb");

			/*
			 * String eurl=
			 * c.encrypt("http://50.17.18.243/SK/globalIndicators.php");
			 * Log.d("PUU Global",eurl); //eurl=
			 * "OrhDWPZlVudmUeZnUedoUidnZ8Yul9IBLD3CbbvsUahAW9ZC0fMC01djWyIB11wpKed"
			 * ; returl = c.decrypt(eurl);
			 */
		}

		if (code.equalsIgnoreCase("Week")) {
			returl = c
					.decrypt("OrhDWPZlV02BUvwEUixzKLMzM5YyV12lT9MBLL3lQnhCVmgAHjhDFfZDLv2AUO2CW9dETX2xMXwyN1dDYvxzMm2BTbxyUL3qVrwz9qcm");

		}

		if (code.equalsIgnoreCase("Month")) {
			returl = c
					.decrypt("OrhDWPZlV02BUvwEUixzKLMzM5YyV12lT9MBLL3lQnhCVmgAHjhDFftBV5gDO5IAZb3p41gBFzgBHDwp0jxDLzYyV1gCH5wEd9gzL1djWa");

		}

		if (code.equalsIgnoreCase("6Month")) {
			returl = c
					.decrypt("OrhDWPZlV02BUvwEUixzKLMzM5YyV12lT9MBLL3lQnhCVmgAHjhDFztBV5gDO5IAZb3p41gBFzgBHDwp0jxDLzYyV1gCH5wEd9gzL1djWa");

		}

		if (code.equalsIgnoreCase("12Month")) {
			returl = c
					.decrypt("OrhDWPZlV02BUvwEUixzKLMzM5YyV12lT9MBLL3lQnhCVmgAHjhDFfJmT9MB0HMlQnhC.GxBS9LzSf2z9qNC1vMjJ9wBWfMB5n0BKvwpKad");

		}

		return returl;
	}

	public String getData(Context c, String code, String... args) {
		String retStr = "";

		String url = mapUrl(code);

		/*
		 * Log.d("ProxyUtil",url); //String enc = Cryptology.encrypt(url);
		 * Log.d("ProxyUtil",enc); String dnc = Cryptology.decrypt(enc);
		 * Log.d("ProxyUtil",dnc);
		 */

		// Log.d("ProxyUtil",Integer.toString(args.length));
		// for(String a:args)
		for (int i = 0; i < args.length; i++) {
			if (args[i] != null)
				url = url.replace("$" + i, args[i]);
		}

		// Log.d("ProxyUtil",url);

		try {
			URL dataUrl = new URL(url);
			retStr = getProxyXML(dataUrl, c);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return retStr;
	}

	public String getProxyXML(URL url, Context c) throws IOException {
		// Log.d("InXMLN", "Start");
		BufferedReader reader = null;
		HttpGet httpRequest = null;
		String retStr = null;
		try {
			URI uri = new URI(url.getProtocol(), url.getHost(), url.getPath(),
					url.getQuery(), null);
			httpRequest = new HttpGet(uri);
			httpRequest.addHeader("Accept-Encoding", "gzip");
		} catch (URISyntaxException e) {
			// Log.e("URLUTIL", "Error in url " );
			e.printStackTrace();
			// ReportError.OnErr("ProxyUtil Url", e.getMessage(),
			// e.getClass().toString());
			return null;
		}

		try {

			ConnectivityManager connMgr = (ConnectivityManager) c
					.getSystemService(Context.CONNECTIVITY_SERVICE);

			HttpParams httpParameters = new BasicHttpParams();

			// Turn off stale checking. Our connections break all the time
			// anyway,
			// and it's not worth it to pay the penalty of checking every time.
			HttpConnectionParams.setStaleCheckingEnabled(httpParameters, false);

			// Set the timeout in milliseconds until a connection is
			// established.
			int timeoutConnection = 25 * 1000;
			int timeoutSocket = 25 * 1000;
			try {
				if (connMgr.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_MOBILE) {
					timeoutConnection = 40 * 1000;
					timeoutSocket = 40 * 000;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			HttpConnectionParams.setConnectionTimeout(httpParameters,
					timeoutConnection);
			// Set the default socket timeout (SO_TIMEOUT)
			// in milliseconds which is the timeout for waiting for data.

			HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);

			HttpConnectionParams.setSocketBufferSize(httpParameters, 4096);

			ConnManagerParams.setTimeout(httpParameters, 40 * 1000);

			DefaultHttpClient httpclient = new DefaultHttpClient(httpParameters);

			android.net.NetworkInfo mobile = connMgr
					.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

			try {
				if (connMgr.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_MOBILE) {
					if ((mobile.isAvailable())
							&& (Proxy.getDefaultHost() != null)) {
						// Log.d("InXMLN","inside set Proxy");
						if ((Proxy.getDefaultHost() != null)
								&& (Proxy.getDefaultPort() != -1)) {
							// HttpHost proxy = new
							// HttpHost(Proxy.getDefaultHost() ,
							// Proxy.getDefaultPort() , "http");
							// httpclient.getParams().setParameter
							// (ConnRoutePNames.DEFAULT_PROXY, proxy);
							// Log.d("InXMLN",Proxy.getDefaultHost()+":"+Integer.toString(Proxy.getDefaultPort()));
							// ReportError.OnErr("ProxyUtil SP",
							// Proxy.getDefaultHost()+":"+Integer.toString(Proxy.getDefaultPort()),
							// this.getClass().toString());
						}
					}
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			httpclient.getParams().setParameter(CoreProtocolPNames.USER_AGENT,
					ua);

			HttpResponse response = (HttpResponse) httpclient
					.execute(httpRequest);

			if (response.getStatusLine() == null) {
				// Log.d("InXMLN", "returning nulld");
				// Try with Proxy settings.
				return getProxyXMLP(url, c);
			}

			int status = response.getStatusLine().getStatusCode();
			if (status == HttpStatus.SC_OK) {
				// Log.d("InXMLN", "Response ok");
				Header contentEncoding = response
						.getFirstHeader("Content-Encoding");
				if (contentEncoding != null
						&& contentEncoding.getValue().equalsIgnoreCase("gzip")) {
					// Log.d("InXMLN", "in GZip");

					InputStream instream = response.getEntity().getContent();
					instream = new GZIPInputStream(instream);
					reader = new BufferedReader(new InputStreamReader(instream));
					retStr = toString(reader);
					reader.close();
					instream.close();
					response.getEntity().consumeContent();

					// httpclient.getConnectionManager().releaseConnection(httpclient,
					// 100, 100);
				} else {
					// Log.d("InXMLN", "in Normal");
					HttpEntity entity = response.getEntity();
					BufferedHttpEntity bufHttpEntity = new BufferedHttpEntity(
							entity);
					// InputStream instream = bufHttpEntity.getContent();
					reader = new BufferedReader(new InputStreamReader(
							bufHttpEntity.getContent()));
					retStr = toString(reader);
				}
			} else {
				// Log.e("getXMLN", "Http response notok " +
				// Integer.toString(status)+":"+url.toString());
				// ReportError.OnErr("ProxyUtil MUrl", Integer.toString(status)
				// + ":Http notok "+ url.toString(),
				// this.getClass().toString());
			}

			// retStr = instream.toString();
		} catch (MalformedURLException e) {
			// Log.w("URLUTIL", "Wrong url: ");
			// ReportError.OnErr("ProxyUtil MUrl", url.toString() + ":" +
			// e.getMessage(), e.getClass().toString());
		} catch (Exception e) {
			e.printStackTrace();
			// ReportError.OnErr("ProxyUtil", e.getMessage(),
			// e.getClass().toString());
		}

		// Log.d("InXMLN", "End");

		// Try with Proxy Settings
		if ((retStr == null) || (retStr.length() < 5))
			return getProxyXMLP(url, c);

		return retStr;

	}

	// ----------------------------------------------------------

	public String getPostXMLP(URL url, Context c, List<NameValuePair> args)
			throws IOException {
		// Log.d("InXMLN", "Start");
		BufferedReader reader = null;
		HttpGet httpRequest = null;
		String retStr = null;
		try {
			URI uri = new URI(url.getProtocol(), url.getHost(), url.getPath(),
					url.getQuery(), null);
			httpRequest = new HttpGet(uri);
			httpRequest.addHeader("Accept-Encoding", "gzip");
		} catch (URISyntaxException e) {
			e.printStackTrace();
			// ReportError.OnErr("ProxyUtil Url", e.getMessage(),
			// e.getClass().toString());
			return null;
		}

		try {

			ConnectivityManager connMgr = (ConnectivityManager) c
					.getSystemService(Context.CONNECTIVITY_SERVICE);

			HttpParams httpParameters = new BasicHttpParams();

			// Turn off stale checking. Our connections break all the time
			// anyway,
			// and it's not worth it to pay the penalty of checking every time.
			HttpConnectionParams.setStaleCheckingEnabled(httpParameters, false);

			// Set the timeout in milliseconds until a connection is
			// established.
			int timeoutConnection = 30 * 1000;
			int timeoutSocket = 30 * 1000;
			try {
				if (connMgr.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_MOBILE) {
					timeoutConnection = 40 * 1000;
					timeoutSocket = 40 * 000;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			HttpConnectionParams.setConnectionTimeout(httpParameters,
					timeoutConnection);
			// Set the default socket timeout (SO_TIMEOUT)
			// in milliseconds which is the timeout for waiting for data.

			HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);

			HttpConnectionParams.setSocketBufferSize(httpParameters, 4096);

			ConnManagerParams.setTimeout(httpParameters, 40 * 1000);

			DefaultHttpClient httpclient = new DefaultHttpClient(httpParameters);

			android.net.NetworkInfo mobile = connMgr
					.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

			try {
				if (connMgr.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_MOBILE) {
					if ((mobile.isAvailable())
							&& (Proxy.getDefaultHost() != null)) {
						// Log.d("InXMLN","inside set Proxy");
						if ((Proxy.getDefaultHost() != null)
								&& (Proxy.getDefaultPort() != -1)) {
							HttpHost proxy = new HttpHost(
									Proxy.getDefaultHost(),
									Proxy.getDefaultPort(), "http");
							httpclient.getParams().setParameter(
									ConnRoutePNames.DEFAULT_PROXY, proxy);
							// Log.d("InXMLN",Proxy.getDefaultHost()+":"+Integer.toString(Proxy.getDefaultPort()));
							// ReportError.OnErr("ProxyUtil SP",
							// Proxy.getDefaultHost()+":"+Integer.toString(Proxy.getDefaultPort()),
							// this.getClass().toString());
						}
					}
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			httpclient.getParams().setParameter(CoreProtocolPNames.USER_AGENT,
					ua);

			HttpResponse response = (HttpResponse) httpclient
					.execute(httpRequest);

			if (response.getStatusLine() == null) {
				// Log.d("InXMLN", "returning nulld");
				// Try with Proxy settings.
				return null;
			}

			int status = response.getStatusLine().getStatusCode();
			if (status == HttpStatus.SC_OK) {
				// Log.d("InXMLN", "Response ok");
				Header contentEncoding = response
						.getFirstHeader("Content-Encoding");
				if (contentEncoding != null
						&& contentEncoding.getValue().equalsIgnoreCase("gzip")) {
					// Log.d("InXMLN", "in GZip");

					InputStream instream = response.getEntity().getContent();
					instream = new GZIPInputStream(instream);
					reader = new BufferedReader(new InputStreamReader(instream));
					retStr = toString(reader);
					reader.close();
					instream.close();
					response.getEntity().consumeContent();

					// httpclient.getConnectionManager().releaseConnection(httpclient,
					// 100, 100);
				} else {
					// Log.d("InXMLN", "in Normal");
					HttpEntity entity = response.getEntity();
					BufferedHttpEntity bufHttpEntity = new BufferedHttpEntity(
							entity);
					// InputStream instream = bufHttpEntity.getContent();
					reader = new BufferedReader(new InputStreamReader(
							bufHttpEntity.getContent()));
					retStr = toString(reader);
				}
			} else {
				// Log.e("getXMLN", "Http response notok " +
				// Integer.toString(status)+":"+url.toString());
				// ReportError.OnErr("ProxyUtil MUrl", url.toString() +
				// ":Http response notok ", this.getClass().toString());
			}

			// retStr = instream.toString();
		} catch (MalformedURLException e) {
			return null;
			// Log.e("URLUTIL", "Wrong url: ");
			// ReportError.OnErr("ProxyUtil MUrl", url.toString() + ":" +
			// e.getMessage(), e.getClass().toString());
		} catch (SocketException ex) {
			return null;
			// Log.i("timeout exception has benn catched \n","") ;
			// break;//b reak the while
		} catch (SocketTimeoutException ex) {
			return null;
			// Log.i("timeout exception has benn catched \n","") ;
			// break;//break the while
		} catch (Exception e) {
			e.printStackTrace();
			return null;
			// e.printStackTrace();
			// ReportError.OnErr("ProxyUtil", e.getMessage(),
			// e.getClass().toString());
		}

		// Log.d("InXMLN", "End");

		// Try with Proxy Settings

		return retStr;

	}

	final static HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
		public boolean verify(String hostname, SSLSession session) {
			return true;
		}
	};

	/**
	 * Trust every server - dont check for any certificate
	 */
	private static void trustAllHosts() {
		// Create a trust manager that does not validate certificate chains
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return new java.security.cert.X509Certificate[] {};
			}

			public void checkClientTrusted(X509Certificate[] chain,
					String authType) throws CertificateException {
			}

			public void checkServerTrusted(X509Certificate[] chain,
					String authType) throws CertificateException {
			}
		} };

		// Install the all-trusting trust manager
		try {
			SSLContext sc = SSLContext.getInstance("TLS");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection
					.setDefaultSSLSocketFactory(sc.getSocketFactory());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getPostXML(URL url, Context c, List<NameValuePair> args)
			throws IOException {
		// Log.d("InXMLN", "Start");
		BufferedReader reader = null;
		HttpPost httpRequest = null;
		String retStr = null;
		try {
			URI uri = new URI(url.getProtocol(), url.getHost(), url.getPath(),
					url.getQuery(), null);
			httpRequest = new HttpPost(uri);
			httpRequest.addHeader("Accept-Encoding", "gzip");
			httpRequest.setEntity(new UrlEncodedFormEntity(args));
		} catch (URISyntaxException e) {
			// Log.e("URLUTIL", "Error in url " );
			e.printStackTrace();
			// ReportError.OnErr("ProxyUtil Url", e.getMessage(),
			// e.getClass().toString());
			return null;
		}

		try {

			HostnameVerifier hostnameVerifier = org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;

			// ConnectivityManager connMgr = (ConnectivityManager)
			// c.getSystemService(Context.CONNECTIVITY_SERVICE);

			SchemeRegistry schemeRegistry = new SchemeRegistry();
			schemeRegistry.register(new Scheme("http", PlainSocketFactory
					.getSocketFactory(), 80));
			schemeRegistry.register(new Scheme("https",
					new EasySSLSocketFactory(), 443));

			/*
			 * SSLSocketFactory socketFactory =
			 * SSLSocketFactory.getSocketFactory();
			 * socketFactory.setHostnameVerifier((X509HostnameVerifier)
			 * hostnameVerifier); schemeRegistry.register(new Scheme("https",
			 * socketFactory, 443));
			 */

			// SchemeRegistry schReg = new SchemeRegistry();
			// schemeRegistry.register(new Scheme("http",
			// PlainSocketFactory.getSocketFactory(), 80));
			// schemeRegistry.register(new
			// Scheme("https",SSLSocketFactory.getSocketFactory(), 443));

			HttpParams params = new BasicHttpParams();
			params.setParameter(ConnManagerPNames.MAX_TOTAL_CONNECTIONS, 30);
			params.setParameter(ConnManagerPNames.MAX_CONNECTIONS_PER_ROUTE,
					new ConnPerRouteBean(30));
			params.setParameter(HttpProtocolParams.USE_EXPECT_CONTINUE, false);
			HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);

			ClientConnectionManager connMgr = new SingleClientConnManager(
					params, schemeRegistry);
			// this.httpClient = new DefaultHttpClient(cm, params);

			HttpParams httpParameters = new BasicHttpParams();

			// Turn off stale checking. Our connections break all the time
			// anyway,
			// and it's not worth it to pay the penalty of checking every time.
			HttpConnectionParams.setStaleCheckingEnabled(httpParameters, false);

			// Set the timeout in milliseconds until a connection is
			// established.
			int timeoutConnection = 25 * 1000;
			int timeoutSocket = 25 * 1000;
			/*
			 * try { if (connMgr.getActiveNetworkInfo().getType() ==
			 * ConnectivityManager.TYPE_MOBILE) { timeoutConnection = 40*1000;
			 * timeoutSocket = 40*000; } } catch (Exception e) {
			 * e.printStackTrace(); }
			 */
			HttpConnectionParams.setConnectionTimeout(httpParameters,
					timeoutConnection);
			// Set the default socket timeout (SO_TIMEOUT)
			// in milliseconds which is the timeout for waiting for data.

			HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);

			HttpConnectionParams.setSocketBufferSize(httpParameters, 4096);

			ConnManagerParams.setTimeout(httpParameters, 40 * 1000);

			// DefaultHttpClient httpclient = new
			// DefaultHttpClient(httpParameters);
			DefaultHttpClient httpclient = new DefaultHttpClient(
					new ThreadSafeClientConnManager(httpParameters,
							schemeRegistry), httpParameters);

			// HttpClient httpClient = new DefaultHttpClient();
			// SSLSocketFactory sf =
			// (SSLSocketFactory)httpClient.getConnectionManager()
			// .getSchemeRegistry().getScheme("https").getSocketFactory();
			// SSLSocketFactory sf = new SSLSocketFactory(sslContext,
			// SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

			/*
			 * android.net.NetworkInfo mobile =
			 * connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
			 * 
			 * try { if (connMgr.getActiveNetworkInfo().getType() ==
			 * ConnectivityManager.TYPE_MOBILE) { if((mobile.isAvailable())
			 * &&(Proxy.getDefaultHost()!=null)) {
			 * //Log.d("InXMLN","inside set Proxy");
			 * if((Proxy.getDefaultHost()!=null) &&(Proxy.getDefaultPort()!=-1))
			 * { //HttpHost proxy = new HttpHost(Proxy.getDefaultHost() ,
			 * Proxy.getDefaultPort() , "http");
			 * //httpclient.getParams().setParameter
			 * (ConnRoutePNames.DEFAULT_PROXY, proxy);
			 * //Log.d("InXMLN",Proxy.getDefaultHost
			 * ()+":"+Integer.toString(Proxy.getDefaultPort()));
			 * //ReportError.OnErr("ProxyUtil SP",
			 * Proxy.getDefaultHost()+":"+Integer
			 * .toString(Proxy.getDefaultPort()), this.getClass().toString()); }
			 * } } } catch (Exception e) { // TODO Auto-generated catch block
			 * e.printStackTrace(); }
			 */
			httpclient.getParams().setParameter(CoreProtocolPNames.USER_AGENT,
					"Tabs_Android");

			HttpResponse response = (HttpResponse) httpclient
					.execute(httpRequest);

			if (response.getStatusLine() == null) {
				// Log.d("InXMLN", "returning nulld");
				// Try with Proxy settings.
				return getPostXMLP(url, c, args);
			}

			int status = response.getStatusLine().getStatusCode();
			if (status == HttpStatus.SC_OK) {
				// Log.d("InXMLN", "Response ok");
				Header contentEncoding = response
						.getFirstHeader("Content-Encoding");
				if (contentEncoding != null
						&& contentEncoding.getValue().equalsIgnoreCase("gzip")) {
					// Log.d("InXMLN", "in GZip");

					InputStream instream = response.getEntity().getContent();
					instream = new GZIPInputStream(instream);
					reader = new BufferedReader(new InputStreamReader(instream));
					retStr = toString(reader);
					reader.close();
					instream.close();
					response.getEntity().consumeContent();

					// httpclient.getConnectionManager().releaseConnection(httpclient,
					// 100, 100);
				} else {
					// Log.d("InXMLN", "in Normal");
					HttpEntity entity = response.getEntity();
					BufferedHttpEntity bufHttpEntity = new BufferedHttpEntity(
							entity);
					// InputStream instream = bufHttpEntity.getContent();
					reader = new BufferedReader(new InputStreamReader(
							bufHttpEntity.getContent()));
					retStr = toString(reader);
				}
			} else {
				// Log.e("getXMLN", "Http response notok " +
				// Integer.toString(status)+":"+url.toString());
				// ReportError.OnErr("ProxyUtil MUrl", Integer.toString(status)
				// + ":Http notok "+ url.toString(),
				// this.getClass().toString());
			}

			// retStr = instream.toString();
		} catch (MalformedURLException e) {
			// Log.w("URLUTIL", "Wrong url: ");
			// ReportError.OnErr("ProxyUtil MUrl", url.toString() + ":" +
			// e.getMessage(), e.getClass().toString());
		} catch (Exception e) {
			e.printStackTrace();
			// ReportError.OnErr("ProxyUtil", e.getMessage(),
			// e.getClass().toString());
		}

		// Log.d("InXMLN", "End");

		// Try with Proxy Settings
		if ((retStr == null) || (retStr.length() < 5))
			return getPostXMLP(url, c, args);

		return retStr;

	}

	/*
	 * 30122013 Monday By RUpesh... Above method will causing 'socket
	 * connnection tyime out exception' That`s why i created below
	 */
	/**
	 * Get Offers data from server.
	 * 
	 * @param url
	 *            URL to get data from.
	 * @param nvp
	 *            List<NameValuePair>.
	 * @return
	 * @throws IOException
	 * @throws ClientProtocolException
	 */
	public static String getOffers(final String url,
			final List<NameValuePair> nvp) throws ClientProtocolException,
			IOException {
		// server response havering all offers list
		String sResponse = null;
		// Log.v("getServantsListUID", "uid:"+uid+", URL:"+url);
		JSONArray jArrayResponse = null;
		// Creating HTTP client

		// Creating HTTP Post
		HttpPost httpPost = new HttpPost(url);

		SchemeRegistry schemeRegistry = new SchemeRegistry();
		schemeRegistry.register(new Scheme("http", PlainSocketFactory
				.getSocketFactory(), 80));
		schemeRegistry.register(new Scheme("https", new EasySSLSocketFactory(),
				443));

		HttpParams httpParameters = new BasicHttpParams();

		// Turn off stale checking. Our connections break all the time anyway,
		// and it's not worth it to pay the penalty of checking every time.
		HttpConnectionParams.setStaleCheckingEnabled(httpParameters, false);

		HttpConnectionParams.setConnectionTimeout(httpParameters, 20000);
		// Set the default socket timeout (SO_TIMEOUT)
		// in milliseconds which is the timeout for waiting for data.

		HttpConnectionParams.setSoTimeout(httpParameters, 20000);

		HttpConnectionParams.setSocketBufferSize(httpParameters, 4096);

		ConnManagerParams.setTimeout(httpParameters, 40 * 1000);

		// DefaultHttpClient httpclient = new DefaultHttpClient(httpParameters);
		DefaultHttpClient httpclient = new DefaultHttpClient(
				new ThreadSafeClientConnManager(httpParameters, schemeRegistry),
				httpParameters);

		// Building post parameters, key and value pair
		// List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>(2);
		// nameValuePair.add(new BasicNameValuePair("uid", uid));
		// nameValuePair.add(new BasicNameValuePair("password",
		// "encrypted_password"));

		// Url Encoding the POST parameters
		try {
			httpPost.setEntity(new UrlEncodedFormEntity(nvp));
			httpPost.addHeader("Accept-Encoding", "gzip");
		} catch (UnsupportedEncodingException e) {
			// writing error to Log
			e.printStackTrace();
		}

		// Making HTTP Request
		// try {
		HttpResponse response = httpclient.execute(httpPost);

		Log.d("response  codeeeeeeeeeee", " "
				+ response.getStatusLine().getStatusCode());

		int status = response.getStatusLine().getStatusCode();
		if (status == HttpStatus.SC_OK) {
			Log.d("InXMLN", "Response ok");
			Header contentEncoding = response
					.getFirstHeader("Content-Encoding");
			if (contentEncoding != null
					&& contentEncoding.getValue().equalsIgnoreCase("gzip")) {
				Log.d("InXMLN", "in GZip");

				InputStream instream = response.getEntity().getContent();
				instream = new GZIPInputStream(instream);
				BufferedReader br = new BufferedReader(new InputStreamReader(
						instream));

				StringBuilder sb = new StringBuilder();
				String line;
				while ((line = br.readLine()) != null) {
					sb.append(line).append('\n');
				}

				sResponse = sb.toString();
				br.close();
				instream.close();
				response.getEntity().consumeContent();

				// httpclient.getConnectionManager().releaseConnection(httpclient,
				// 100, 100);
			} else {
				Log.d("InXMLN", "in Normal");
				HttpEntity entity = response.getEntity();
				BufferedHttpEntity bufHttpEntity = new BufferedHttpEntity(
						entity);
				// InputStream instream = bufHttpEntity.getContent();
				BufferedReader br = new BufferedReader(new InputStreamReader(
						bufHttpEntity.getContent()));
				StringBuilder sb = new StringBuilder();
				String line;
				while ((line = br.readLine()) != null) {
					sb.append(line).append('\n');
				}

				sResponse = sb.toString();
			}
		}

		// writing response to log
		Log.d("Http Response:", sResponse.toString());
		// jArrayResponse=new JSONArray(sResponse);//....

		// } catch (ClientProtocolException e) {
		// // writing exception to log
		// e.printStackTrace();
		// } catch (IOException e) {
		// // writing exception to log
		// e.printStackTrace();
		// }
		return sResponse;

	}

	/**
	 * Get needed Data from the server.
	 * 
	 * @param url
	 *            URL to get data from.
	 * @param nvp
	 *            List<NameValuePair>.
	 * @return Response String
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @author Rupesh
	 */
	// May be gc...
	public static String getData(final String url, final List<NameValuePair> nvp)
			throws ClientProtocolException, IOException {
		// server response havering all offers list
		String sResponse = null;
		JSONArray jArrayResponse = null;
		// Creating HTTP client
		HttpClient httpClient = new DefaultHttpClient();
		HttpConnectionParams
				.setConnectionTimeout(httpClient.getParams(), 20000);

		// Creating HTTP Post
		HttpPost httpPost = new HttpPost(url);

		// this will through 'UnsupportedEncodingException/IOException'!
		httpPost.setEntity(new UrlEncodedFormEntity(nvp));

		// Making HTTP Request
		// this will through 'ClientProtocolException'!
		HttpResponse response = httpClient.execute(httpPost);

		BufferedReader br = new BufferedReader(new InputStreamReader(response
				.getEntity().getContent(), "UTF-8"));

		sResponse = br.readLine();
		// writing response to log
		// Log.d("Http Response:", sResponse.toString());

		return sResponse;// jArrayResponse;

	}

	// Rupesh 21.12.13 Ssaturday...
	public final String jOffersCategoryDataArray(String url, Context con) {
		String offer = "";
		HttpClient client = new DefaultHttpClient();
		HttpConnectionParams.setConnectionTimeout(client.getParams(), 20000);
		HttpResponse response;

		/** JSONObject that contain parameters to be send to php */
		// JSONObject json = new JSONObject();
		try {
			HttpPost post = new HttpPost(url);

			// json.put("radius", redius);
			// json.put("lat", lat);
			// json.put("lang", lng);
			// json.put("myid", myUId);
			//
			// post.setHeader("json", json.toString());
			// StringEntity se = new StringEntity(json.toString());
			// se.setContentEncoding(new
			// BasicHeader(HTTP.CONTENT_TYPE,"application/json"));

			// post.setEntity(se);

			/** executing my post request */
			response = client.execute(post);

			/** Checking response */
			// Get the data in the entity
			InputStream in = response.getEntity().getContent();

			// convert it to string...
			offer = convertStreamToString(in).trim();
			// fd.trim();
			// Log.i("Read from Server(FansData)", ":" + offer + ":");

		} catch (Exception e) {
			// TODO: handle exception
			return offer; //
		}
		return offer;
	}

	/**
	 * converting response into human readable string
	 * 
	 * @param is
	 *            : response data to convert into String(e.g.,human readable
	 *            format)
	 * @return String : human readable String
	 */
	protected static String convertStreamToString(InputStream is) {

		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}

	// ---------------------------------------------------------

	public Bitmap getImage(URL url, Context c) throws IOException {

		HttpGet httpRequest = null;
		Bitmap bmp = null;
		// Log.d("IngetImage", "Start");
		// Log.d("IngetImage", url.toString());
		InputStream instream = null;
		try {
			URI uri = new URI(url.getProtocol(), url.getHost(), url.getPath(),
					url.getQuery(), null);
			httpRequest = new HttpGet(uri);
			httpRequest.addHeader("Accept-Encoding", "gzip");
		} catch (URISyntaxException e) {
			// ReportError.OnErr("ProxyUtil Img", e.getMessage(),
			// e.getClass().toString());
			e.printStackTrace();
		}

		try {

			HttpParams httpParameters = new BasicHttpParams();
			// Set the timeout in milliseconds until a connection is
			// established.
			int timeoutConnection = 20 * 1000;
			// HttpConnectionParams.setConnectionTimeout(httpParameters,
			// timeoutConnection);
			// Set the default socket timeout (SO_TIMEOUT)
			// in milliseconds which is the timeout for waiting for data.
			int timeoutSocket = 10 * 1000;
			HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
			HttpProtocolParams.setUseExpectContinue(httpParameters, true);
			DefaultHttpClient httpclient = new DefaultHttpClient(httpParameters);
			// HttpProtocolParams.setUseExpectContinue(httpclient.getParams(),
			// true);

			// HttpClient httpclient = new DefaultHttpClient();

			ConnectivityManager connMgr = (ConnectivityManager) c
					.getSystemService(Context.CONNECTIVITY_SERVICE);

			// android.net.NetworkInfo wifi =
			// connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

			android.net.NetworkInfo mobile = connMgr
					.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

			if (connMgr.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_MOBILE) {
				if ((mobile.isAvailable()) && (Proxy.getDefaultHost() != null)) {
					// Log.d("IngetImage","inside set Proxy");
					HttpHost proxy = new HttpHost(Proxy.getDefaultHost(),
							Proxy.getDefaultPort(), "http");
					httpclient.getParams().setParameter(
							ConnRoutePNames.DEFAULT_PROXY, proxy);
				}
			}

			HttpResponse response = (HttpResponse) httpclient
					.execute(httpRequest);
			int status = response.getStatusLine().getStatusCode();
			if (status == HttpStatus.SC_OK) {
				// Log.d("IngetImage", "Response ok");
				Header contentEncoding = response
						.getFirstHeader("Content-Encoding");
				if (contentEncoding != null
						&& contentEncoding.getValue().equalsIgnoreCase("gzip")) {
					// Log.d("IngetImage", "in GZip");

					instream = response.getEntity().getContent();
					instream = new GZIPInputStream(instream);
					// reader = new BufferedReader(new
					// InputStreamReader(instream));

				} else {
					// Log.d("IngetImage", "in Normal");
					HttpEntity entity = response.getEntity();
					BufferedHttpEntity bufHttpEntity = new BufferedHttpEntity(
							entity);
					instream = bufHttpEntity.getContent();
					// reader = new BufferedReader(new
					// InputStreamReader(bufHttpEntity.getContent()));

				}

				bmp = BitmapFactory.decodeStream(instream);
			} else {
				// Log.e("IngetImage", url.toString()+"Http response notok " +
				// Integer.toString(status));
			}

			// retStr = instream.toString();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// ReportError.OnErr("ProxyUtil Img", e.getMessage(),
			// e.getClass().toString());
			e.printStackTrace();
			return getImageRegular(url, c);
		}

		return bmp;
	}

	private Bitmap getImageRegular(URL url, Context c) throws IOException {

		HttpGet httpRequest = null;
		Bitmap bmp = null;
		// Log.d("IngetImage", "Start");
		InputStream instream = null;
		try {
			URI uri = new URI(url.getProtocol(), url.getHost(), url.getPath(),
					url.getQuery(), null);
			httpRequest = new HttpGet(uri);
			// httpRequest.addHeader("Accept-Encoding", "gzip");
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}

		try {

			HttpParams httpParameters = new BasicHttpParams();
			// Set the timeout in milliseconds until a connection is
			// established.
			int timeoutConnection = 3000;
			HttpConnectionParams.setConnectionTimeout(httpParameters,
					timeoutConnection);
			// Set the default socket timeout (SO_TIMEOUT)
			// in milliseconds which is the timeout for waiting for data.
			int timeoutSocket = 3000;
			HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
			// HttpProtocolParams.setUseExpectContinue(httpParameters, true);
			DefaultHttpClient httpclient = new DefaultHttpClient(httpParameters);
			// HttpProtocolParams.setUseExpectContinue(httpclient.getParams(),
			// true);

			ConnectivityManager connMgr = (ConnectivityManager) c
					.getSystemService(Context.CONNECTIVITY_SERVICE);
			android.net.NetworkInfo mobile = connMgr
					.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

			if (connMgr.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_MOBILE) {
				if ((mobile.isAvailable()) && (Proxy.getDefaultHost() != null)) {
					// Log.d("IngetImage","inside set Proxy");
					HttpHost proxy = new HttpHost(Proxy.getDefaultHost(),
							Proxy.getDefaultPort(), "http");
					httpclient.getParams().setParameter(
							ConnRoutePNames.DEFAULT_PROXY, proxy);
				}
			}

			HttpResponse response = (HttpResponse) httpclient
					.execute(httpRequest);
			int status = response.getStatusLine().getStatusCode();
			if (status == HttpStatus.SC_OK) {
				// Log.d("IngetImage", "Response ok");
				Header contentEncoding = response
						.getFirstHeader("Content-Encoding");
				if (contentEncoding != null
						&& contentEncoding.getValue().equalsIgnoreCase("gzip")) {
					// Log.d("IngetImage", "in GZip");

					instream = response.getEntity().getContent();
					instream = new GZIPInputStream(instream);
					// reader = new BufferedReader(new
					// InputStreamReader(instream));

				} else {
					// Log.d("IngetImage", "in Normal");
					HttpEntity entity = response.getEntity();
					BufferedHttpEntity bufHttpEntity = new BufferedHttpEntity(
							entity);
					instream = bufHttpEntity.getContent();
					// reader = new BufferedReader(new
					// InputStreamReader(bufHttpEntity.getContent()));

				}

				bmp = BitmapFactory.decodeStream(instream);
			}
			// else
			// Log.e("IngetImage", "Http response notok " +
			// Integer.toString(status));

			// retStr = instream.toString();
		} catch (MalformedURLException e) {
			e.printStackTrace();
			// ReportError.OnErr("ProxyUtil", e.getMessage(),
			// e.getClass().toString());
		} catch (Exception e) {
			// ReportError.OnErr("ProxyUtil", e.getMessage(),
			// e.getClass().toString());
			e.printStackTrace();
		}

		return bmp;
	}

	private class Cryptology {

		private char mapping[] = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i',
				'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u',
				'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G',
				'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S',
				'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '0', '1', '2', '3', '4',
				'5', '6', '7', '8', '9', '-', '.' };

		private char getc(int presentCount, String encryptString) {
			char c = 0;
			if (presentCount < encryptString.length()) {
				c = encryptString.charAt(presentCount);
			}
			return c;
		}

		private void putc(int i, StringBuffer sbr) {
			sbr.append((char) i);
		}

		public String encrypt(String s) {

			String encryptString = "";
			StringBuffer sbr = new StringBuffer();

			encryptString = s;
			int bitoffset = 0;
			int firstchar = 0;
			int secondchar = 0;
			int presentCount = 0;

			sbr.delete(0, sbr.length());
			firstchar = getc(presentCount, encryptString);
			if (firstchar > 0) {
				presentCount++;
				secondchar = getc(presentCount, encryptString);
			}

			while (firstchar > 0) {
				while (bitoffset < 8) {
					int i = ((firstchar + secondchar * 256) >> bitoffset) & 63;
					putc(mapping[i], sbr);
					bitoffset += 6;
				}
				bitoffset -= 8;
				firstchar = secondchar;
				presentCount++;
				secondchar = getc(presentCount, encryptString);
			}

			String t = sbr.toString();
			sbr = null;

			return t;
		}

		public String decrypt(String s1) {

			String encryptString = "";
			StringBuffer sbr = new StringBuffer();

			int bitoffset = 0;
			int outchar = 0;
			int c = 0;
			int mapping1[] = new int[256];
			sbr.delete(0, sbr.length());
			int charCount = 0;
			for (int j = 0; j < mapping.length; j++) {
				mapping1[mapping[j]] = j;
			}

			encryptString = s1;
			c = getc(charCount, encryptString);

			int t2 = 0;
			while (c > 0) {
				outchar |= mapping1[c] << bitoffset;
				bitoffset += 6;
				if (bitoffset >= 8) {
					t2 = outchar & 127;
					putc(t2, sbr);
					bitoffset -= 8;
					outchar >>= 8;
				}
				charCount++;
				c = getc(charCount, encryptString);
			}

			String t = sbr.toString();
			sbr = null;

			return t;
		}

	} // end Cryptology

	/**
	 * Check the availability of Internet.
	 * 
	 * @param activity
	 * @return True if Internet service is available False otherwise.
	 */
	public static boolean isNetworkAvailable(Activity activity) {
		ConnectivityManager connectivity = (ConnectivityManager) activity
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity == null) {
			return false;
		} else {
			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if (info != null) {
				for (int i = 0; i < info.length; i++) {
					if (info[i].getState() == NetworkInfo.State.CONNECTED) {
						return true;
					}
				}
			}
		}
		return false;
	}

}
