package com.snapwork.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

@SuppressWarnings("unused")
public class Utils {

	public static boolean isNetworkAvailableConnected(final Context ctx) {
		boolean ret = true;
		try {
			ConnectivityManager conMgr = (ConnectivityManager) ctx
					.getSystemService(Context.CONNECTIVITY_SERVICE);
			if (conMgr != null) {
				NetworkInfo i = conMgr.getActiveNetworkInfo();
				if (i != null) {
					if (!i.isConnected())
						ret = false;
					if (!i.isAvailable())
						ret = false;
				} else
					ret = false;

			} else
				ret = false;
			if (!ret) {
				final Runnable showToastMessage = new Runnable() {
					public void run() {
						Toast.makeText(ctx, "No Network Connection!",
								Toast.LENGTH_LONG).show();
					}
				};
			}
		} catch (Exception e) {
			ret = false;
		}
		return ret;
	}

}
// end Cryptology

