package com.snapwork.messages;

public class ContactDetails {
	private String district;
	private String state;
	private String city_town_village;
	private String locality;
	private String pincode;
	private String name;
	private String sname;

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCity_town_village() {
		return city_town_village;
	}

	public void setCity_town_village(String city_town_village) {
		this.city_town_village = city_town_village;
	}

	public String getLocality() {
		return locality;
	}

	public void setLocality(String locality) {
		this.locality = locality;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSname() {
		return sname;
	}

	public void setSname(String sname) {
		this.sname = sname;
	}

}
