package com.snapwork.constants;

public class AppConstants {

	// public static final String SENDER_ID = "194125411826"; // Project Number:
	// public static String DeviceID = "";// user device uid
	// /**
	// * Preference file name where to save '<b>register id</b>' received after
	// * successfully registration of user with Server for GCM push.
	// */
	// public static String REGISTER_PREFRENCE = "register_prefrence";
	//
	// /**
	// * Preference name of Register id(with Our pushing server) tag name.
	// Reg.Id
	// * from received from our server after(sending GCM-ID and all other
	// params)
	// * successful sending GCM id.
	// */
	// public static String USER_REGISTER_ID = "user_register_id";
	// /**
	// * Preference name of User`s device GCM registration ID received from
	// Google
	// * GCM.
	// */
	// public static String DEVICE_GCM_REGISTERED_ID =
	// "device_gcm_registered_id";

	/**
	 * Map zoom level.
	 */
	public static final int INTZOOM_LEVEL = 12;

}
