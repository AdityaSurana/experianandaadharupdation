package com.snapwork.constants;

public class ConstantURLs {

	/**
	 * RBL URL to get ATMs, Branches and Lockers data.
	 */
	public static final String RBL_ABL_URL = "https://180.179.117.36/rbl/geolocator";

	/**
	 * Map zoom level.
	 */
	public static final int INTZOOM_LEVEL = 12;

	/**
	 * URL to track user location.
	 */

}
