package com.snapwork.parser;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.snapwork.messages.Place;

public class PlaceDetailsParser extends DefaultHandler {

	Boolean celement = false;
	String cvalue = null;

	public static Place placelist;
	private List<Place> placeDetail;

	/*
	 * public static Place getPlaceList() { return placelist; }
	 * 
	 * public static void setPlace(Place places) {
	 * PlaceDetailsParser.placelist=places; }
	 */

	public PlaceDetailsParser(List<Place> placeDetail) {
		super();
		this.placeDetail = placeDetail;
		// placeDetail=new ArrayList<Place>();
	}

	public List<Place> getPlaces() {
		return placeDetail;
	}

	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		celement = true;

		try {

			if (localName.equals("pois")) {
				placelist = new Place();
			} else if (localName.equals("poi")) {
				/** Get attribute value */
				placelist = new Place();
				placeDetail.add(placelist);

				String poi_id = attributes.getValue("poi_id");
				placelist.setPoi_id(poi_id);

				String landmark = attributes.getValue("landmark");
				placelist.setLandmark(landmark);

				String address = attributes.getValue("address");
				placelist.setAddress(address);

				String city = attributes.getValue("city");
				placelist.setCity(city);

				String locality = attributes.getValue("locality");
				placelist.setLocality(locality);

				String state = attributes.getValue("state");
				placelist.setState(state);

				String phone = attributes.getValue("phone");
				placelist.setPhone(phone);

				String fax = attributes.getValue("fax");
				placelist.setFax(fax);

				String atm_info = attributes.getValue("atm_info");
				placelist.setAtm_info(atm_info);

				String locker_info = attributes.getValue("locker_info");
				placelist.setLocker_info(locker_info);

				String weekday_info = attributes.getValue("weekday_info");
				placelist.setWeekday_info(weekday_info);

				String weekend_info = attributes.getValue("weekend_info");
				placelist.setWeekend_info(weekend_info);

				String weeklyoff_info = attributes.getValue("weeklyoff_info");
				placelist.setWeeklyoff_info(weeklyoff_info);

				String latitude = attributes.getValue("latitude");
				placelist.setLatitude(latitude);

				String longitude = attributes.getValue("longitude");
				placelist.setLongitude(longitude);

				String type = attributes.getValue("type");
				placelist.settype(type);

				String flggold = attributes.getValue("flggold");
				placelist.setFlggold(flggold);

				String flgsilver = attributes.getValue("flgsilver");
				placelist.setFlgsilver(flgsilver);

				String ifscode = attributes.getValue("ifscode");
				placelist.setIfscode(ifscode);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void endElement(String uri, String localName, String qName)
			throws SAXException {

		celement = false;

		if (localName.equalsIgnoreCase("poi")) {
			placelist.setPoi(cvalue);
			// Log.i("data",cvalue);
			// places.add(placelist);

		}
	}

	public void characters(char[] ch, int start, int length)
			throws SAXException {

		if (celement) {
			cvalue = new String(ch, start, length);
			celement = false;
		}
	}
}
