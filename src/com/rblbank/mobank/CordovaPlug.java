package com.rblbank.mobank;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;

import android.content.Intent;
import android.net.Uri;

public class CordovaPlug extends CordovaPlugin {

	@Override
	public boolean execute(String action, String rawArgs,
			CallbackContext callbackContext) throws JSONException {
		if (action.equalsIgnoreCase("call")) {

			JSONArray array = new JSONArray(rawArgs);

			RBL_iBank.getInstance().startActivity(
					new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel",
							array.getString(0), null)));
		}

		if (action.equalsIgnoreCase("mail")) {
			Intent emailIntent = new Intent(Intent.ACTION_SENDTO,
					Uri.fromParts("mailto", "customercare@rblbank.com", null));
			emailIntent.putExtra(Intent.EXTRA_SUBJECT, "");
			emailIntent.putExtra(Intent.EXTRA_TEXT, "");
			RBL_iBank.getInstance().startActivity(
					Intent.createChooser(emailIntent, "Send email..."));
		}
		return true;
	}
}
