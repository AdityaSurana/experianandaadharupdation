package com.rblbank.mobank;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ZoomControls;

import com.rblbank.mobank.R;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;
import com.snapwork.ethreads.UserTask;
import com.snapwork.messages.ContactDetails;
import com.snapwork.messages.ReplyItem;
import com.snapwork.parser.ContactsParser;
import com.snapwork.util.ProxyUrlUtil;

public class OffersDetailActivity extends MapActivity implements
		OnClickListener, ReplyItem {

	ImageView back, getOffersDirection, showOfferDetail, showOfferOnMap,
			imgBtnMenu, imgCategoryIcon;
	LinearLayout offerDetailLayout;
	ScrollView scrollOfferDetailView;
	/**
	 * Offer`s Detail MAP-ICON
	 */
	Drawable offers_map_icon;// =
								// getResources().getDrawable(R.drawable.offers_icon);
	/**
	 * Offer`s Detail ICON.
	 */
	Drawable offers_list_icon;// =
								// getResources().getDrawable(R.drawable.offers_icon);
	/**
	 * Offer`s Detail Backgroung Image;
	 */
	Drawable offers_detail_bg;
	MapView detailMapView;
	private List<Overlay> mapOverlays;
	GeoPoint gp1, gp2;

	Bundle offerDetailBundle;
	String name, add, offer, category, validTill, o_latitude, o_longitude,
			c_latitude, c_longitude, distance, time, fromX, fromY, toX, toY,
			Locality, stateName, curCity, CurDest;
	/**
	 * Indicate whether to show direction from the user`s current location or
	 * from the City Selected from Adv.Search. <br>
	 * ---NOTE---<br>
	 * When Location Services disabled ... in this case we do not have user`s
	 * location and hence we use selected CITY location as to get the
	 * directions.
	 */
	boolean from_current_location;
	String toString, fromString, viaString;

	private GetDirectionData mGetDirectionData;
	private List<ContactDetails> curLocationDetail;
	ReplyItem itm = this;

	private ProgressDialog mPdialog = null;

	TextView offerName, offerOffers, OfferValidity, OfferAddress;

	private List<HashMap<String, String>> DirectionData = new ArrayList<HashMap<String, String>>();

	private Resources res;
	private String noNetwork = null;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// TODO Auto-generated method stub
		setContentView(R.layout.offer_details);

		res = getResources();
		noNetwork = res.getString(R.string.no_network);

		offerDetailLayout = (LinearLayout) findViewById(R.id.offerDetaillayout);
		scrollOfferDetailView = (ScrollView) findViewById(R.id.scrollOfferDetail);

		// Menu Button
		imgBtnMenu = (ImageView) findViewById(R.id.menu);
		imgBtnMenu.setOnClickListener(this);

		// Category ICON
		imgCategoryIcon = (ImageView) findViewById(R.id.image_icon_detail_cate);

		// Detail map view.
		detailMapView = (MapView) findViewById(R.id.mapview);
		detailMapView.setBuiltInZoomControls(true);
		FrameLayout.LayoutParams zoomParams = new FrameLayout.LayoutParams(
				LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		ZoomControls controls = (ZoomControls) detailMapView
				.getZoomButtonsController().getZoomControls();
		// controls.setGravity(Gravity.RIGHT);
		zoomParams.setMargins(0, 0, 0, 25);
		controls.setLayoutParams(zoomParams);

		offerName = (TextView) findViewById(R.id.text_offer_name);
		offerOffers = (TextView) findViewById(R.id.text_offer_offers);
		OfferValidity = (TextView) findViewById(R.id.text_offer_validity);
		OfferAddress = (TextView) findViewById(R.id.text_offer_address);

		showOfferDetail = (ImageView) findViewById(R.id.img_btn_show_offer_details);
		showOfferOnMap = (ImageView) findViewById(R.id.img_btn_show_offer_on_map);
		showOfferDetail.setOnClickListener(this);
		showOfferOnMap.setOnClickListener(this);

		getOffersDirection = (ImageView) findViewById(R.id.get_direction_to_offer);
		getOffersDirection.setOnClickListener(this);

		// get Bundle from Intent.
		offerDetailBundle = getIntent().getExtras();
		name = offerDetailBundle.get("name").toString();
		offer = offerDetailBundle.get("offers").toString();
		category = offerDetailBundle.get("category").toString();
		validTill = offerDetailBundle.get("validity").toString();
		add = offerDetailBundle.get("address").toString();
		o_latitude = offerDetailBundle.get("o_latitude").toString();
		o_longitude = offerDetailBundle.get("o_longitude").toString();
		c_latitude = offerDetailBundle.get("c_latitude").toString();
		c_longitude = offerDetailBundle.get("c_longitude").toString();
		from_current_location = offerDetailBundle
				.getBoolean("from_current_location");

		toString = o_longitude + "," + o_latitude;// 77.2197, 28.6327
		fromString = c_longitude + "," + c_latitude;// 73.014053, 19.120517
		viaString = toString;

		// final String toString=o_longitude+","+o_latitude;
		// String fromString=c_longitude+","+c_latitude;
		// String viaString=toString;
		// mGetDirectionData = (GetDirectionData) new
		// GetDirectionData().execute(fromString,toString,viaString);

		// Log.v("EEE", "EEEnter in ASYN");
		// showDirectionOnMap();
		// Log.v("OnNNN", "in ON onCreate->");

		setIconOnMapAndList();
		imgCategoryIcon.setImageDrawable(offers_list_icon);
		scrollOfferDetailView.setBackgroundDrawable(offers_detail_bg);
		offerName.setText(name);
		offerOffers.setText(offer);
		OfferValidity.setText("Valid till: " + validTill);
		OfferAddress.setText(add);

		// On Back Button Press
		back = (ImageView) findViewById(R.id.backbutton);
		back.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		Intent intent;
		switch (v.getId()) {

		case R.id.backbutton:
			OffersDetailActivity.this.finish();
			// intent=new Intent(OffersDetailActivity.this,
			// ShowOffersActivity.class);
			// startActivity(intent);
			break;
		case R.id.get_direction_to_offer:
			// Toast.makeText(OffersDetailActivity.this,
			// "get_direction_to_offer", 0).show();
			// OffersDetailActivity.this.finish();
			// Intent in=new Intent(OffersDetailActivity.this,
			// ShowOffersActivity.class);
			// startActivity(in);
			if (ProxyUrlUtil.isNetworkAvailable(OffersDetailActivity.this)) {
				// do...
				showDirectionOnMap();
			} else {
				Toast.makeText(OffersDetailActivity.this, noNetwork, 1).show();

			}

			break;
		case R.id.img_btn_show_offer_details:
			// Toast.makeText(OffersDetailActivity.this,
			// "get_direction_to_offer", 0).show();
			showOfferOnMap.setBackgroundDrawable(getResources().getDrawable(
					R.drawable.show_mapview_btn_normal));
			showOfferDetail.setBackgroundDrawable(getResources().getDrawable(
					R.drawable.show_listview_btn_selected));
			if (detailMapView.getVisibility() == View.VISIBLE) {
				detailMapView.setVisibility(View.GONE);
				offerDetailLayout.setVisibility(View.VISIBLE);

			}
			break;
		case R.id.img_btn_show_offer_on_map:
			// Toast.makeText(OffersDetailActivity.this,
			// "get_direction_to_offer", 0).show();
			enableMap();
			// showOfferDetail.setBackgroundDrawable(getResources().getDrawable(
			// R.drawable.show_listview_btn_normal));
			// showOfferOnMap.setBackgroundDrawable(getResources().getDrawable(
			// R.drawable.show_mapview_btn_selected));
			// if (offerDetailLayout.getVisibility() == View.VISIBLE) {
			// offerDetailLayout.setVisibility(View.GONE);
			// detailMapView.setVisibility(View.VISIBLE);
			//
			// }

			// mGetDirectionData = (GetDirectionData) new
			// GetDirectionData().execute(fromString,toString,viaString);

			// Show Offer`s DIRECTION on map from my current Location_
			// showDirectionOnMap();

			// .............
			// GeoPoint offerLocGeoPoint = new
			// GeoPoint((int)(Double.valueOf(o_latitude)*1E6),(int)(Double.valueOf(o_longitude)*1E6));
			//
			// if (detailMapView.getOverlays().size()==0) {
			// // GeoPoint offerLocGeoPoint = new
			// GeoPoint((int)(Double.valueOf(o_latitude)*1E6),(int)(Double.valueOf(o_longitude)*1E6));
			// mapOverlays=detailMapView.getOverlays();
			// CustomItemizedOverlay itemizedOverlay =
			// new CustomItemizedOverlay(offers_map_icon,
			// OffersDetailActivity.this,itm);
			// OverlayItem overlayitem =
			// new OverlayItem(offerLocGeoPoint, name,add);
			// itemizedOverlay.addOverlay(overlayitem);
			// mapOverlays.add(itemizedOverlay);
			// MapController mapController = detailMapView.getController();
			// mapController.animateTo(offerLocGeoPoint);
			// mapController.setZoom(15);
			// } else {
			// //Do nothing...
			// MapController mapController = detailMapView.getController();
			// mapController.animateTo(offerLocGeoPoint);
			// mapController.setZoom(15);
			// }
			// ..........

			detailMapView.invalidate();
			// Navigate to location of current Offer only!
			GeoPoint offerLocGeoPoint = new GeoPoint(
					(int) (Double.valueOf(o_latitude) * 1E6),
					(int) (Double.valueOf(o_longitude) * 1E6));
			mapOverlays = detailMapView.getOverlays();
			mapOverlays.clear();

			// TO set Image of Concern Item According to 'Category' it belongs
			// to... gc...
			/*
			 * Drawable offers =
			 * getResources().getDrawable(R.drawable.offers_icon); if
			 * (category.equalsIgnoreCase("Apparels")) { offers =
			 * getResources().getDrawable(R.drawable.apparels_map_icon); } else
			 * if (category.equalsIgnoreCase("Dining")) { offers =
			 * getResources().getDrawable(R.drawable.rest_map_icon); } else if
			 * (category.equalsIgnoreCase("Electronics")) { offers =
			 * getResources().getDrawable(R.drawable.electronics_map_icon); }
			 * else if (category.equalsIgnoreCase("Fuel")) { offers =
			 * getResources().getDrawable(R.drawable.fuel_map_icon); } else if
			 * (category.equalsIgnoreCase("Grocery")) { offers =
			 * getResources().getDrawable(R.drawable.grocery_map_icon); } else
			 * if (category.equalsIgnoreCase("Personal Care")) { offers =
			 * getResources
			 * ().getDrawable(R.drawable.personal_care_listing_icon); } else if
			 * (category.equalsIgnoreCase("Telecom")) { offers =
			 * getResources().getDrawable(R.drawable.telecom_map_icon); }
			 * //2...else if (category.equalsIgnoreCase("Hotel")) { //No icon
			 * for HOTEL... offers =
			 * getResources().getDrawable(R.drawable.rest_map_icon); } else if
			 * (category.equalsIgnoreCase("Offer")) { offers =
			 * getResources().getDrawable(R.drawable.offer); } else if
			 * (category.equalsIgnoreCase("offers")) { //No Icon for 'offers'
			 * offers = getResources().getDrawable(R.drawable.offer); } else if
			 * (category.equalsIgnoreCase("Restaurant")) { offers =
			 * getResources().getDrawable(R.drawable.rest_map_icon); } else if
			 * (category.equalsIgnoreCase("Telecommunication")) { offers =
			 * getResources().getDrawable(R.drawable.telecom_map_icon); } else
			 * if (category.equalsIgnoreCase("Tyre")) { //No Icon for 'Tyre'
			 * offers = getResources().getDrawable(R.drawable.offer); } else {
			 * //Set Default Image... If NO category... }
			 */

			CustomItemizedOverlay itemizedOverlay = new CustomItemizedOverlay(
					offers_map_icon, OffersDetailActivity.this, itm);
			OverlayItem overlayitem = new OverlayItem(offerLocGeoPoint, name,
					add);
			itemizedOverlay.addOverlay(overlayitem);
			mapOverlays.add(itemizedOverlay);
			MapController mapController = detailMapView.getController();
			mapController.animateTo(offerLocGeoPoint);
			mapController.setZoom(15);

			break;
		case R.id.menu:
			// intent = new Intent(OffersDetailActivity.this,
			// OffersActivityNew.class);
			intent = new Intent(OffersDetailActivity.this, RBL_iBank.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			// intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
			// Bundle bundle = new Bundle();
			// bundle.putString("requestid", "smdashboardlist");
			// intent.putExtras(bundle);
			finish();
			startActivity(intent);
			break;
		default:
			break;
		}
	}

	/**
	 * Set Image for Map and Detail of Concern clicked Item According to
	 * 'Category' it belongs to...
	 */
	private void setIconOnMapAndList() {
		// TODO Auto-generated method stub
		// TO set Image of Concern Item According to 'Category' it belongs to...
		if (category.equalsIgnoreCase("Apparels")
				|| category.equalsIgnoreCase("Shopping")) {
			offers_map_icon = getResources().getDrawable(
					R.drawable.apparels_map_icon);
			offers_list_icon = getResources().getDrawable(
					R.drawable.apparels_listing_icon);
			offers_detail_bg = getResources().getDrawable(
					R.drawable.bg_shopping);
		} else if (category.equalsIgnoreCase("Dining")) {
			offers_map_icon = getResources().getDrawable(
					R.drawable.rest_map_icon);
			offers_list_icon = getResources().getDrawable(
					R.drawable.resturant_listing_icon);
			offers_detail_bg = getResources().getDrawable(
					R.drawable.bg_restaurant);
		} else if (category.equalsIgnoreCase("Electronics")) {
			offers_map_icon = getResources().getDrawable(
					R.drawable.electronics_map_icon);
			offers_list_icon = getResources().getDrawable(
					R.drawable.electronics_listing_icon);
			offers_detail_bg = getResources().getDrawable(
					R.drawable.bg_electronics);
		} else if (category.equalsIgnoreCase("Fuel")) {
			offers_map_icon = getResources().getDrawable(
					R.drawable.fuel_map_icon);
			offers_list_icon = getResources().getDrawable(
					R.drawable.fuel_listing_icon);
			offers_detail_bg = getResources().getDrawable(R.drawable.bg_fuel);
		} else if (category.equalsIgnoreCase("Grocery")
				|| category.equalsIgnoreCase("Retail")) {
			offers_map_icon = getResources().getDrawable(
					R.drawable.grocery_map_icon);
			offers_list_icon = getResources().getDrawable(
					R.drawable.grocery_listing_icon);
			offers_detail_bg = getResources().getDrawable(R.drawable.bg_retail);
		} else if (category.equalsIgnoreCase("Personal Care")) {
			offers_map_icon = getResources().getDrawable(
					R.drawable.personal_care_map_icon);
			offers_list_icon = getResources().getDrawable(
					R.drawable.personal_care_listing_icon);
			offers_detail_bg = getResources().getDrawable(
					R.drawable.bg_personal_care);
		} else if (category.equalsIgnoreCase("Telecom")) {
			offers_map_icon = getResources().getDrawable(
					R.drawable.telecom_map_icon);
			offers_list_icon = getResources().getDrawable(
					R.drawable.telecom_listing_icon);
			offers_detail_bg = getResources()
					.getDrawable(R.drawable.bg_telecom);
		} else if (category.equalsIgnoreCase("Hotel")) {
			// No icon for HOTEL...
			offers_map_icon = getResources().getDrawable(
					R.drawable.hotel_map_icon);
			offers_list_icon = getResources().getDrawable(
					R.drawable.hotel_listing_icon);
			offers_detail_bg = getResources().getDrawable(R.drawable.bg_hotel);
		} else if (category.equalsIgnoreCase("Offer")) {
			offers_map_icon = getResources()
					.getDrawable(R.drawable.offers_icon);
			offers_list_icon = getResources().getDrawable(R.drawable.offer);
			offers_detail_bg = getResources().getDrawable(
					R.drawable.bg_adventure);
		} else if (category.equalsIgnoreCase("offers")) {
			// No Icon for 'offers'
			offers_map_icon = getResources()
					.getDrawable(R.drawable.offers_icon);
			offers_list_icon = getResources().getDrawable(R.drawable.offer);
			offers_detail_bg = getResources().getDrawable(
					R.drawable.bg_adventure);
		} else if (category.equalsIgnoreCase("Restaurant")) {
			offers_map_icon = getResources().getDrawable(
					R.drawable.rest_map_icon);
			offers_list_icon = getResources().getDrawable(
					R.drawable.resturant_listing_icon);
			offers_detail_bg = getResources().getDrawable(
					R.drawable.bg_restaurant);
		} else if (category.equalsIgnoreCase("Telecommunication")) {
			offers_map_icon = getResources().getDrawable(
					R.drawable.telecom_map_icon);
			offers_list_icon = getResources().getDrawable(
					R.drawable.telecom_listing_icon);
			offers_detail_bg = getResources()
					.getDrawable(R.drawable.bg_telecom);
		} else if (category.equalsIgnoreCase("Tyre")) {
			// No Icon for 'Tyre'
			offers_map_icon = getResources()
					.getDrawable(R.drawable.offers_icon);
			offers_list_icon = getResources().getDrawable(R.drawable.offer);
			offers_detail_bg = getResources().getDrawable(
					R.drawable.bg_adventure);
		} else if (category.equalsIgnoreCase("Entertainment")) {
			offers_map_icon = getResources().getDrawable(
					R.drawable.entertainment_map_icon);
			offers_list_icon = getResources().getDrawable(
					R.drawable.entertainment_listing_icon);
			offers_detail_bg = getResources().getDrawable(
					R.drawable.bg_entertainment);
		} else if (category.equalsIgnoreCase("Movies")) {
			offers_map_icon = getResources().getDrawable(
					R.drawable.movies_map_icon);
			offers_list_icon = getResources().getDrawable(
					R.drawable.movies_listing_icon);
			offers_detail_bg = getResources().getDrawable(R.drawable.bg_movies);
		} else if (category.equalsIgnoreCase("Travel")) {
			offers_map_icon = getResources().getDrawable(
					R.drawable.travel_map_icon);
			offers_list_icon = getResources().getDrawable(
					R.drawable.travel_listing_icon);
			offers_detail_bg = getResources().getDrawable(R.drawable.bg_travel);
		} else if (category.equalsIgnoreCase("Caf�")
				|| category.equalsIgnoreCase("Cafe")) {
			offers_map_icon = getResources().getDrawable(
					R.drawable.cafe_map_icon);
			offers_list_icon = getResources().getDrawable(
					R.drawable.cafe_listing_icon);
			offers_detail_bg = getResources().getDrawable(R.drawable.bg_cafe);
		} else if (category.equalsIgnoreCase("Adventure")) {
			offers_map_icon = getResources().getDrawable(
					R.drawable.adventure_map_icon);
			offers_list_icon = getResources().getDrawable(
					R.drawable.adventure_listing_icon);
			offers_detail_bg = getResources().getDrawable(
					R.drawable.bg_adventure);
		} else if (category.equalsIgnoreCase("Sport")) {
			offers_map_icon = getResources().getDrawable(
					R.drawable.sport_map_icon);
			offers_list_icon = getResources().getDrawable(
					R.drawable.sport_listing_icon);
			offers_detail_bg = getResources().getDrawable(R.drawable.bg_sport);
		} else if (category.equalsIgnoreCase("Others")) {
			// Change the map N List Icons,,,Here!
			offers_map_icon = getResources().getDrawable(
					R.drawable.other_map_icon);
			offers_list_icon = getResources().getDrawable(
					R.drawable.other_listing_icon);
			offers_detail_bg = getResources().getDrawable(R.drawable.bg_others);
		} else {
			// Set Default Image... If NO category...
			offers_map_icon = getResources().getDrawable(
					R.drawable.other_map_icon);
			offers_list_icon = getResources().getDrawable(
					R.drawable.other_listing_icon);
			offers_detail_bg = getResources().getDrawable(R.drawable.bg_others);
		}

	}

	/**
	 * Show Offer`s DIRECTION on map from my current Location.
	 */
	private void showDirectionOnMap() {
		// TODO Auto-generated method stub
		// showOfferDetail.setImageDrawable(getResources().getDrawable(
		// R.drawable.show_listview_btn_normal));
		// showOfferOnMap.setImageDrawable(getResources().getDrawable(
		// R.drawable.show_mapview_btn_selected));
		// if (offerDetailLayout.getVisibility() == View.VISIBLE) {
		// offerDetailLayout.setVisibility(View.GONE);
		// detailMapView.setVisibility(View.VISIBLE);
		//
		// }
		enableMap();

		mGetDirectionData = (GetDirectionData) new GetDirectionData().execute(
				fromString, toString, viaString);

	}

	/**
	 * Enable MapView Tab.
	 */
	private void enableMap() {
		// TODO Auto-generated method stub
		showOfferDetail.setBackgroundDrawable(getResources().getDrawable(
				R.drawable.show_listview_btn_normal));
		showOfferOnMap.setBackgroundDrawable(getResources().getDrawable(
				R.drawable.show_mapview_btn_selected));
		if (offerDetailLayout.getVisibility() == View.VISIBLE) {
			offerDetailLayout.setVisibility(View.GONE);
			detailMapView.setVisibility(View.VISIBLE);

		}
	}

	@Override
	protected boolean isRouteDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * My custom ALert Dialog!
	 * 
	 * @author Rupesh
	 * 
	 */
	class Myalert extends AlertDialog {

		protected Myalert(Context context, int theme) {
			super(context, theme);
		}

		protected Myalert(Context context) {
			super(context);
		}

	}

	/**
	 * Dialog message shown when no area found.
	 */
	private void noDirectionFound(String title, String msg) {
		// TODO Auto-generated method stub
		LayoutInflater infalter = LayoutInflater
				.from(OffersDetailActivity.this);
		View addressDialog = infalter.inflate(R.layout.addressdialog, null);

		// final AlertDialog.Builder alert=new
		// AlertDialog.Builder(ShowOffersActivity.this);
		final Myalert alert = new Myalert(OffersDetailActivity.this);
		alert.setInverseBackgroundForced(true);
		alert.setView(addressDialog, 0, 0, 0, 0);
		alert.setCancelable(false);
		alert.setIcon(android.R.drawable.btn_default);
		alert.show();

		final ImageView close = (ImageView) addressDialog
				.findViewById(R.id.exit);
		close.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				alert.cancel();

			}
		});

		((TextView) addressDialog.findViewById(R.id.addtitle)).setText(title);
		((TextView) addressDialog.findViewById(R.id.adddetails)).setText(msg);

		// show it
		alert.show();
	}

	private class GetDirectionData extends UserTask<String, Void, String> {
		public void onPreExecute() {
			try {
				if (mPdialog == null)
					mPdialog = new ProgressDialog(OffersDetailActivity.this);

				mPdialog.setMessage("Getting Directions...");
				mPdialog.setIndeterminate(true);
				mPdialog.setCancelable(false);
				mPdialog.show();//

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		public void onPostExecute(String data) {
			String lat = null, lon = null;
			if (mPdialog != null) {
				if (mPdialog.isShowing())
					mPdialog.dismiss();
			}

			if (data == null || data.length() < 20) {
				return;
			}
			try {
				JSONObject jObj = null;
				JSONArray advices = null;
				// Log.d("Direction Data:", "data:"+data);
				if (data != null && data.contains("403 Access Denied")) {
					noDirectionFound("Unable to connect",
							"No directions at the moment. Please, Try again later");
					// Toast.makeText(OffersDetailActivity.this,
					// "No direction at the moment. Please, Try again later",
					// 1).show();

					return;

				} else if ("unknown_host".equalsIgnoreCase(data)) {
					noDirectionFound("Unable to connect",
							"No directions at the moment. Please, Try again later");
					return;

				}
				jObj = new JSONObject(data);

				JSONObject resultObj = jObj.getJSONObject("result");
				distance = resultObj.getString("distance");
				// / Log.e("distance",""+distance);
				time = resultObj.getString("time");
				// Log.e("time",""+time);
				fromX = resultObj.getString("fromX");
				fromY = resultObj.getString("fromY");
				toX = resultObj.getString("toX");
				toY = resultObj.getString("toY");

				String via1X = resultObj.getString("via1X");
				String via1Y = resultObj.getString("via1Y");
				String path = resultObj.getString("path");

				// Log.d("json vpath=>",""+path);

				if (path.length() < 2 || "".equalsIgnoreCase(path)) {
					noDirectionFound("Bad Location",
							"No directions at the moment.");
					return;

				}

				String[] latArray = path.split("\\|");
				String routeLong, routeLang;

				// try
				// {
				// // Log.d("lat n lon=>",""+fromY+"="+fromX);
				// SAXParserFactory
				// mySAXParserFactory=SAXParserFactory.newInstance();
				// SAXParser mySAXParser=mySAXParserFactory.newSAXParser();
				// XMLReader myXMLReader=mySAXParser.getXMLReader();
				// String
				// url="http://apis.mapmyindia.com/v2.0/reversegeocode/key=2c07ede70ef00dd05ab7db0366e64ecd&rtype=json&x="+fromX+"&y="+fromY;
				// // String
				// url="http://apis.mapmyindia.com/v2.0/reversegeocode/key=2c07ede70ef00dd05ab7db0366e64ecd&rtype=json&x=77.2094&y=28.5229";
				// // Log.d("url string=>",""+url);
				// URL myUrl=new URL(url);
				//
				// curLocationDetail=new ArrayList<ContactDetails>();
				// ContactsParser parse=new ContactsParser(curLocationDetail);
				// myXMLReader.setContentHandler(parse);
				// myXMLReader.parse(new InputSource(myUrl.openStream()));
				// }
				// catch(Exception e)
				// {
				// Log.e("Error","Parsing data"+e.toString());
				// }
				//
				// //
				// Log.d("contactDetail.size()=>",""+curLocationDetail.size());
				// for(int i=0;i<curLocationDetail.size();i++)
				// {
				// Locality =curLocationDetail.get(i).getName();
				// stateName=curLocationDetail.get(i).getSname();
				// curCity=curLocationDetail.get(i).getLocality();
				// //
				// Log.d("state n city=>","curCity:"+curCity+", stateName:"+stateName+", Locality:"+Locality);
				//
				// }
				try {
					// Log.d("lat n lon=>",""+toY+"="+toX);
					SAXParserFactory mySAXParserFactory = SAXParserFactory
							.newInstance();
					SAXParser mySAXParser = mySAXParserFactory.newSAXParser();
					XMLReader myXMLReader = mySAXParser.getXMLReader();
					String url = "http://apis.mapmyindia.com/v2.0/reversegeocode/key=2c07ede70ef00dd05ab7db0366e64ecd&rtype=json&x="
							+ toX + "&y=" + toY;
					// String
					// url="http://apis.mapmyindia.com/v2.0/reversegeocode/key=2c07ede70ef00dd05ab7db0366e64ecd&rtype=json&x=77.2094&y=28.5229";
					// Log.d("url string=>",""+url);
					URL myUrl = new URL(url);
					curLocationDetail = new ArrayList<ContactDetails>();
					ContactsParser parse = new ContactsParser(curLocationDetail);
					myXMLReader.setContentHandler(parse);
					myXMLReader.parse(new InputSource(myUrl.openStream()));
				} catch (Exception e) {
					e.printStackTrace();
				}

				// Log.d("contactDetail.size()=>",""+curLocationDetail.size());
				for (int i = 0; i < curLocationDetail.size(); i++) {
					Locality = curLocationDetail.get(i).getName();
					stateName = curLocationDetail.get(i).getSname();
					CurDest = curLocationDetail.get(i).getLocality();
					// Log.d("state n city=>",""+CurDest+"==="+stateName+"==="+Locality);

				}

				RouteOverlay routeOverlay = new RouteOverlay();
				for (String pair : latArray) {
					String coordinates[] = pair.split("\\$");
					GeoPoint geoPoint = new GeoPoint(
							(int) (Double.parseDouble(coordinates[1]) * 1E6),
							(int) (Double.parseDouble(coordinates[0]) * 1E6));// ArrayIndexOutOfBond
																				// ...
																				// When
																				// LatLng
																				// -
																				// 0,0
					// Log.d("routeData=>",""+coordinates[1]+"===>"+coordinates[0]);

					routeOverlay.addGeoPoint(geoPoint);
				}
				// Log.d("geopint size=>",""+routeOverlay.geoPoints.size());

				gp2 = routeOverlay.geoPoints.get(0);
				MapController mapController = detailMapView.getController();
				mapController.animateTo(gp2);
				mapController.setZoom(15);
				// GeoPoint point = new GeoPoint(lat,lng);
				mapOverlays = detailMapView.getOverlays();

				if (detailMapView.getOverlays() != null) {
					// Clear old Overlays...
					detailMapView.getOverlays().clear();
				}

				Drawable atm_icon = getResources()
						.getDrawable(R.drawable.arrow);
				// CustomItemizedOverlay itemizedOverlay =
				// new CustomItemizedOverlay(atm_icon,ctx,itm);
				CustomItemizedOverlay itemizedOverlay = new CustomItemizedOverlay(
						atm_icon, OffersDetailActivity.this, itm);
				OverlayItem overlayitem;
				if (from_current_location) {
					overlayitem = new OverlayItem(gp2, "Current Location",
							curCity);
				} else {
					overlayitem = new OverlayItem(gp2, "City", curCity);
				}

				itemizedOverlay.addOverlay(overlayitem);
				mapOverlays.add(itemizedOverlay);
				int size = routeOverlay.geoPoints.size();
				size = size - 1;
				// Log.d("size-1=>",""+size);
				GeoPoint dest = routeOverlay.geoPoints.get(size);
				// Log.d("branchOverFlag===",""+branchOverFlag);
				// Log.d("atmOverFlag===",""+atmOverFlag);

				// Map Icon Is according to category...
				Drawable at = offers_map_icon;
				// at = getResources().getDrawable(R.drawable.offers_icon);
				CustomItemizedOverlay itemizedOverlay1 = new CustomItemizedOverlay(
						at, OffersDetailActivity.this, itm);
				OverlayItem overlayitem1 = new OverlayItem(dest, /* "Destination" */
						name, add);
				itemizedOverlay1.addOverlay(overlayitem1);
				mapOverlays.add(itemizedOverlay1);

				for (int i = 1; i < routeOverlay.geoPoints.size(); i++) {
					gp1 = gp2;
					gp2 = routeOverlay.geoPoints.get(i);
					Overlay ol1 = new RouteOverlay(gp1, gp2, 2, Color.BLUE);
					mapOverlays.add(ol1);
				}

				Overlay ol2 = new RouteOverlay(OffersDetailActivity.this, gp1,
						gp2, 3);
				mapOverlays.add(ol2);
				try {

					advices = resultObj.getJSONArray("advices");

				} catch (Exception e) {
					// Log.e("JSON Parser", "Error parsing data " +
					// e.toString());
				}
				for (int i = 0; i < advices.length(); i++) {
					try {
						JSONObject robject = advices.getJSONObject(i);
						String meters_from_route_start = robject
								.getString("meters_from_route_start");
						String meters_to_next_advice = robject
								.getString("meters_to_next_advice");
						String advice_text = robject.getString("advice_text");

						JSONObject advice_location = robject
								.getJSONObject("advice_location");
						String x = advice_location.getString("x");
						String y = advice_location.getString("y");

						HashMap<String, String> resultMap = new HashMap<String, String>();
						resultMap.put("distance", distance);
						resultMap.put("time", time);
						resultMap.put("meters_from_route_start",
								meters_from_route_start);
						resultMap.put("meters_to_next_advice",
								meters_to_next_advice);
						resultMap.put("advice_text", advice_text);
						resultMap.put("x", x);
						resultMap.put("y", y);

						// Log.d("DirectionData.size() in Getdirection==",""+DirectionData.size());
						DirectionData.add(resultMap);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

			} catch (Exception e) {
				e.printStackTrace();
				// Show N/W Conn. MSg...
				e.printStackTrace();
			}
		}

		@Override
		public String doInBackground(String... params) {
			String response = null;
			String from = params[0];
			String to = params[1];
			String via = params[2];
			// Log.d("address=>",""+from+"==="+to+"=="+via);
			try {
				URL PostUrl = new URL(
						"http://apis.mapmyindia.com/v2.0/directions/key=2c07ede70ef00dd05ab7db0366e64ecd&rtype=json");
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
						7);
				nameValuePairs.add(new BasicNameValuePair("from", from));
				nameValuePairs.add(new BasicNameValuePair("to", to));
				nameValuePairs.add(new BasicNameValuePair("via1", via));
				nameValuePairs.add(new BasicNameValuePair("route", "0"));
				nameValuePairs.add(new BasicNameValuePair("vehicle", "0"));
				nameValuePairs.add(new BasicNameValuePair("avoid", "0,1"));
				nameValuePairs.add(new BasicNameValuePair("q", "0"));

				/*
				 * Log.v("PostUrl-to get Direction_", "PostUrl"+PostUrl);
				 * Log.v("PostUrl-to get Direction_",
				 * "PostUrl"+from+"&to="+to+"&via="
				 * +via+"&route=0&vehicle=0&avoid=0,1&q=0");
				 * Log.v("PostUrl-to get Direction_",
				 * "http://apis.mapmyindia.com/v2.0/directions/key=2c07ede70ef00dd05ab7db0366e64ecd&rtype=json&from="
				 * +
				 * from+"&to="+to+"&via="+via+"&route=0&vehicle=0&avoid=0,1&q=0"
				 * );
				 */
				// "http://apis.mapmyindia.com/v2.0/directions/key=2c07ede70ef00dd05ab7db0366e64ecd&rtype=json&from="+from+"&to="+to+"&via="+via+"&route=0&vehicle=0&avoid=0,1&q=0"

				ProxyUrlUtil pU = new ProxyUrlUtil();
				response = pU.getPostXML(PostUrl, OffersDetailActivity.this,
						nameValuePairs);
				// Log.d("Responmse in directiondata=>",""+response);
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				response = "unknown_host";
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			// ,,,,,,,,,,,,,NEW
			try {
				// Log.d("lat n lon=>",""+fromY+"="+fromX);
				SAXParserFactory mySAXParserFactory = SAXParserFactory
						.newInstance();
				SAXParser mySAXParser = mySAXParserFactory.newSAXParser();
				XMLReader myXMLReader = mySAXParser.getXMLReader();
				String url = "http://apis.mapmyindia.com/v2.0/reversegeocode/key=2c07ede70ef00dd05ab7db0366e64ecd&rtype=json&x="
						+ c_longitude + "&y=" + c_latitude;
				// String
				// url="http://apis.mapmyindia.com/v2.0/reversegeocode/key=2c07ede70ef00dd05ab7db0366e64ecd&rtype=json&x=77.2094&y=28.5229";
				// Log.d("url string=>",""+url);
				URL myUrl = new URL(url);

				curLocationDetail = new ArrayList<ContactDetails>();
				ContactsParser parse = new ContactsParser(curLocationDetail);
				myXMLReader.setContentHandler(parse);
				myXMLReader.parse(new InputSource(myUrl.openStream()));
			} catch (Exception e) {
				e.printStackTrace();
			}

			// Log.d("contactDetail.size()=>",""+curLocationDetail.size());
			for (int i = 0; i < curLocationDetail.size(); i++) {
				Locality = curLocationDetail.get(i).getName();
				stateName = curLocationDetail.get(i).getSname();
				curCity = curLocationDetail.get(i).getLocality();
				// Log.d("state n city=>","curCity:"+curCity+", stateName:"+stateName+", Locality:"+Locality);

			}
			// ...........nEW WND....
			return response;
		}

	}

	@Override
	public void setItem(OverlayItem item) {
		// TODO Auto-generated method stub

	}

}
