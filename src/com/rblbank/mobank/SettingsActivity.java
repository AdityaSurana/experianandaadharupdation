package com.rblbank.mobank;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.rblbank.mobank.R;
//public class SettingsActivity extends Activity {
//
//	/** Called when the activity is first created. */
//	@Override
//	public void onCreate(Bundle savedInstanceState) {
//	    super.onCreate(savedInstanceState);
//	
//	    // TODO Auto-generated method stub
//	}
//
//}
//package com.snapwork.hdfc;
//import com.snapwork.hdfc.MainGridActivity.ImageAdapter;
//import com.snapwork.hdfc.MainGridActivity.ViewHolder;

public class SettingsActivity extends Activity implements LocationListener {
	private ImageView back, bgo, bcancel;
	private SeekBar mSeekbar;
	private RadioButton gps, manual, radio, radio1, radio2, radio3, radio4,
			radio5;
	private CheckBox restCheck, spaCheck, movieCheck, shopCheck, traCheck,
			allCheck;
	private LinearLayout slinear;
	private EditText searchText;
	private ProgressDialog mPdialog = null;
	// private GetOffersData mGetOffersData;
	private List<HashMap<String, String>> offersData = new ArrayList<HashMap<String, String>>();
	private boolean gpsFlag = true, manFlag = false;
	private String curRadius, city = null;
	LocationManager lm;
	private String lat, lon;
	boolean allFlag, spaFlag, movieFlag, restFlag, shopFlag, tarFlag;
	SharedPreferences settings;
	private TextView llocation, lastarea, gpstext, loctext, selText, arText,
			radius, diText, toText, ofText, restTag, spaTag, movieTag, shopTag,
			traTag, allTag;

	protected void onStart() {
		// initCheckBox();
		super.onStart();
	}

	public void onPause() {
		super.onPause();
	}

	public void onResume() {
		super.onResume();
		// Log.d("Resume of settings","Resume of settings");
		settings = getSharedPreferences("settings", 0);

		gpsFlag = settings.getBoolean("gpsFlag", true);
		manFlag = settings.getBoolean("manFlag", true);

		/*
		 * restFlag=settings.getBoolean("restFlag", false);
		 * spaFlag=settings.getBoolean("spaFlag", false);
		 * movieFlag=settings.getBoolean("movieFlag", false);
		 * shopFlag=settings.getBoolean("shopFlag", false);
		 * tarFlag=settings.getBoolean("tarFlag", false);
		 * allFlag=settings.getBoolean("allFlag", false);
		 */

		selectedRadius = settings.getInt("radius", 100);
		selectProgress = settings.getInt("selectProgress", 50);
		city = settings.getString("city", city);

		// /
		// Log.d("gps man radi city=>",""+gpsFlag+"="+manFlag+"="+selectedRadius+"="+city+"="+selectProgress);

		// Log.i("Flags=>",""+restFlag+"="+spaFlag+"="+movieFlag+"="+shopFlag+"="+tarFlag+"="+allFlag);
		if (gpsFlag) {
			gpsFlag = true;
			manFlag = false;
			gps.setButtonDrawable(R.drawable.radio_btn_clicked);
			manual.setButtonDrawable(R.drawable.radio_btn);
			searchText.setVisibility(View.GONE);
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(searchText.getWindowToken(), 0);
		} else {
			gpsFlag = false;
			manFlag = true;
			searchText.setVisibility(View.VISIBLE);
			searchText.setText(city);
			manual.setButtonDrawable(R.drawable.radio_btn_clicked);
			gps.setButtonDrawable(R.drawable.radio_btn);
		}

		mSeekbar.setProgress(selectProgress);
	}

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.settings);
		// Log.d("settingsActivity","settingsActivity");

		Typeface fontFace = Typeface.createFromAsset(getBaseContext()
				.getAssets(), "fonts/hlr___.ttf");

		back = (ImageView) findViewById(R.id.backbutton);
		bgo = (ImageView) findViewById(R.id.bgo);
		bcancel = (ImageView) findViewById(R.id.bcancel);
		back.setVisibility(View.GONE);

		llocation = (TextView) findViewById(R.id.llocation);
		llocation.setTypeface(fontFace);
		gpstext = (TextView) findViewById(R.id.gpstext);
		gpstext.setTypeface(fontFace);
		loctext = (TextView) findViewById(R.id.loctext);
		loctext.setTypeface(fontFace);
		selText = (TextView) findViewById(R.id.selText);
		selText.setTypeface(fontFace);
		arText = (TextView) findViewById(R.id.arText);
		arText.setTypeface(fontFace);
		radius = (TextView) findViewById(R.id.radius);
		radius.setTypeface(fontFace);
		diText = (TextView) findViewById(R.id.diText);
		diText.setTypeface(fontFace);
		toText = (TextView) findViewById(R.id.toText);
		toText.setTypeface(fontFace);

		Bundle b = getIntent().getExtras();
		String lastlocation = b.getString("lastlocation");
		TextView lastarea = (TextView) findViewById(R.id.lastarea);
		lastarea.setText(lastlocation);
		lastarea.setTypeface(fontFace);

		searchText = (EditText) findViewById(R.id.searchtext);
		searchText.setTypeface(fontFace);

		settings = getSharedPreferences("settings", 0);

		final SharedPreferences.Editor editor = settings.edit();

		// Log.i("Flags=>",""+restFlag+"="+spaFlag+"="+movieFlag+"="+shopFlag+"="+tarFlag+
		// "="+allFlag);

		bgo.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (gps.isSelected()) {
					gpsFlag = true;
					manFlag = false;
				}
				if (manual.isSelected())
					gpsFlag = false;

				SharedPreferences.Editor editor1 = settings.edit();
				editor1.putString("city", searchText.getText().toString());
				editor1.putInt("radius", selectedRadius);
				editor1.putBoolean("gpsFlag", gpsFlag);
				editor1.putInt("selectProgress", selectProgress);
				editor1.putBoolean("manFlag", manFlag);
				editor1.commit();

				if (selectedRadius == 0) {
					selectedRadius = 500;
				}
				curRadius = String.valueOf(selectedRadius);
				boolean showMap = false;

				if (manFlag) {
					if (searchText.getText().toString().length() < 2) {
						final AlertDialog alertDialog = new AlertDialog.Builder(
								SettingsActivity.this).create();
						alertDialog.setTitle("Alert");
						alertDialog.setMessage("Please Enter Location");
						alertDialog.setButton("OK",
								new DialogInterface.OnClickListener() {

									public void onClick(DialogInterface dialog,
											int which) {
										alertDialog.dismiss();
									}
								});
						alertDialog.show();
						searchText.clearFocus();
						return;
					}
				}
				// Log.d("lat n lon=>",""+lat+"==>"+lon);

				Bundle b = new Bundle();
				b.putBoolean("setting", true);
				b.putBoolean("gpsFlag", gpsFlag);
				// Log.i("gpsFlag","gpsFlag "+gpsFlag );
				b.putBoolean("manFlag", manFlag);
				// Log.i("manFlag", "manFlag"+manFlag);
				b.putString("area", searchText.getText().toString());
				// Log.i("area", "area"+searchText.getText().toString());
				b.putString("radius", curRadius);
				// Log.i("radius","radius"+curRadius );
				// b.putString("radius", radius.getText().toString());
				b.putString("lat", lat);
				// Log.i("lat", "lat"+lat);
				b.putString("lon", lon);
				// Log.i("lon","lon"+lon );

				Intent intent = new Intent(SettingsActivity.this,
						MapViewActivity.class);
				// to clear all top activities on it
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				// to start the same Activity if its already running
				intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
				intent.putExtras(b);
				// SettingsActivity.this.finish();
				startActivity(intent);

				// Intent intent=new
				// Intent(SettingsActivity.this,MapViewActivity.class);
				// intent.putExtras(b);
				// startActivity(intent);

			}
		});

		bcancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				SettingsActivity.this.finish();
				Intent intent = new Intent(SettingsActivity.this,
						MapViewActivity.class);
				startActivity(intent);
			}
		});

		mSeekbar = (SeekBar) findViewById(R.id.seekbar);
		// mSeekbar.setProgress(50);
		mSeekbar.setOnSeekBarChangeListener(mclickone);

		gps = (RadioButton) findViewById(R.id.gps);
		manual = (RadioButton) findViewById(R.id.loca);

		if (gps.isSelected()) {
			searchText.setText("");
			gpsFlag = true;
			manFlag = false;
		}
		if (manual.isSelected()) {
			searchText.setText("");
			gpsFlag = false;
			manFlag = true;
		}

	}

	int selectedRadius;
	int selectProgress;
	private OnSeekBarChangeListener mclickone = new OnSeekBarChangeListener() {

		public void onProgressChanged(SeekBar seekBar, int progress,
				boolean fromUser) {
			selectProgress = progress;
			int radius = 100 + 99 * progress;
			selectedRadius = radius;
			TextView tv = (TextView) findViewById(R.id.radius);
			String dis = getMToKm(radius);
			tv.setText(dis);
		}

		public void onStartTrackingTouch(SeekBar seekBar) {

		}

		public void onStopTrackingTouch(SeekBar seekBar) {

		}

	};

	private String getMToKm(int radius) {
		/*
		 * if (radius < 1000) return radius + " M"; else {
		 */
		int km = radius / 1000;
		int m = radius % 1000;
		String dis = km + " KM" + " " + m + " M";
		// if (m > 0)
		// dis += " " + m + " M";
		return dis;
		// }
	}

	public void gpsLocation(View v) {
		SharedPreferences.Editor editor = settings.edit();
		editor.remove("manFlag");
		editor.remove("gpsFlag");
		editor.commit();

		gpsFlag = true;
		manFlag = false;

		gps.setButtonDrawable(R.drawable.radio_btn_clicked);
		manual.setButtonDrawable(R.drawable.radio_btn);
		searchText.setVisibility(View.GONE);
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(searchText.getWindowToken(), 0);
		// Toast.makeText(SettingsActivity.this, "GPS Location Searching",
		// Toast.LENGTH_SHORT).show();
		lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		Criteria criteria = new Criteria();
		String provider = lm.getBestProvider(criteria, false);
		// Location location = getLastKnownLocation(getApplicationContext());
		Location location = lm.getLastKnownLocation(provider);

		if (location != null) {
			System.out.println("Provider " + provider + " has been selected.");
			onLocationChanged(location);
		} else {

		}
		lm.requestLocationUpdates(provider, 20000, 0, this);
	}

	public void manualLocation(View v) {
		SharedPreferences.Editor editor = settings.edit();
		editor.remove("manFlag");
		editor.remove("gpsFlag");
		editor.commit();

		gpsFlag = false;
		manFlag = true;

		searchText.setVisibility(View.VISIBLE);
		searchText.setText("");
		manual.setButtonDrawable(R.drawable.radio_btn_clicked);
		gps.setButtonDrawable(R.drawable.radio_btn);
	}

	@Override
	public void onLocationChanged(Location location) {
		lat = String.valueOf(location.getLatitude());
		lon = String.valueOf(location.getLongitude());
		// Log.e("lat n lon=>",""+lat+"==>"+lon);

	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}

}
