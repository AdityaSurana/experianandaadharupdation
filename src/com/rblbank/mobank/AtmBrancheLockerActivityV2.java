package com.rblbank.mobank;

import com.rblbank.mobank.R;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;

public class AtmBrancheLockerActivityV2 extends FragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_atm_branche_locker_activity_v2);

		addFragmentsPrelogin(new AtmBrancheLockerFragment(), true,
				FragmentTransaction.TRANSIT_NONE,
				FragmentTransaction.TRANSIT_NONE,
				R.id.linearlayout_map_container,
				AtmBrancheLockerActivityV2.this);

	}

	public void addFragmentsPrelogin(Fragment fragment, boolean addToBackStack,
			int transitionIn, int transitionOut, int fragmentContainer,
			Context mContext) {
		// Log.v("addFragments->",
		// "currentViewClicked:"+MarketTodayActivity.currentViewClicked+", cfragmentNM:");
		FragmentTransaction ft = ((FragmentActivity) mContext)
				.getSupportFragmentManager().beginTransaction();
		// for in n out both
		if (transitionIn != FragmentTransaction.TRANSIT_NONE
				&& transitionOut != FragmentTransaction.TRANSIT_NONE) {
			ft.setCustomAnimations(transitionIn, transitionOut);
		}

		// ft.setTransition(transitionIn);
		// ft.replace(fragmentContainer, fragment);
		ft.replace(R.id.linearlayout_map_container, fragment);
		// if(addToBackStack)
		// ft.addToBackStack(hTag);
		ft.commit();
	}
}