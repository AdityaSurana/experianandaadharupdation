package com.rblbank.mobank;

import android.os.Bundle;

import com.worklight.androidgap.WLDroidGap;

public class RBL_iBank extends WLDroidGap {
	/**
	 * RBLApplication instance.
	 */
	RBLApp rblApp;
	public static RBL_iBank context;

	public static RBL_iBank getInstance() {
		return context;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);// index.html
		context = this;
	}

	/**
	 * onWLInitCompleted is called when the Worklight runtime framework
	 * initialization is complete
	 */
	@Override
	public void onWLInitCompleted(Bundle savedInstanceState) {
		super.loadUrl(getWebMainFilePath());
		// Add custom initialization code after this line

		/*
		 * Get Application instance...
		 */
		rblApp = (RBLApp) getApplication();
		// rblApp.connectWLClient(getActivity());

	}

}
