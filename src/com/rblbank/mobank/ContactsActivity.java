package com.rblbank.mobank;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.rblbank.mobank.R;
import com.snapwork.ethreads.UserTask;
import com.snapwork.messages.ContactDetails;
import com.snapwork.parser.ContactsParser;

public class ContactsActivity extends Activity implements LocationListener {
	private TextView ccity, csdetail, phnumber, cloandetail, cdematedetail,
			creditphnumber, agentphnumber, credittitle, crdcity, crdphnumber;
	private Spinner cityspinner, crdpinner;
	private Button cdown, crddown;
	private ImageView /* cdown, */call, creditcall, creditagentcall, back,
			numback, creditnumback, creditagentnumback, crdcall, crdnumback;
	private ListView citylist;
	ArrayAdapter<CharSequence> adapter;
	private List<ContactDetails> contactDetail;
	private String lat, lon, cityName, stateName, creditCity, creditState;
	LocationManager lm;
	private List<HashMap<String, String>> contactlist = new ArrayList<HashMap<String, String>>();
	private List<HashMap<String, String>> creditcontactlist = new ArrayList<HashMap<String, String>>();
	String[] cities, creditcities;
	int position;
	Typeface fontFace;
	boolean gpsFlag;
	boolean temp;
	private TextView titletext, ccredit, creditcity, creditagent,
			creditagentdetail, cstitle, cloantitle, cdematetitle;
	private ProgressDialog mPdialog = null;
	private GetLocation mGetLocation;
	private double latitude, longitude;
	private String CurrentLat;
	private String CurrentLon;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.contacts);

		fontFace = Typeface.createFromAsset(getBaseContext().getAssets(),
				"fonts/hlr___.ttf");

		phnumber = (TextView) findViewById(R.id.phnumber);
		phnumber.setTypeface(fontFace);
		ccity = (TextView) findViewById(R.id.ccity);
		ccity.setTypeface(fontFace);
		cityspinner = (Spinner) findViewById(R.id.cityspinner);
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
				this, R.array.City, R.layout.my_spinner_text);
		cityspinner.setAdapter(adapter);
		cityspinner.setOnItemSelectedListener(new MyOnItemSelectedListener());

		// crdpinner=(Spinner)findViewById(R.id.crdpinner);
		// ArrayAdapter<CharSequence> madapter =
		// ArrayAdapter.createFromResource(
		// this, R.array.CreditCard, R.layout.my_spinner_text);
		// crdpinner.setAdapter(madapter);
		// cityspinner.setAdapter(new ContactAdapter(ContactsActivity.this,
		// contactlist));
		// crdpinner.setOnItemSelectedListener(new
		// CreditCardSelectedListener());
		csdetail = (TextView) findViewById(R.id.csdetail);
		csdetail.setTypeface(fontFace);
		cloandetail = (TextView) findViewById(R.id.cloandetail);
		cloandetail.setTypeface(fontFace);
		cdematedetail = (TextView) findViewById(R.id.cdematedetail);
		cdematedetail.setTypeface(fontFace);
		creditphnumber = (TextView) findViewById(R.id.creditphnumber);
		creditphnumber.setTypeface(fontFace);
		agentphnumber = (TextView) findViewById(R.id.agentphnumber);
		agentphnumber.setTypeface(fontFace);

		titletext = (TextView) findViewById(R.id.titletext);
		titletext.setTypeface(fontFace);
		ccredit = (TextView) findViewById(R.id.ccredit);
		ccredit.setTypeface(fontFace);
		creditcity = (TextView) findViewById(R.id.creditcity);
		creditcity.setTypeface(fontFace);
		creditagent = (TextView) findViewById(R.id.creditagent);
		creditagent.setTypeface(fontFace);
		creditagentdetail = (TextView) findViewById(R.id.creditagentdetail);
		creditagentdetail.setTypeface(fontFace);
		cstitle = (TextView) findViewById(R.id.cstitle);
		cstitle.setTypeface(fontFace);
		cloantitle = (TextView) findViewById(R.id.cloantitle);
		cloantitle.setTypeface(fontFace);
		cdematetitle = (TextView) findViewById(R.id.cdematetitle);
		cdematetitle.setTypeface(fontFace);
		/*
		 * credittitle=(TextView)findViewById(R.id.credittitle);
		 * credittitle.setTypeface(fontFace);
		 * crdcity=(TextView)findViewById(R.id.crdcity);
		 * crdcity.setTypeface(fontFace);
		 * crdphnumber=(TextView)findViewById(R.id.crdphnumber);
		 * crdphnumber.setTypeface(fontFace);
		 */

		// cdown=(ImageView)findViewById(R.id.cdown);
		cdown = (Button) findViewById(R.id.cdown);
		// crddown=(Button)findViewById(R.id.crddown);
		call = (ImageView) findViewById(R.id.call);
		creditcall = (ImageView) findViewById(R.id.creditcall);
		creditagentcall = (ImageView) findViewById(R.id.creditagentcall);
		back = (ImageView) findViewById(R.id.backbutton);
		/*
		 * crdcall=(ImageView)findViewById(R.id.crdcall);
		 * crdnumback=(ImageView)findViewById(R.id.crdnumback);
		 */

		numback = (ImageView) findViewById(R.id.numback);
		creditnumback = (ImageView) findViewById(R.id.creditnumback);
		creditagentnumback = (ImageView) findViewById(R.id.creditagentnumback);

		String data = readFile(this.getApplicationContext(), R.raw.phonebanking);
		String[] city = data.split(",");
		for (int i = 0; i < city.length; i++) {
			cities = city[i].split(":");
			HashMap<String, String> contact = new HashMap<String, String>();
			contact.put("city", cities[0]);
			contact.put("phone", cities[1]);
			contactlist.add(contact);
		}

		String creditdata = readFile(this.getApplicationContext(),
				R.raw.creditcard);
		String[] creditcity = creditdata.split(",");
		for (int i = 0; i < creditcity.length; i++) {
			creditcities = creditcity[i].split(":");
			HashMap<String, String> contact = new HashMap<String, String>();
			contact.put("city", creditcities[0]);
			contact.put("phone", creditcities[1]);
			creditcontactlist.add(contact);
		}

		csdetail.setText("Banking services are combination of IVR and Agent offering depending on the type of transaction. All IVR transactions are available 24x7 on all days. Reporting loss of cards is a PhoneBanker-assisted facility and is available 24 hours on all days.");
		cloandetail
				.setText("Loan services are available between 8:00 a.m. to 8:00 p.m. on all days including Sundays & Bank Holidays.");
		cdematedetail
				.setText("Services are available between 8:30 a.m to 5:30 p.m on weekdays & 9:30 a.m to 1:30 p.m on Saturdays (This facility is not available on Sundays & Bank Holidays)");

		cdown.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// citylist.setVisibility(View.VISIBLE);
				cityspinner.performClick();
				// ccity.setText("test");
				temp = true;

			}
		});
		/*
		 * crddown.setOnClickListener(new OnClickListener() {
		 * 
		 * @Override public void onClick(View v) {
		 * //citylist.setVisibility(View.VISIBLE); crdpinner.performClick();
		 * //ccity.setText("test"); temp=true;
		 * 
		 * } });
		 */
		ccity.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// citylist.setVisibility(View.VISIBLE);
				cityspinner.performClick();
				// ccity.setText("test");
				temp = true;
			}
		});
		/*
		 * crdcity.setOnClickListener(new OnClickListener() {
		 * 
		 * @Override public void onClick(View v) {
		 * //citylist.setVisibility(View.VISIBLE); crdpinner.performClick();
		 * //ccity.setText("test"); temp=true; } });
		 */back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				ContactsActivity.this.finish();
				// Intent intent=new
				// Intent(ContactsActivity.this,MainGridActivity.class);
				// startActivity(intent);
			}
		});

		numback.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String number = "tel:" + phnumber.getText().toString().trim();
				Intent callIntent = new Intent(Intent.ACTION_CALL, Uri
						.parse(number));
				startActivity(callIntent);
			}
		});

		creditnumback.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String number = "tel:"
						+ creditphnumber.getText().toString().trim();
				Intent callIntent = new Intent(Intent.ACTION_CALL, Uri
						.parse(number));
				startActivity(callIntent);
			}
		});

		creditagentnumback.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String number = "tel:"
						+ agentphnumber.getText().toString().trim();
				Intent callIntent = new Intent(Intent.ACTION_CALL, Uri
						.parse(number));
				startActivity(callIntent);
			}
		});
		call.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String number = "tel:" + phnumber.getText().toString().trim();
				Intent callIntent = new Intent(Intent.ACTION_CALL, Uri
						.parse(number));
				startActivity(callIntent);
			}
		});
		creditcall.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String number = "tel:"
						+ creditphnumber.getText().toString().trim();
				Intent callIntent = new Intent(Intent.ACTION_CALL, Uri
						.parse(number));
				startActivity(callIntent);
			}
		});

		creditagentcall.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String number = "tel:"
						+ agentphnumber.getText().toString().trim();
				Intent callIntent = new Intent(Intent.ACTION_CALL, Uri
						.parse(number));
				startActivity(callIntent);
			}
		});

		/*
		 * crdcall.setOnClickListener(new OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { String number = "tel:" +
		 * crdphnumber.getText().toString().trim(); Intent callIntent = new
		 * Intent(Intent.ACTION_CALL, Uri.parse(number));
		 * startActivity(callIntent); } });
		 * 
		 * crdnumback.setOnClickListener(new OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { String number = "tel:" +
		 * crdphnumber.getText().toString().trim(); Intent callIntent = new
		 * Intent(Intent.ACTION_CALL, Uri.parse(number));
		 * startActivity(callIntent); } });
		 */

		Location location = getLastKnownLocation(getApplicationContext());
		if (location != null) {
			onLocationChanged(location);
			mGetLocation = (GetLocation) new GetLocation().execute();
		} else {
			// Toast.makeText( getApplicationContext(),
			// "Your current location is temporary unavailable!!!",
			// Toast.LENGTH_LONG).show();
		}

	}

	public static Location getLastKnownLocation(Context context) {
		LocationManager manager = (LocationManager) context
				.getSystemService(Context.LOCATION_SERVICE);

		Location location = null;
		Location locationGPS = null;
		String provider = LocationManager.GPS_PROVIDER;
		// System.out.println("Provider " + provider + " has been selected.");
		if (manager.isProviderEnabled(provider))
			locationGPS = manager.getLastKnownLocation(provider);
		Location locationNetwork = null;
		provider = LocationManager.NETWORK_PROVIDER;
		// System.out.println("Provider " + provider + " has been selected.");
		if (manager.isProviderEnabled(provider))
			locationNetwork = manager.getLastKnownLocation(provider);
		if (locationGPS == null)
			location = locationNetwork;
		else {
			location = locationGPS;
			if (locationNetwork != null
					&& locationNetwork.getTime() > locationGPS.getTime())
				location = locationNetwork;
		}

		return location;
	}

	public static String readFile(Context ctx, int resId) {
		InputStream inputStream = ctx.getResources().openRawResource(resId);

		InputStreamReader inputreader = new InputStreamReader(inputStream);
		BufferedReader buffreader = new BufferedReader(inputreader);
		String line;
		StringBuilder text = new StringBuilder();

		try {
			while ((line = buffreader.readLine()) != null) {
				text.append(line);
				text.append('\n');
			}
		} catch (IOException e) {
			return null;
		}
		return text.toString();
	}

	boolean UnknownHost;

	private class GetLocation extends UserTask<String, Void, String> {
		public void onPreExecute() {
			try {
				if (mPdialog == null)
					mPdialog = new ProgressDialog(ContactsActivity.this);

				mPdialog.setMessage("Loading Contact Information..");
				mPdialog.setIndeterminate(true);
				mPdialog.setCancelable(true);
				mPdialog.show();

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		public void onPostExecute(String data) {
			if (mPdialog != null) {
				if (mPdialog.isShowing())
					mPdialog.dismiss();
			}

			if (UnknownHost) {
				return;
			}

			if (contactDetail.size() == 0) {
				// Log.d("text change","text change");
				ccity.setText("Mumbai");
				phnumber.setText("(+91)(22)6160 6161");
				// crdcity.setText("Mumbai");
				// crdphnumber.setText("(+91)(22)28564332");
				return;
			}

			for (int i = 0; i < contactDetail.size(); i++) {
				cityName = contactDetail.get(i).getName();
				stateName = contactDetail.get(i).getSname();
				// creditCity =contactDetail.get(i).getName();
				// creditState =contactDetail.get(i).getSname();
			}

			for (int j = 0; j < contactlist.size(); j++) {
				if (cityName != null
						&& cityName.equalsIgnoreCase(contactlist.get(j).get(
								"city"))) {
					ccity.setText(cityName);
					phnumber.setText(contactlist.get(j).get("phone"));
					break;
				} else if (stateName != null
						&& stateName.equalsIgnoreCase(contactlist.get(j).get(
								"city"))) {
					ccity.setText(stateName);
					phnumber.setText(contactlist.get(j).get("phone"));
				}
			}

			/*
			 * for(int j=0;j<creditcontactlist.size();j++) {
			 * if(creditCity.equalsIgnoreCase
			 * (creditcontactlist.get(j).get("city"))) {
			 * //Log.d("posiition=>",contactlist.get(j).get("phone"));
			 * crdcity.setText(cityName);
			 * crdphnumber.setText(creditcontactlist.get(j).get("phone"));
			 * break; } else
			 * if((creditcontactlist.get(j).get("city")).contains(creditState))
			 * { crdcity.setText((creditcontactlist.get(j).get("city")));
			 * crdphnumber.setText(creditcontactlist.get(j).get("phone")); } }
			 */
		}

		@Override
		public String doInBackground(String... params) {

			try {
				// Log.d("lat n long=",""+latitude+"=="+longitude);
				SAXParserFactory mySAXParserFactory = SAXParserFactory
						.newInstance();
				SAXParser mySAXParser = mySAXParserFactory.newSAXParser();
				XMLReader myXMLReader = mySAXParser.getXMLReader();
				// String
				// url="http://apis.mapmyindia.com/v2.0/reversegeocode/key=2c07ede70ef00dd05ab7db0366e64ecd&rtype=json&x=25.2499478&y=55.2995398";
				// //UAE
				String url = "http://apis.mapmyindia.com/v2.0/reversegeocode/key=2c07ede70ef00dd05ab7db0366e64ecd&rtype=json&x="
						+ CurrentLon + "&y=" + CurrentLat;
				// String
				// url="http://apis.mapmyindia.com/v2.0/reversegeocode/key=2c07ede70ef00dd05ab7db0366e64ecd&rtype=json&x=72.8068538&y=21.19357";
				// //surat
				// String
				// url="http://apis.mapmyindia.com/v2.0/reversegeocode/key=2c07ede70ef00dd05ab7db0366e64ecd&rtype=json&x=72.5646159&y=23.0396894";
				// //Ahmedabad
				// String
				// url="http://apis.mapmyindia.com/v2.0/reversegeocode/key=2c07ede70ef00dd05ab7db0366e64ecd&rtype=json&x=72.8833356&y=19.0621069";
				// //Mumbai
				URL myUrl = new URL(url);
				contactDetail = new ArrayList<ContactDetails>();
				ContactsParser parse = new ContactsParser(contactDetail);
				myXMLReader.setContentHandler(parse);
				myXMLReader.parse(new InputSource(myUrl.openStream()));
			} catch (Exception e) {
				e.printStackTrace();
				UnknownHost = true;
			}

			return "Locality";
		}
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}

	public class MyOnItemSelectedListener implements OnItemSelectedListener {

		@Override
		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {

			if (temp) {
				ccity.setText(parent.getItemAtPosition(pos).toString());
				phnumber.setText(contactlist.get(pos).get("phone"));
			}

		}

		@Override
		public void onNothingSelected(AdapterView<?> parent) {
			// TODO Auto-generated method stub

		}
	}

	/*
	 * public class CreditCardSelectedListener implements OnItemSelectedListener
	 * {
	 * 
	 * @Override public void onItemSelected(AdapterView<?> parent,View view, int
	 * pos, long id) {
	 * 
	 * if(temp){ crdcity.setText(parent.getItemAtPosition(pos).toString());
	 * crdphnumber.setText(creditcontactlist.get(pos).get("phone")); }
	 * 
	 * 
	 * }
	 * 
	 * @Override public void onNothingSelected(AdapterView<?> parent) { // TODO
	 * Auto-generated method stub
	 * 
	 * } }
	 */

	@Override
	public void onLocationChanged(Location location) {
		latitude = location.getLatitude();
		longitude = location.getLongitude();

		CurrentLat = String.valueOf(latitude);
		CurrentLon = String.valueOf(longitude);
	}
}
