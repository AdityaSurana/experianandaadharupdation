package com.rblbank.mobank;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gcm.GCMBaseIntentService;

@SuppressLint("NewApi")
public class GCMIntentService extends GCMBaseIntentService {

	private static final String TAG = "NotificationText";
	private static final String TokenID = "";
	private static String notificationId = "";

	// private String notificationIdHP = "";

	private SharedPreferences sharedpreferences;

	private Editor editor;

	public GCMIntentService() {
		super("GCMIntentService");
	}

	@Override
	public void onRegistered(Context context, String regId) {

		// Log.v(TAG, "onRegistered: "+ regId);

		JSONObject json;

		try {
			json = new JSONObject().put("event", "registered");
			json.put("regid", regId);

			// Log.v(TAG, "onRegistered: " + json.toString());

			// Send this JSON data to the JavaScript application above EVENT
			// should be set to the msg type
			// In this case this is the registration ID
			PushPlugin.sendJavascript(json);

		} catch (JSONException e) {
			// No message to the user is sent, JSON failed
			Log.e(TAG, "onRegistered: JSON exception");
		}
	}

	@Override
	public void onUnregistered(Context context, String regId) {
		// Log.d(TAG, "onUnregistered - regId: " + regId);
	}

	@Override
	protected void onMessage(Context context, Intent intent) {
		// Log.d(TAG, "onMessage - context: " + context);

		sharedpreferences = getSharedPreferences("RBL_MO_BANK",
				Context.MODE_PRIVATE);
		editor = sharedpreferences.edit();

		// Extract the payload from the message
		Bundle extras = intent.getExtras();
		boolean isDuplicate = false;

		//Log.d(TAG, "" + extras);

		// Log.d("NotificationText", "payload - " +
		// extras.getString("payload"));
		if (extras != null) {
			try {
				if (extras.getString("payload") != null
						&& extras.getString("payload").length() != 0) {

					if (sharedpreferences.getString("notificationIdHP", "")
							.trim().equalsIgnoreCase(extras.getString("payload").trim())) {
						//Log.d(TAG, "Duplicate");
						isDuplicate = true;
					} else {
						//Log.d(TAG, "Not Duplicate");
						isDuplicate = false;

						// notificationIdHP = "null";
						editor.putString("notificationIdHP", extras.getString("payload").trim()).commit();
						new Handler().postDelayed(new Runnable() {

							@Override
							public void run() {
								// TODO Auto-generated method stub

								editor.putString("notificationIdHP", "")
										.commit();
							}
						}, 1000 * 5);
					}

					// if (notificationId.equalsIgnoreCase(extras
					// .getString("payload"))) {
					// Log.d("NotificationText",
					// "" + extras.getString("payload"));
					// }
					// Log.d(TAG,
					// "deeplinking - " + extras.getString("deeplinking"));

					// String data = extras.getString("deeplinking");
					// Log.d(TAG, "" + data);

					// extras.remove("deeplinking");

					// }
					// Log.d("NotificationText",
					// "deeplinking again - " +
					// extras.getString("deeplinking"));

					// if (extras.getString("deeplinking") != null
					// && extras.getString("deeplinking").length() != 0
					// && !extras.getString("deeplinking")
					// .equalsIgnoreCase("null")) {
					// data = data.replace("\\", "");
					// data = data.replace("{", "");
					// data = data.replace("}", "");
					// data = data.trim();
					// Log.d(TAG, "" + data);
					// String[] dataArr = data.split(":");
					// // Log.d("NotificationText", "" + dataArr.length);
					// Log.d(TAG, "" + dataArr[1]);
					//
					// if (sharedpreferences.getString("notificationIdHP", "")
					// .trim().equalsIgnoreCase(dataArr[1].trim())) {
					// Log.d(TAG, "Duplicate");
					// isDuplicate = true;
					// } else {
					// Log.d(TAG, "Not Duplicate");
					// isDuplicate = false;
					//
					// // notificationIdHP = dataArr[1];
					// editor.putString("notificationIdHP", dataArr[1])
					// .commit();
					//
					// new Handler().postDelayed(new Runnable() {
					//
					// @Override
					// public void run() {
					// // TODO Auto-generated method stub
					//
					// editor.putString("notificationIdHP", "")
					// .commit();
					// }
					// }, 1000*5);
					// }
					// } else {
					// if (sharedpreferences.getString("notificationIdHP", "")
					// .trim().equalsIgnoreCase("null")) {
					// Log.d(TAG, "Duplicate");
					// isDuplicate = true;
					// } else {
					// Log.d(TAG, "Not Duplicate");
					// isDuplicate = false;
					//
					// // notificationIdHP = "null";
					// editor.putString("notificationIdHP", "null")
					// .commit();
					// new Handler().postDelayed(new Runnable() {
					//
					// @Override
					// public void run() {
					// // TODO Auto-generated method stub
					//
					// editor.putString("notificationIdHP", "")
					// .commit();
					// }
					// }, 1000*5);
					// }
					// }

					// Log.d("NotificationText", notificationIdHP);
					// Log.d("NotificationText", "" + dataArr[0]);
					// if (dataArr[1] == "") {
					//
					// Log.d("NotificationText", "SW");
					// } else {
					// Log.d("NotificationText",
					// "" + extras.getString("NotificationPoolID"));
					// }
					// Log.d("NotificationText",
					// "" + extras.getString("NotificationPoolID"));

				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			// if we are in the foreground, just surface the payload, else post
			// it
			// to the statusbar
			if (!isDuplicate) {
				if (PushPlugin.isInForeground()) {
					extras.putBoolean("foreground", true);
					PushPlugin.sendExtras(extras);
					// Log.d(TAG, "onMessage 1");
				} else {
					extras.putBoolean("foreground", false);

					// Send a notification if there is a message
					if (extras.getString("payload") != null
							&& extras.getString("payload").length() != 0) {

						// for (String key : extras.keySet()) {
						// Object value = extras.get(key);
						// Log.e(TAG,
						// "called:"
						// + String.format("%s %s (%s)", key,
						// value.toString(), value
						// .getClass()
						// .getName()));
						// }
						//
						// Log.e(TAG,
						// "on message called:"
						// + extras.getString("payload"));
						createNotification(context, extras);
						PushPlugin.sendExtras(extras);
						// Log.d(TAG, "onMessage 3");
					}
					// Log.d(TAG, "onMessage 2");
				}
			} else {
				// notification is not shown when we've received duplicate
				// notificationPoolId
			}
		}
	}

	public void createNotification(Context context, Bundle extras) {

		// Log.e("createNotification", "called");

		if (extras.getString("image").equalsIgnoreCase("null")) {

			//System.out.println("in generate picture normal");
			/*
			 * NotificationManager mNotificationManager = (NotificationManager)
			 * getSystemService(Context.NOTIFICATION_SERVICE); String appName =
			 * getAppName(this);
			 * 
			 * 
			 * PackageManager pm = getPackageManager(); Intent
			 * notificationIntent =
			 * pm.getLaunchIntentForPackage(getApplicationContext
			 * ().getPackageName());
			 * 
			 * //Intent notificationIntent = new Intent(this,
			 * PushHandlerActivity.class);
			 * notificationIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP |
			 * Intent.FLAG_ACTIVITY_CLEAR_TOP);
			 * notificationIntent.putExtra("pushBundle", extras);
			 * 
			 * PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
			 * notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
			 * 
			 * int defaults = Notification.DEFAULT_ALL;
			 * 
			 * if (extras.getString("defaults") != null) { try { defaults =
			 * Integer.parseInt(extras.getString("defaults")); } catch
			 * (NumberFormatException e) {} }
			 * 
			 * 
			 * 
			 * Notification notification = new NotificationCompat.Builder(this)
			 * .setDefaults(defaults)
			 * .setSmallIcon(context.getApplicationInfo().icon)
			 * .setWhen(System.currentTimeMillis()) .setContentTitle(appName)
			 * .setTicker(extras.getString("payload"))
			 * .setContentIntent(contentIntent) .setAutoCancel(true).build();
			 * 
			 * notification.contentView = new RemoteViews(this.getPackageName(),
			 * R.layout.custom_notification);//set your custom layout
			 * 
			 * notification.contentView.setImageViewResource(R.id.notification_image
			 * , context.getApplicationInfo().icon); String message =
			 * extras.getString("payload"); if (message != null) {
			 * notification.contentView.setTextViewText(R.id.notification_text,
			 * message); } else {
			 * notification.contentView.setTextViewText(R.id.notification_text,
			 * "<missing message content>"); }
			 * 
			 * int notId =0;
			 * 
			 * mNotificationManager.notify((String) appName, notId,
			 * notification);
			 */
			long when = System.currentTimeMillis();
			int notification_id = (int) when;
			NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
			String appName = getAppName(this);

			PackageManager pm = getPackageManager();
			Intent notificationIntent = pm
					.getLaunchIntentForPackage(getApplicationContext()
							.getPackageName());

			// notificationIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP |
			// Intent.FLAG_ACTIVITY_CLEAR_TOP);
			notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
					| Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
			notificationIntent.putExtra("pushBundle", extras);

			PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
					notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

			// Use NotificationCompat.Builder to set up our notification.
			NotificationCompat.Builder builder = new NotificationCompat.Builder(
					context);
			builder.setDefaults(Notification.DEFAULT_SOUND);

			// icon appears in device notification bar and right hand corner of
			// notification
			builder.setSmallIcon(context.getApplicationInfo().icon);

			// Content title, which appears in large type at the top of the
			// notification
			builder.setContentTitle(appName);

			// Content text, which appears in smaller text below the title
			String message = extras.getString("payload");
			if (message != null) {
				builder.setContentText(message);
			} else {
				builder.setContentText("<missing message content>");
			}

			builder.setStyle(new NotificationCompat.BigTextStyle()
					.bigText(message));

			builder.setDefaults(Notification.DEFAULT_VIBRATE);

			// Set the intent that will fire when the user taps the
			// notification.
			builder.setContentIntent(contentIntent);

			// set auto cancel
			builder.setAutoCancel(true);

			// Large icon appears on the left of the notification
			builder.setLargeIcon(BitmapFactory.decodeResource(
					context.getResources(), R.drawable.icon));
			int notId = 0;

			// Log.e(TAG, "createnotification called:" +
			// "showing notification");
			mNotificationManager.notify((String) appName, notification_id,
					builder.build());

		} else {
		//System.out.println("in generate picture");
			String message = extras.getString("payload");
			String imageURL = extras.getString("image");
			String appName = getAppName(this);
			new generatePictureStyleNotification(this, appName, message,
					imageURL).execute();
		}
	}

	private static String getAppName(Context context) {
		CharSequence appName = context.getPackageManager().getApplicationLabel(
				context.getApplicationInfo());

		return (String) appName;
	}

	@Override
	public void onError(Context context, String errorId) {
		Log.e(TAG, "onError - errorId: " + errorId);
	}

}
