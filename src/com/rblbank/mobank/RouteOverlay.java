package com.rblbank.mobank;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.Projection;

public class RouteOverlay extends Overlay {

	private GeoPoint gp1;
	private GeoPoint gp2;
	private int mode = 0;
	private int defaultColor;
	Context mContext;

	public List<GeoPoint> geoPoints;
	private int mRadius = 50;

	public RouteOverlay() {
		geoPoints = new ArrayList<GeoPoint>();
	}

	public RouteOverlay(Context context, GeoPoint gp1, GeoPoint gp2, int mode) // GeoPoint
																				// is
																				// a
																				// int.
																				// (6E)
	{
		this.gp1 = gp1;
		this.gp2 = gp2;
		this.mode = mode;
		this.mContext = context;
		defaultColor = 999; // no defaultColor

	}

	public RouteOverlay(GeoPoint gp1, GeoPoint gp2, int mode, int defaultColor) {
		this.gp1 = gp1;
		this.gp2 = gp2;
		this.mode = mode;
		this.defaultColor = defaultColor;
	}

	public int getMode() {
		return mode;
	}

	public void addGeoPoint(GeoPoint gp) {
		geoPoints.add(gp);
	}

	/*
	 * @Override public boolean draw(Canvas canvas, MapView mapView, boolean
	 * shadow, long when) {
	 * 
	 * Log.d("Shadow=>",""+shadow); if (shadow == false) { Projection projection
	 * = mapView.getProjection(); Paint paint = new Paint();
	 * paint.setAntiAlias(true);
	 * 
	 * GeoPoint geoPointFrom = null; GeoPoint geoPointTo = null; Point pointFrom
	 * = new Point(); Point pointTo = new Point();
	 * 
	 * //Route for (GeoPoint geoPoint : geoPoints) {
	 * Log.d("GEOPOints=>",""+geoPointFrom+"====="+geoPointTo); if(geoPointFrom
	 * != null) { geoPointTo = geoPoint;
	 * 
	 * projection.toPixels(geoPointFrom, pointFrom);
	 * projection.toPixels(geoPointTo, pointTo);
	 * 
	 * paint.setColor(Color.GREEN); paint.setStrokeWidth(5);
	 * //paint.setAlpha(120);
	 * 
	 * canvas.drawLine(pointFrom.x, pointFrom.y, pointTo.x, pointTo.y, paint);
	 * 
	 * geoPointFrom = geoPointTo; } else { geoPointFrom = geoPoint; } }
	 * 
	 * //Start point paint.setColor(Color.BLUE);
	 * projection.toPixels(geoPoints.get(0), pointFrom); RectF ovalStart = new
	 * RectF(pointFrom.x - mRadius, pointFrom.y - mRadius, pointFrom.x +
	 * mRadius, pointFrom.y + mRadius); canvas.drawOval(ovalStart, paint);
	 * 
	 * //Stop point paint.setColor(Color.RED); RectF ovalStop = new
	 * RectF(pointTo.x - mRadius, pointTo.y - mRadius, pointTo.x + mRadius,
	 * pointTo.y + mRadius); canvas.drawOval(ovalStop, paint); }
	 * 
	 * return super.draw(canvas, mapView, shadow, when); }
	 */

	public boolean draw(Canvas canvas, MapView mapView, boolean shadow,
			long when) {
		Projection projection = mapView.getProjection();
		if (shadow == false) {
			Paint paint = new Paint();
			paint.setAntiAlias(true);
			Point point = new Point();
			projection.toPixels(gp1, point);
			// mode=1&#65306;start
			if (mode == 1) { // Log.d("mode 1","mode 1");
				if (defaultColor == 999)
					paint.setColor(Color.BLUE);
				else
					paint.setColor(defaultColor);
				// start point
			}
			// mode=2&#65306;path
			else if (mode == 2) {
				// Log.d("mode 2","mode 2");
				if (defaultColor == 999)
					paint.setColor(Color.RED);
				else
					paint.setColor(defaultColor);
				Point point2 = new Point();
				projection.toPixels(gp2, point2);
				paint.setStrokeWidth(5);
				paint.setAlpha(120);
				canvas.drawLine(point.x, point.y, point2.x, point2.y, paint);

			}
			/* mode=3&#65306;end */
			else if (mode == 3) {
				/* the last path */
				// Log.d("mode 3","mode 3");
				if (defaultColor == 999)
					paint.setColor(Color.GREEN);
				else
					paint.setColor(defaultColor);
				Point point2 = new Point();
				projection.toPixels(gp2, point2);
				paint.setStrokeWidth(5);
				paint.setAlpha(120);
				canvas.drawLine(point.x, point.y, point2.x, point2.y, paint);
				/* end point */

			}
		}
		return super.draw(canvas, mapView, shadow, when);
	}
}
