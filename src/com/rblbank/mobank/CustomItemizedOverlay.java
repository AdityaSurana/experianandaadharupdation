package com.rblbank.mobank;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.rblbank.mobank.R;
import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.OverlayItem;
import com.snapwork.messages.ReplyItem;

public class CustomItemizedOverlay extends ItemizedOverlay<OverlayItem> {

	private ArrayList<OverlayItem> mapOverlays = new ArrayList<OverlayItem>();

	private Context context, mContext;
	private Dialog dialog;
	private Myalert alertDialog;
	private ReplyItem reply;

	public CustomItemizedOverlay(Drawable defaultMarker) {
		super(boundCenterBottom(defaultMarker));
		// mContext = new Context();
	}

	public CustomItemizedOverlay(Drawable defaultMarker, Context context) {
		// super(boundCenterBottom(defaultMarker));
		this(defaultMarker);
		this.context = context;
		populate();
	}

	public CustomItemizedOverlay(Drawable defaultMarker, Context context,
			ReplyItem reply) {
		// super(boundCenterBottom(defaultMarker));
		this(defaultMarker);
		this.context = context;
		this.reply = reply;
	}

	@Override
	protected OverlayItem createItem(int i) {
		return mapOverlays.get(i);
	}

	@Override
	public int size() {
		return mapOverlays.size();
	}

	@Override
	protected boolean onTap(int index) {
		Typeface fontFace = Typeface.createFromAsset(this.context.getAssets(),
				"fonts/hlr___.ttf");
		OverlayItem item = mapOverlays.get(index);
		AlertDialog builder = new AlertDialog.Builder(this.context).create();

		LayoutInflater infalter = LayoutInflater.from(context);
		View addressDialog = infalter.inflate(R.layout.addressdialog, null);
		/*
		 * builder.setView(addressDialog,0,0,0,0);
		 * builder.setTitle(item.getTitle());
		 * builder.setMessage(item.getSnippet()); builder.setCancelable(true);
		 * 
		 * ((TextView)addressDialog.findViewById(R.id.addtitle)).setText(item.
		 * getTitle());
		 * 
		 * ((TextView)addressDialog.findViewById(R.id.adddetails)).setText(item.
		 * getSnippet());
		 * 
		 * ((ImageView)addressDialog.findViewById(R.id.exit)).setOnClickListener(
		 * new OnClickListener() {
		 * 
		 * public void onClick(View v) { dialog.dismiss(); } }); dialog=builder;
		 * dialog.show();
		 */

		alertDialog = new Myalert(context);
		// alertDialog.setView(bluedialogView);
		alertDialog.setInverseBackgroundForced(true);
		// alertDialog.setView(bluedialogView);
		alertDialog.setView(addressDialog, 0, 0, 0, 0);
		final ImageView close = (ImageView) addressDialog
				.findViewById(R.id.exit);
		close.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				alertDialog.cancel();

			}
		});

		((TextView) addressDialog.findViewById(R.id.addtitle)).setText(item
				.getTitle());
		((TextView) addressDialog.findViewById(R.id.addtitle))
				.setTypeface(fontFace);

		((TextView) addressDialog.findViewById(R.id.adddetails)).setText(item
				.getSnippet());
		((TextView) addressDialog.findViewById(R.id.adddetails))
				.setTypeface(fontFace);

		// alertDialog.getWindow().setBackgroundDrawable(new
		// ColorDrawable(android.graphics.Color.TRANSPARENT));
		// show it
		alertDialog.show();

		return true;
	}

	class Myalert extends AlertDialog {

		protected Myalert(Context context, int theme) {
			super(context, theme);
		}

		protected Myalert(Context context) {
			super(context);
		}

	}

	public void addOverlay(OverlayItem overlay) {
		mapOverlays.add(overlay);
		this.populate();
	}

}
