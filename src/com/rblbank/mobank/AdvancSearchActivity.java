package com.rblbank.mobank;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.rblbank.mobank.R;
import com.snapwork.ethreads.UserTask;
import com.snapwork.util.ProxyUrlUtil;

//import android.util.Log;

public class AdvancSearchActivity extends Activity implements OnClickListener {

	ImageView back, advSearchGO, imgBtnMenu;
	private TextView areaText, categoryText, subCategoryText, programText,
			cardTypeText, discountText;//
	private AutoCompleteTextView cCity;

	int currentCityPosition = 0;
	int last_currentCityPosition = -1;
	private ImageView cDown, areaDown, categoryDown, subCategoryDown,
			programDown, cardTypeDown, discountDown;
	private CheckBox emiCheckBox;
	Typeface fontFace;
	private Spinner citySpinner, areaSpinner, categorySpinner,
			subCategorySpinner, programSpinner, cardTypeSpinner,
			discountSpinner;
	private SeekBar mSeekbar;
	private LinearLayout subCategoryLayout;

	private Resources res;
	private String noNetwork = null;
	/**
	 * Massage when unable to conncet to the HDFC server.
	 */
	private String UNABLE_TO_REACH = null;
	/**
	 * Radius to search Offers in.
	 */
	private String curRadius = "10", isEMI = "";
	boolean fromAdvSearch = false;
	static ProgressDialog mPdialog = null;
	GetAllCities getAllCities;
	GetAllAreas getAllAreas;

	/**
	 * Location latitude of the City Entered by user from Offers Home or
	 * Selected from Adv. Search City List.
	 */
	private String latEnterCity = null;
	/**
	 * Location longitude of the City Entered by user from Offers Home or
	 * Selected from Adv. Search City List.
	 */
	private String lonEnterCity = null;
	/**
	 * List of all Cities.
	 */
	private ArrayList<String> cityArrayList = new ArrayList<String>();
	/**
	 * List of all Areas.
	 */
	private ArrayList<String> areaArrayList = new ArrayList<String>();
	/**
	 * List of all Categories.
	 */
	private ArrayList<String> categoryArrayList = new ArrayList<String>();
	/**
	 * List of all Sub Categories.
	 */
	private ArrayList<String> subCategoryArrayList = new ArrayList<String>();
	/**
	 * List of all Programs.
	 */
	private ArrayList<String> programArrayList = new ArrayList<String>();
	/**
	 * List of all Card Types.
	 */
	private ArrayList<String> cardTypeArrayList = new ArrayList<String>();
	/**
	 * List of all Discounts.
	 */
	private ArrayList<String> discountArrayList = new ArrayList<String>();

	/**
	 * Adapter that contains all Cities.
	 */
	DataAdapter cityDataAdapter, areaDataAdapter, categoryDataAdapter,
			subCategoryDataAdapter, programDataAdapter, cardTypeDataAdapter,
			discountDataAdapter;;
	/**
	 * WebService Url to get Data from.
	 */
	private String PostUrl = null;// UAT server

	/**
	 * WebService Url to get offers Data.
	 */
	private String OffersPostUrl = null;

	/**
	 * Max. radius in meters. Default is 10000M.(10KM)
	 */
	int selectedRadius = 10000;

	int selectProgress;
	/**
	 * InputMethodManager
	 */
	InputMethodManager imm;

	/**
	 * Bundle that hold the info params selected or entered by user to get
	 * offers respactively.
	 */
	Bundle b;

	private OnSeekBarChangeListener mclickone = new OnSeekBarChangeListener() {
		public void onProgressChanged(SeekBar seekBar, int progress,
				boolean fromUser) {
			selectProgress = progress;
			int radius = 100 + 99 * progress;
			selectedRadius = radius;
			curRadius = "" + selectedRadius / 1000;
			TextView tv = (TextView) findViewById(R.id.radius);
			String dis = getMToKm(radius);
			tv.setText("(" + dis + ")");

		}

		public void onStartTrackingTouch(SeekBar seekBar) {

		}

		public void onStopTrackingTouch(SeekBar seekBar) {

		}

	};

	private String getMToKm(int radius) {
		/*
		 * if (radius < 1000) return radius + " M"; else {
		 */
		int km = radius / 1000;
		int m = radius % 1000;
		String dis = km + " KM" + " " + m + " M";
		// if (m > 0)
		// dis += " " + m + " M";
		return dis;
		// }
	}

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// TO hide the 'SoftKeyboard'
		imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		if (imm.isAcceptingText()) {/*
									 * //"Software Keyboard was shown" if
									 * (this.getCurrentFocus
									 * ().getWindowToken()!=null)
									 * {//NullPointerException 8 ths LINE...
									 * imm.
									 * hideSoftInputFromWindow(getCurrentFocus
									 * ().getWindowToken(),
									 * InputMethodManager.HIDE_NOT_ALWAYS);
									 * 
									 * } //Hide Keyboard ...
									 * //this.getWindow().setSoftInputMode
									 * (WindowManager
									 * .LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
									 * );
									 */
		}
		mPdialog = new ProgressDialog(AdvancSearchActivity.this);
		mPdialog.setMessage("Please wait...");
		mPdialog.setIndeterminate(true);
		mPdialog.setCancelable(false);
		// mPdialog.show();

		res = getResources();
		noNetwork = res.getString(R.string.no_network);
		UNABLE_TO_REACH = res.getString(R.string.unable_to_connect);

		PostUrl = res.getString(R.string.ADV_SEARCH_MASTER_LIVE_URL);
		OffersPostUrl = res.getString(R.string.OFFERS_LIVE_URL);

		setContentView(R.layout.adv_search_layout);

		fontFace = Typeface.createFromAsset(getBaseContext().getAssets(),
				"fonts/hlr___.ttf");

		// gc... need to remove 24.03.14
		// subCategoryLayout=(LinearLayout)
		// findViewById(R.id.sub_category_layout);

		/*---NOTE---
		 * If need to remember the last selected radius then read value of 'radius' from 'SharedPreferences'
		 * Pref - Distance bar...
		 */
		// SharedPreferences sharedPreferences = getSharedPreferences("RADIUS",
		// MODE_WORLD_READABLE);
		// int radius = sharedPreferences.getInt("radius", 10000);
		// selectedRadius=radius;
		// Toast.makeText(AdvancSearchActivity.this,
		// selectedRadius+"M<-radius:"+selectedRadius/1000+"KM", 1).show();
		mSeekbar = (SeekBar) findViewById(R.id.seekbar);
		// mSeekbar.setProgress(100);
		mSeekbar.setProgress(selectedRadius / 100);// (radius/100);
		// set text of Km n M respactively... default is 10KM 0M
		// String dis = getMToKm(10000);
		String dis = getMToKm(selectedRadius);// (radius);
		((TextView) findViewById(R.id.radius)).setText("(" + dis + ")");

		mSeekbar.setOnSeekBarChangeListener(mclickone);
		// Adv-Search GO button
		advSearchGO = (ImageView) findViewById(R.id.img_btn_adv_search);
		advSearchGO.setOnClickListener(this);
		// On Back Button Press
		back = (ImageView) findViewById(R.id.backbutton);
		back.setOnClickListener(this);
		// Menu Button
		imgBtnMenu = (ImageView) findViewById(R.id.menu);
		imgBtnMenu.setOnClickListener(this);

		// EMI checkbox
		emiCheckBox = (CheckBox) findViewById(R.id.ck_box_emi);
		emiCheckBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub
				if (isChecked) {
					isEMI = "Yes";
				} else {
					// isEMI="No";
					isEMI = "";
				}
			}
		});

		// DownArrowImages
		cDown = (ImageView) findViewById(R.id.downCity);
		areaDown = (ImageView) findViewById(R.id.downArea);

		// gc... need to remove 24.03.14
		// categoryDown=(ImageView)findViewById(R.id.downCategory);
		// subCategoryDown=(ImageView)findViewById(R.id.downSubCategory);
		// programDown=(ImageView)findViewById(R.id.downProgram);
		// cardTypeDown=(ImageView)findViewById(R.id.downCardType);
		// discountDown=(ImageView)findViewById(R.id.downDiscount);

		cDown.setOnClickListener(this);
		areaDown.setOnClickListener(this);

		// gc... need to remove 24.03.14
		// categoryDown.setOnClickListener(this);
		// subCategoryDown.setOnClickListener(this);
		// programDown.setOnClickListener(this);
		// cardTypeDown.setOnClickListener(this);
		// discountDown.setOnClickListener(this);

		TextView cityText = (TextView) findViewById(R.id.text_view_city);
		String text = "City<font color='red'> *</font>";
		cityText.setText(Html.fromHtml(text), TextView.BufferType.SPANNABLE);

		// cCity=(TextView)findViewById(R.id.text_city);
		// cCity.setTypeface(fontFace);
		// cCity.setOnClickListener(this);
		cCity = (AutoCompleteTextView) findViewById(R.id.auto_text_city);
		cCity.setTypeface(fontFace);
		// cCity.setOnClickListener(this);

		areaText = (TextView) findViewById(R.id.text_area);
		areaText.setTypeface(fontFace);
		areaText.setOnClickListener(this);

		// gc... need to remove 24.03.14
		// categoryText=(TextView)findViewById(R.id.text_category);
		// categoryText.setTypeface(fontFace);
		// categoryText.setOnClickListener(this);
		//
		// subCategoryText=(TextView)findViewById(R.id.text_sub_category);
		// subCategoryText.setTypeface(fontFace);
		// subCategoryText.setOnClickListener(this);
		//
		// programText=(TextView)findViewById(R.id.text_program_name);
		// programText.setTypeface(fontFace);
		// programText.setOnClickListener(this);
		//
		// cardTypeText=(TextView)findViewById(R.id.text_card_type);
		// cardTypeText.setTypeface(fontFace);
		// cardTypeText.setOnClickListener(this);
		//
		// discountText=(TextView)findViewById(R.id.text_discount);
		// discountText.setTypeface(fontFace);
		// discountText.setOnClickListener(this);

		// City Text & Spinner
		// call WS to get all 'Cities'
		if (ProxyUrlUtil.isNetworkAvailable(AdvancSearchActivity.this)) {
			// do...
			getAllCities = (GetAllCities) new GetAllCities().execute();
		} else {
			Toast.makeText(AdvancSearchActivity.this, noNetwork, 1).show();

		}

		// getAllCities.cancel(true);//when need to cancel onPause or onStop!!!
		citySpinner = (Spinner) findViewById(R.id.spinner_city);
		// ArrayAdapter<CharSequence> adapter =
		// ArrayAdapter.createFromResource(this, R.array.City,
		// R.layout.my_spinner_text);
		// cityspinner.setAdapter(adapter);
		cityDataAdapter = new DataAdapter(AdvancSearchActivity.this,
				R.layout.my_spinner_text, cityArrayList);
		citySpinner.setAdapter(cityDataAdapter);
		citySpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int pos, long id) {
				// currentCityPosition=pos;
				cCity.setText(parent.getItemAtPosition(pos).toString());

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {

			}
		});

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				R.layout.my_spinner_text, cityArrayList);
		cCity.setAdapter(adapter);
		// cCity.setAdapter(cityDataAdapter);

		// To attach List View Of Cities... gc...
		final ListView cityListView = (ListView) findViewById(R.id.city_list);
		cityListView.setAdapter(new DataAdapter(AdvancSearchActivity.this,
				R.layout.my_spinner_text, cityArrayList));

		// if (!cCity.isFocused()) {
		// if (cityListView.isShown()) {
		// cityListView.setVisibility(View.GONE);
		// } else {
		//
		// }
		// } else {
		//
		// }
		// cCity.setOnFocusChangeListener(new OnFocusChangeListener() {
		//
		// @Override
		// public void onFocusChange(View v, boolean hasFocus) {
		// // TODO Auto-generated method stub
		// if (hasFocus) {
		// /// performFiltering("", 0);
		// cCity.showDropDown();
		// if (!cityListView.isShown()) {
		// cityListView.setVisibility(View.VISIBLE);.
		// } else {
		//
		// }
		// }
		// }
		// });

		/*
		 * cCity.setOnKeyListener(new OnKeyListener() {
		 * 
		 * @Override public boolean onKey(View v, int keyCode, KeyEvent event) {
		 * if (keyCode == EditorInfo.IME_ACTION_DONE) { // do your stuff here
		 * cCity.clearFocus();
		 * imm.hideSoftInputFromWindow(cCity.getWindowToken(), 0); return true;
		 * } return false; } });
		 */

		cCity.setOnEditorActionListener(new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_DONE) {
					// do your stuff here
					imm.hideSoftInputFromWindow(cCity.getWindowToken(), 0);
					return true;
				}
				return false;
			}
		});

		cCity.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				cCity.showDropDown();
				return false;
			}
		});
		cCity.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				// "Software Keyboard was shown" gc...
				imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
						InputMethodManager.RESULT_HIDDEN);
				// InputMethodManager imm =
				// (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
				// imm.hideSoftInputFromWindow(cCity.getWindowToken(), 0);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});

		cCity.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence cs, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				// When user changed the Text
				AdvancSearchActivity.this.cityDataAdapter.getFilter()
						.filter(cs);
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});

		// Area Text & Spinner
		areaSpinner = (Spinner) findViewById(R.id.spinner_area);
		// ArrayAdapter<CharSequence> adapter =
		// ArrayAdapter.createFromResource(this, R.array.City,
		// R.layout.my_spinner_text);
		// cityspinner.setAdapter(adapter);
		areaDataAdapter = new DataAdapter(AdvancSearchActivity.this,
				R.layout.my_spinner_text, areaArrayList);
		areaSpinner.setAdapter(areaDataAdapter);
		areaSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int pos, long id) {
				if (pos != 0) {
					areaText.setText(parent.getItemAtPosition(pos).toString());
				} else {
					// Do nothing...
					areaText.setText("");

				}

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {

			}

		});
		// getAllAreas = (GetAllAreas) new GetAllAreas().execute();

		// gc... need to remove 24.03.14
		// //Category Spinner
		// categorySpinner=(Spinner)findViewById(R.id.spinner_category);
		// //ArrayAdapter<CharSequence> adapter =
		// ArrayAdapter.createFromResource(this, R.array.City,
		// R.layout.my_spinner_text);
		// //cityspinner.setAdapter(adapter);
		// categoryDataAdapter = new DataAdapter(AdvancSearchActivity.this,
		// R.layout.my_spinner_text, categoryArrayList);
		// categorySpinner.setAdapter(categoryDataAdapter);
		// categorySpinner.setOnItemSelectedListener(new
		// OnItemSelectedListener() {
		//
		// @Override
		// public void onItemSelected(AdapterView<?> parent, View view, int pos,
		// long id) {
		// Log.v("Spinn", "Position"+pos);
		// if (pos!=0) {
		// categoryText.setText(parent.getItemAtPosition(pos).toString());
		// } else {
		// //Do nothing...
		// categoryText.setText("");
		//
		// }
		//
		// if
		// (parent.getItemAtPosition(pos).toString().equalsIgnoreCase("Restaurant"))
		// {
		// subCategoryLayout.setVisibility(View.VISIBLE);
		// } else {
		// if (subCategoryLayout.getVisibility()==View.VISIBLE) {
		// subCategoryLayout.setVisibility(View.GONE);
		// }
		// }
		//
		// }
		//
		// @Override
		// public void onNothingSelected(AdapterView<?> arg0) {
		//
		// }
		// });
		//
		// //Sub Category Spinner
		// subCategorySpinner=(Spinner)findViewById(R.id.spinner_sub_category);
		// //ArrayAdapter<CharSequence> adapter =
		// ArrayAdapter.createFromResource(this, R.array.City,
		// R.layout.my_spinner_text);
		// //cityspinner.setAdapter(adapter);
		// subCategoryDataAdapter = new DataAdapter(AdvancSearchActivity.this,
		// R.layout.my_spinner_text, subCategoryArrayList);
		// subCategorySpinner.setAdapter(subCategoryDataAdapter);
		// subCategorySpinner.setOnItemSelectedListener(new
		// OnItemSelectedListener() {
		//
		// @Override
		// public void onItemSelected(AdapterView<?> parent, View view, int pos,
		// long id) {
		// Log.v("Spinn", "Position"+pos);
		// if (pos!=0) {
		// subCategoryText.setText(parent.getItemAtPosition(pos).toString());
		// } else {
		// //Do nothing...
		// subCategoryText.setText("");
		//
		// }
		//
		// }
		//
		// @Override
		// public void onNothingSelected(AdapterView<?> arg0) {
		//
		// }
		// });
		//
		// //Programs Spinner
		// programSpinner=(Spinner)findViewById(R.id.spinner_program_nm);
		// //ArrayAdapter<CharSequence> adapter =
		// ArrayAdapter.createFromResource(this, R.array.City,
		// R.layout.my_spinner_text);
		// //cityspinner.setAdapter(adapter);
		// programDataAdapter= new DataAdapter(AdvancSearchActivity.this,
		// R.layout.my_spinner_text, programArrayList);
		// programSpinner.setAdapter(programDataAdapter);
		// programSpinner.setOnItemSelectedListener(new OnItemSelectedListener()
		// {
		//
		// @Override
		// public void onItemSelected(AdapterView<?> parent, View view, int pos,
		// long id) {
		// Log.v("Spinn", "Position"+pos);
		// if (pos!=0) {
		// programText.setText(parent.getItemAtPosition(pos).toString());
		// } else {
		// //Do nothing...
		// programText.setText("");
		//
		// }
		//
		// }
		//
		// @Override
		// public void onNothingSelected(AdapterView<?> arg0) {
		//
		// }
		// });
		//
		// //Card Type Spinner
		// cardTypeSpinner=(Spinner)findViewById(R.id.spinner_card_type);
		// //ArrayAdapter<CharSequence> adapter =
		// ArrayAdapter.createFromResource(this, R.array.City,
		// R.layout.my_spinner_text);
		// //cityspinner.setAdapter(adapter);
		// cardTypeDataAdapter= new DataAdapter(AdvancSearchActivity.this,
		// R.layout.my_spinner_text, cardTypeArrayList);
		// cardTypeSpinner.setAdapter(cardTypeDataAdapter);
		// cardTypeSpinner.setOnItemSelectedListener(new
		// OnItemSelectedListener() {
		//
		// @Override
		// public void onItemSelected(AdapterView<?> parent, View view, int pos,
		// long id) {
		// Log.v("Spinn", "Position"+pos);
		// if (pos!=0) {
		// cardTypeText.setText(parent.getItemAtPosition(pos).toString());
		// } else {
		// //Do nothing...
		// cardTypeText.setText("");
		//
		// }
		//
		// }
		//
		// @Override
		// public void onNothingSelected(AdapterView<?> arg0) {
		//
		// }
		// });
		//
		// //Card Type Spinner
		// discountSpinner=(Spinner)findViewById(R.id.spinner_discount);
		// //ArrayAdapter<CharSequence> adapter =
		// ArrayAdapter.createFromResource(this, R.array.City,
		// R.layout.my_spinner_text);
		// //cityspinner.setAdapter(adapter);
		// discountDataAdapter= new DataAdapter(AdvancSearchActivity.this,
		// R.layout.my_spinner_text, discountArrayList);
		// discountSpinner.setAdapter(discountDataAdapter);
		// discountSpinner.setOnItemSelectedListener(new
		// OnItemSelectedListener() {
		//
		// @Override
		// public void onItemSelected(AdapterView<?> parent, View view, int pos,
		// long id) {
		// Log.v("Spinn", "Position"+pos);
		// if (pos!=0) {
		// discountText.setText(parent.getItemAtPosition(pos).toString());
		// } else {
		// //Do nothing...
		// discountText.setText("");
		//
		// }
		//
		// }
		//
		// @Override
		// public void onNothingSelected(AdapterView<?> arg0) {
		//
		// }
		// });

	}

	/*
	 * @Override public boolean onKeyDown(int keyCode, KeyEvent event) { if
	 * (keyCode == EditorInfo.IME_ACTION_DONE) { // do your stuff here
	 * Toast.makeText(AdvancSearchActivity.this, "IF-onKeyDown", 0).show();
	 * cCity.clearFocus();// imm.isAcceptingText();
	 * imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
	 * return true; } else { Toast.makeText(AdvancSearchActivity.this,
	 * "ELSE-onKeyDown", 0) .show(); } return false; }
	 */

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		if (mPdialog != null) {
			if (mPdialog.isShowing()) {
				mPdialog.dismiss();
			}
		}

		if (getAllCities != null) {
			getAllCities.cancel(true);
		}
		if (getAllAreas != null) {
			getAllAreas.cancel(true);
		}

	}

	private void setAllAdvSearchFields() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		int whichField = 0;
		if (v.getId() == R.id.downCity || v.getId() == R.id.text_city) {
			whichField = 1;
		} else if (v.getId() == R.id.downArea || v.getId() == R.id.text_area) {
			whichField = 2;
		}
		// gc... need to remove 24.03.14
		// else if (v.getId()==R.id.downCategory||v.getId()==R.id.text_category)
		// {
		// whichField=3;
		// } else if
		// (v.getId()==R.id.downSubCategory||v.getId()==R.id.text_sub_category)
		// {
		// whichField=4;
		// } else if
		// (v.getId()==R.id.downProgram||v.getId()==R.id.text_program_name) {
		// whichField=5;
		// } else if
		// (v.getId()==R.id.downCardType||v.getId()==R.id.text_card_type) {
		// whichField=6;
		// } else if
		// (v.getId()==R.id.downDiscount||v.getId()==R.id.text_discount) {
		// whichField=7;
		// }

		switch (whichField) {
		case 1:
			if (ProxyUrlUtil.isNetworkAvailable(AdvancSearchActivity.this)) {
				// do...
				if (cityArrayList.size() != 0) {
					// do...
					citySpinner.performClick();
					// showCityList();
				} else {
					// call WS to get all 'Cities'
					getAllCities = (GetAllCities) new GetAllCities().execute();
				}

			} else {
				Toast.makeText(AdvancSearchActivity.this, noNetwork, 1).show();

			}

			break;
		case 2:
			if (cCity.getText().length() != 0) {
				if (ProxyUrlUtil.isNetworkAvailable(AdvancSearchActivity.this)) {
					// do...
					// if (areaArrayList.size()!=0) { // Showing PB
					// correctly.... In this Carse WHY?.
					// if (last_currentCityPosition==currentCityPosition) {
					// //do...
					// areaSpinner.performClick();
					// } else {
					//
					// last_currentCityPosition = currentCityPosition;

					// call WS to get all 'Area'
					getAllAreas = (GetAllAreas) new GetAllAreas().execute(""
							+ cCity.getText().toString());
					// Area ofCurrently Selected City
					// getAllAreas = (GetAllAreas) new
					// GetAllAreas().execute(cCity.getText().toString());
					// }

				} else {
					Toast.makeText(AdvancSearchActivity.this, noNetwork, 1)
							.show();

				}

			} else {
				Toast.makeText(AdvancSearchActivity.this,
						"Please enter City first!", 1).show();
			}

			break;
		// gc.. ned to remove 24.03.14
		// case 3:
		// if (ProxyUrlUtil.isNetworkAvailable(AdvancSearchActivity.this)) {
		// //do...
		// if (categoryArrayList.size()!=0) {
		// categorySpinner.performClick();
		// } else {
		// //call WS to get all 'Categories'
		// getAllCategories=(GetAllCategories) new GetAllCategories().execute();
		// }
		// } else {
		// Toast.makeText(AdvancSearchActivity.this, noNetwork, 1).show();
		//
		// }
		//
		// break;
		// case 4:
		// if (ProxyUrlUtil.isNetworkAvailable(AdvancSearchActivity.this)) {
		// //do...
		// if (subCategoryArrayList.size()!=0) {
		// subCategorySpinner.performClick();
		// } else {
		// //call WS to get all 'SubCategories'
		// getAllSubCategories=(GetAllSubCategories) new
		// GetAllSubCategories().execute();
		//
		// }
		// } else {
		// Toast.makeText(AdvancSearchActivity.this, noNetwork, 1).show();
		//
		// }
		//
		// break;
		// case 5:
		// if (ProxyUrlUtil.isNetworkAvailable(AdvancSearchActivity.this)) {
		// //do...
		// if (programArrayList.size()!=0) {
		// programSpinner.performClick();
		// } else {
		// //call WS to get all 'Programs'
		// getAllPrograms=(GetAllPrograms) new GetAllPrograms().execute();
		//
		// }
		// } else {
		// Toast.makeText(AdvancSearchActivity.this, noNetwork, 1).show();
		//
		// }
		//
		// break;
		// case 6:
		// if (ProxyUrlUtil.isNetworkAvailable(AdvancSearchActivity.this)) {
		// //do...
		// if (cardTypeArrayList.size()!=0) {
		// cardTypeSpinner.performClick();
		// } else {
		// //call WS to get all 'CardTypes'
		// getAllCardTypes=(GetAllCardTypes) new GetAllCardTypes().execute();
		//
		// }
		// } else {
		// Toast.makeText(AdvancSearchActivity.this, noNetwork, 1).show();
		//
		// }
		//
		// break;
		// case 7:
		// if (ProxyUrlUtil.isNetworkAvailable(AdvancSearchActivity.this)) {
		// //do...
		// if (discountArrayList.size()!=0) {
		// discountSpinner.performClick();
		// } else {
		// //call WS to get all 'Discounts'
		// getAllDiscounts=(GetAllDiscounts) new GetAllDiscounts().execute();
		//
		// }
		// } else {
		// Toast.makeText(AdvancSearchActivity.this, noNetwork, 1).show();
		//
		// }
		//
		// break;

		default:
			break;
		}
		Intent intent;
		switch (v.getId()) {
		case R.id.backbutton:
			AdvancSearchActivity.this.finish();
			// Intent intent=new Intent(AdvancSearchActivity.this,
			// ShowOffersActivity.class);
			// startActivity(intent);
			break;
		case R.id.menu:
			// intent = new Intent(AdvancSearchActivity.this,
			// OffersActivityNew.class);
			intent = new Intent(AdvancSearchActivity.this, RBL_iBank.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			// intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
			// Bundle bundle = new Bundle();
			// bundle.putString("requestid", "smdashboardlist");
			// intent.putExtras(bundle);
			finish();
			startActivity(intent);
			break;
		case R.id.img_btn_adv_search:

			if (ProxyUrlUtil.isNetworkAvailable(AdvancSearchActivity.this)) {

				if (cCity.getText().toString().length() != 0) {

					fromAdvSearch = true;
					/*
					 * SharedPreferences.Editor editor1 = settings.edit();
					 * editor1.putString("city",
					 * searchText.getText().toString());
					 * editor1.putInt("radius", selectedRadius);
					 * editor1.putInt("selectProgress",selectProgress);
					 * editor1.commit();
					 */

					// update radius in to Preferences...
					SharedPreferences sharedPreferences = getSharedPreferences(
							"RADIUS", MODE_WORLD_READABLE);
					Editor editor = sharedPreferences.edit();
					editor.putInt("radius", selectedRadius);
					editor.commit();
					// Toast.makeText(AdvancSearchActivity.this,
					// "radius:"+selectedRadius/1000, 1).show();.

					// if(selectedRadius==0)
					// {
					// selectedRadius=10;
					// } else {
					// selectedRadius=selectedRadius/1000;
					// Toast.makeText(AdvancSearchActivity.this,
					// selectedRadius+"-radius:"+selectedRadius/1000, 1).show();
					// }
					// curRadius=String.valueOf(selectedRadius);

					boolean showMap = false;// gc...

					// Log.i("manFlag",""+manFlag);
					// if(manFlag)
					// {
					// if(searchText.getText().toString().length()<2)
					// {
					// final AlertDialog alertDialog = new
					// AlertDialog.Builder(AdvancSearchActivity.this).create();
					// alertDialog.setTitle("Alert");
					// alertDialog.setMessage("Please Enter Location");
					// alertDialog.setButton("OK",new
					// DialogInterface.OnClickListener() {
					//
					// public void onClick(DialogInterface dialog,int which) {
					// alertDialog.dismiss();
					// }
					// });
					// alertDialog.show();
					// searchText.clearFocus();
					// return;
					// }
					// }
					// Log.d("lat n lon=>",""+lat+"==>"+lon);
					// cCity, areaText, categoryText, subCategoryText,
					// programText, cardTypeText, discountText
					b = new Bundle();
					String city = cCity.getText().toString();
					String area = areaText.getText().toString();

					// gc... need to remove 24.03.14
					// String category = categoryText.getText().toString();
					// String subCategory =
					// subCategoryText.getText().toString();
					// String program = programText.getText().toString();
					// String cardType = cardTypeText.getText().toString();
					// String discount = discountText.getText().toString();

					b.putBoolean("advSearch", fromAdvSearch);

					if (city.length() != 0) {
						b.putString("city", city);

					} else {
						b.putString("city", "");
					}

					if (area.length() != 0
							&& !area.equalsIgnoreCase("Select Area")) {
						b.putString("area", area);

					} else {
						b.putString("area", "");
					}

					// gc... need to remove 24.03.14
					// if (category.length()!=0 &&
					// !category.equalsIgnoreCase("Select Category")) {
					// b.putString("category", category);
					// Log.i("category", "category:"+category);
					// } else {
					// b.putString("category", "");
					// //Toast.makeText(AdvancSearchActivity.this,
					// "No Category selected - none!", 1).show();
					//
					// }
					//
					// if (subCategory.length()!=0 &&
					// !subCategory.equalsIgnoreCase("Select Sub Category")) {
					// b.putString("subCategory", subCategory);
					// Log.i("subCategory", "subCategory:"+subCategory);
					//
					// } else {
					// b.putString("subCategory", "");
					// }
					//
					// if (program.length()!=0 &&
					// !program.equalsIgnoreCase("Select Program Name")) {
					// b.putString("program", program);
					// Log.i("program", "program:"+program);
					//
					// } else {
					// b.putString("program", "");
					// }
					//
					// if (cardType.length()!=0 &&
					// !cardType.equalsIgnoreCase("Select Card Type")) {
					// b.putString("cardType", cardType);
					// Log.i("cardType", "cardType:"+cardType);
					// } else {
					// b.putString("cardType", "");
					// }
					//
					// if (discount.length()!=0 &&
					// !discount.equalsIgnoreCase("Select Discount %")) {
					// b.putString("discount", discount);
					// Log.i("discount", "discount:"+discount);
					// } else {
					// b.putString("discount", "");
					// }

					if (isEMI.length() != 0) {
						b.putString("emi", isEMI);
					} else {
						b.putString("emi", "");
					}

					b.putString("radius", "" + curRadius);

					// gc... now i work it in 'GetOffers'
					// intent=new
					// Intent(AdvancSearchActivity.this,ShowOffersActivity.class);
					// //to clear all top activities on it
					// intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					// //to start the same Activity if its already running
					// intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
					// intent.putExtras(b);
					// AdvancSearchActivity.this.finish();
					// startActivity(intent);

					// new GetOffersData().execute("19.56843", "73.218902",
					// "");.
					new GetCityOffersData().execute(city);

				} else {
					Toast.makeText(AdvancSearchActivity.this,
							"Please enter City first!", 1).show();

				}
			} else {
				Toast.makeText(AdvancSearchActivity.this, noNetwork, 1).show();

			}

			break;

		default:
			break;
		}
	}

	// gc...
	public class MyOnItemSelectedListener implements OnItemSelectedListener {
		@Override
		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			switch (view.getId()) {
			case R.id.spinner_city:
				cCity.setText(parent.getItemAtPosition(pos).toString());
				// Get all Areas of Selected City.
				getAllAreas = (GetAllAreas) new GetAllAreas().execute();
				break;
			case R.id.spinner_area:
				Toast.makeText(AdvancSearchActivity.this,
						"spinner_area Clicked...", 0).show();
				break;
			default:
				break;
			}
		}

		@Override
		public void onNothingSelected(AdapterView<?> parent) {
			// TODO Auto-generated method stub
		}
	}

	static class FilterViewHolder {
		TextView cCityTextView;

	}

	/**
	 * Adapter that hold data of all 'Area Data'.
	 * 
	 * @author Rupesh
	 * 
	 */
	public class DataAdapter extends ArrayAdapter<String> {
		private List<String> cityList;
		private Activity context;

		public DataAdapter(Context context, int textViewResourceId,
				List<String> objects) {
			super(context, textViewResourceId, objects);
			// TODO Auto-generated constructor stub
			// this.context=context;
			this.cityList = objects;
		}

		public DataAdapter(Activity context, List<String> cityList) {
			super(AdvancSearchActivity.this, R.layout.my_spinner_text, cityList);
			this.context = context;
			this.cityList = cityList;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			View row = convertView;
			final FilterViewHolder holder;
			final int clickPos = position;
			if (row == null) {
				LayoutInflater inflater = getLayoutInflater();

				row = inflater.inflate(R.layout.my_spinner_text, parent, false);
				holder = new FilterViewHolder();
				holder.cCityTextView = (TextView) row
						.findViewById(R.id.spinnerText);
				// holder.cCityTextView.setTypeface(fontFace);

				row.setTag(holder);
			} else {
				// holder = (View) row;
				holder = (FilterViewHolder) row.getTag();
			}
			// Log.v("Item In Area List-", ""+areaList.get(position));
			holder.cCityTextView.setText(cityList.get(position));

			// Test to click... may be need to remove or not... gc...
			/*
			 * holder.cCityTextView.setOnClickListener(new OnClickListener() {
			 * 
			 * @Override public void onClick(View v) { // TODO Auto-generated
			 * method stub
			 * cCity.setText(holder.cCityTextView.getText().toString()); } });
			 */

			return row;

		}

	}

	/**
	 * Show Intermediate Progress.
	 * 
	 * @param context
	 *            context of the Activity where to show progress.
	 * @param msg
	 *            message to show.
	 * @param isCancelable
	 *            progress is cancelable(true) or not(false)
	 */
	public static void showProgress(Context context, String msg,
			boolean isCancelable) {
		// TODO Auto-generated method stub
		// if (!mPdialog.isShowing()) {
		mPdialog = new ProgressDialog(context);
		mPdialog.setMessage(msg);// ("Please wait...");
		mPdialog.setIndeterminate(true);
		mPdialog.setCancelable(isCancelable);
		mPdialog.show();
		// }

	}

	/**
	 * Remove the intermediate Progress Bar from the Screen.
	 */
	public void removeProgressBar() {
		if (mPdialog != null && mPdialog.isShowing()) {
			mPdialog.dismiss();
		}
	}

	/**
	 * My custom ALert Dialog!
	 * 
	 * @author Rupesh
	 * 
	 */
	class Myalert extends AlertDialog {

		protected Myalert(Context context, int theme) {
			super(context, theme);
		}

		protected Myalert(Context context) {
			super(context);
		}

	}

	/**
	 * Return all Cities.
	 * 
	 * @author Rupesh
	 * 
	 */
	private class GetAllCities extends UserTask<String, Void, String> {
		public void onPreExecute() {

			// showProgress(AdvancSearchActivity.this, "Please wait...", false);
			if (mPdialog.isShowing()) {
				// remove already showing PB
				mPdialog.dismiss();
				// show PB again
				mPdialog.show();
			} else {
				mPdialog.show();
			}
		}

		public void onPostExecute(String data) {
			String lat = null, lon = null;

			// removeProgressBar();
			if (mPdialog != null) {
				if (mPdialog.isShowing())
					mPdialog.dismiss();
			}
			/*
			 * Imp. Note- Here you are gatting below res. when no data founds_
			 * so keep this in mind! { "output": "No Records Found" }
			 */
			if (data != null && data.contains("output")) {
				try {
					String msg = new JSONObject(data).getString("output");
					LayoutInflater infalter = LayoutInflater
							.from(AdvancSearchActivity.this);
					View addressDialog = infalter.inflate(
							R.layout.addressdialog, null);

					// final AlertDialog.Builder alert=new
					// AlertDialog.Builder(ShowOffersActivity.this);
					final Myalert alert = new Myalert(AdvancSearchActivity.this);
					alert.setInverseBackgroundForced(true);
					alert.setView(addressDialog, 0, 0, 0, 0);
					alert.setIcon(android.R.drawable.btn_default);
					alert.show();

					final ImageView close = (ImageView) addressDialog
							.findViewById(R.id.exit);
					close.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View arg0) {
							alert.cancel();

						}
					});

					((TextView) addressDialog.findViewById(R.id.addtitle))
							.setText("No Data!");
					((TextView) addressDialog.findViewById(R.id.adddetails))
							.setText(msg);

					// show it
					alert.show();

					return;
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} else {
				/*
				 * If new data comes means then only you have to clear all older
				 * 'cuisines' & 'area' Bcoz, now we build new lists of C & A.
				 */
				// areaArrayList.clear();
				// cuisineArrayList.clear();

			}
			if (data == null || data.length() < 20) {
				// Toast.makeText(AdvancSearchActivity.this, "No City found!",
				// 1).show();
				Toast.makeText(AdvancSearchActivity.this, noNetwork, 1).show();
				return;
			}
			if (cityArrayList != null) {
				cityArrayList.clear();
			}
			try {
				JSONObject jObj = null;
				JSONArray cityJArr = null;
				jObj = new JSONObject(data);

				// JSONObject jFiltersObj = jObj.getJSONObject("filters");
				cityJArr = jObj.getJSONArray("dropdown");
				System.out.println("cityJArr Length:" + cityJArr.length());
				if (cityJArr.length() != 0) {
					System.out
							.println("cityJArr LengthIN:" + cityJArr.length());
					for (int i = 0; i < cityJArr.length(); i++) {
						// System.out.println(i + "th cityJArr Item:"+
						// cityJArr.getString(i));
						if (!cityJArr.getString(i).equalsIgnoreCase("-")
								|| !cityJArr.getString(i).equalsIgnoreCase("")) {
							cityArrayList.add(cityJArr.getString(i));
						}

					}

					cityDataAdapter.notifyDataSetChanged();
				}

			} catch (Exception e) {
				e.printStackTrace();
				// Log.e("JSON Parser", "Error parsing data " + e.toString());
			} finally {
				// Come here, means we have received 'SubCategories' data. Now,
				// Show spinner of subCate. ...
				if (cityArrayList.size() != 0) {
					// citySpinner.performClick();
					// showCityList();//dialog having city list that enable
					// filtering functionality...
				} else {
					// do ...?

				}

			}

		}

		public String doInBackground(String... params) {
			String response = null;
			// String x = params[0];

			// Log.d("address=>",""+x+"==="+y+"=="+selectedRadius);
			try {

				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
						4);
				nameValuePairs.add(new BasicNameValuePair("master", "City"));

				// System.out.println("GetOffersData.doInBackground(). Lat:" +
				// x+ ", Long:" + y);
				ProxyUrlUtil pU = new ProxyUrlUtil();

				// response =
				// pU.getPostXML(PostUrl,ShowOffersActivity.this,nameValuePairs);
				// response =
				// ProxyUrlUtil.getOffers("http://122.180.94.253:8080/hdfcapi/poi.do",
				// nameValuePairs);
				response = ProxyUrlUtil.getData(PostUrl, nameValuePairs);
				// Log.d("Response of offers=>", "" + response);

			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			}

			return response;
		}
	}

	/**
	 * Dialog message shown when no area found.
	 */
	private void noAreaFound() {
		// TODO Auto-generated method stub
		LayoutInflater infalter = LayoutInflater
				.from(AdvancSearchActivity.this);
		View addressDialog = infalter.inflate(R.layout.addressdialog, null);

		// final AlertDialog.Builder alert=new
		// AlertDialog.Builder(ShowOffersActivity.this);
		final Myalert alert = new Myalert(AdvancSearchActivity.this);
		alert.setInverseBackgroundForced(true);
		alert.setView(addressDialog, 0, 0, 0, 0);
		alert.setCancelable(false);
		alert.setIcon(android.R.drawable.btn_default);
		alert.show();

		final ImageView close = (ImageView) addressDialog
				.findViewById(R.id.exit);
		close.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				alert.cancel();

			}
		});

		((TextView) addressDialog.findViewById(R.id.addtitle))
				.setText("No Area!");
		((TextView) addressDialog.findViewById(R.id.adddetails))
				.setText("Area not found around " + cCity.getText().toString()
						+ ". Please, Select other City!");

		areaText.setText("");

		// show it
		alert.show();
	}

	/**
	 * Return all Areas.
	 * 
	 * @author Rupesh
	 * 
	 */
	private class GetAllAreas extends UserTask<String, Void, String> {
		// ProgressDialog mPdialog =null;
		public void onPreExecute() {
			// showProgress(AdvancSearchActivity.this, "Please wait...", false);
			if (mPdialog.isShowing()) {
				// remove already showing PB
				mPdialog.dismiss();
				// show PB again
				mPdialog.show();
			} else {
				mPdialog.show();
			}
		}

		public void onPostExecute(String data) {
			String lat = null, lon = null;

			// removeProgressBar();
			if (mPdialog != null) {
				if (mPdialog.isShowing())
					mPdialog.dismiss();
			}
			/*
			 * Imp. Note- Here you are gatting below res. when no data founds_
			 * so keep this in mind! { "output": "No Records Found" }
			 */
			if (data != null && data.contains("output")) {
				try {
					String msg = new JSONObject(data).getString("output");

					noAreaFound();

					return;
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} else {
				/*
				 * If new data comes means then only you have to clear all older
				 * 'cuisines' & 'area' Bcoz, now we build new lists of C & A.
				 */
				// areaArrayList.clear();
				// cuisineArrayList.clear();

			}
			if (data == null || data.length() < 2) {
				// noffers.setText("Sorry there are no offers in your area currently. We are adding exciting offers everyday. Do come back and check later.");
				// Toast.makeText(AdvancSearchActivity.this, "No Area fond!",
				// 1).show();
				Toast.makeText(AdvancSearchActivity.this, noNetwork, 1).show();
				return;
			}
			if (areaArrayList != null) {
				areaArrayList.clear();
			}
			try {
				JSONObject jObj = null;
				JSONArray areaJArr = null;
				jObj = new JSONObject(data);

				areaJArr = jObj.getJSONArray("dropdown");
				System.out.println("areaJArr Length:" + areaJArr.length());
				if (areaJArr.length() != 0
						&& !areaJArr.getString(0).equalsIgnoreCase("-")) {
					System.out
							.println("areaJArr LengthIN:" + areaJArr.length());
					for (int i = 0; i < areaJArr.length(); i++) {
						// System.out.println(i + "th areaJArr Item:"+
						// areaJArr.getString(i));
						if (!areaJArr.getString(i).equalsIgnoreCase("-")
								&& !(areaJArr.getString(i).equalsIgnoreCase("") || areaJArr
										.getString(i).equalsIgnoreCase(" ")))
							areaArrayList.add(areaJArr.getString(i));

					}
					if (areaArrayList.size() != 0) {
						areaArrayList.add(0, "Select Area");
					} else {
						noAreaFound();
					}
					areaDataAdapter.notifyDataSetChanged();
				}

			} catch (Exception e) {
				e.printStackTrace();
				// Log.e("JSON Parser", "Error parsing data " + e.toString());
			} finally {
				// Come here, means we have received 'SubCategories' data. Now,
				// Show spinner of subCate. ...
				if (areaArrayList.size() != 0) {
					areaSpinner.performClick();

				} else {
					// do ...?

				}

			}

		}

		public String doInBackground(String... params) {
			String response = null;
			String city = params[0];

			try {
				// filterOffersByThisCuisineList.toString().substring(1,
				// filterOffersByThisCuisineList.toString().length() -
				// 1).replace(", ", ",");

				// String PostUrl =
				// "http://122.180.94.253:8080/hdfcapi/masters.do";
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
						4);
				// master=Area&City=Dehradun

				nameValuePairs.add(new BasicNameValuePair("master", "Area"));
				nameValuePairs.add(new BasicNameValuePair("City", city));

				// System.out.println("GetOffersData.doInBackground(). Lat:" +
				// x+ ", Long:" + y);
				ProxyUrlUtil pU = new ProxyUrlUtil();

				// response =
				// pU.getPostXML(PostUrl,ShowOffersActivity.this,nameValuePairs);
				// response =
				// ProxyUrlUtil.getOffers("http://122.180.94.253:8080/hdfcapi/poi.do",
				// nameValuePairs);
				response = ProxyUrlUtil.getData(PostUrl, nameValuePairs);
				// Log.d("Response of offers=>", "" + response);

			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			}

			return response;
		}
	}

	/**
	 * Return all Offerers data.
	 * 
	 * @author Rupesh
	 * 
	 */
	private class GetOffersData extends UserTask<String, Void, String> {
		ProgressDialog mPdialog;

		public void onPreExecute() {
			try {
				if (mPdialog == null)
					mPdialog = new ProgressDialog(AdvancSearchActivity.this);

				// if(offersFlag)
				// {
				mPdialog.setMessage("Getting Offers...");
				// }else{
				// mPdialog.setMessage("Getting Map View...");
				// }

				mPdialog.setIndeterminate(false);
				mPdialog.setCancelable(false);
				mPdialog.show();

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		public void onPostExecute(String data) {

			String lat = null, lon = null;
			if (mPdialog != null) {
				/*
				 * if(mPdialog.isShowing()) mPdialog.dismiss();
				 */
				try {
					mPdialog.dismiss();
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
			/*
			 * Imp. Note- Here you are gatting below res. when no data founds_
			 * so keep this in mind! { "output": "No Records Found" }
			 */
			if (data != null && data.contains("output")) {
				try {

					String msg = new JSONObject(data).getString("output");
					LayoutInflater infalter = LayoutInflater
							.from(AdvancSearchActivity.this);
					View addressDialog = infalter.inflate(
							R.layout.addressdialog, null);

					// final AlertDialog.Builder alert=new
					// AlertDialog.Builder(ShowOffersActivity.this);
					final Myalert alert = new Myalert(AdvancSearchActivity.this);
					alert.setInverseBackgroundForced(true);
					alert.setCancelable(false);
					alert.setView(addressDialog, 0, 0, 0, 0);
					alert.setIcon(android.R.drawable.btn_default);

					final ImageView close = (ImageView) addressDialog
							.findViewById(R.id.exit);
					close.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View arg0) {
							alert.cancel();

						}
					});

					((TextView) addressDialog.findViewById(R.id.addtitle))
							.setText("No Offers!");

					((TextView) addressDialog.findViewById(R.id.adddetails))
							.setText("No offers available for the current selection");

					// alertDialog.getWindow().setBackgroundDrawable(new
					// ColorDrawable(android.graphics.Color.TRANSPARENT));
					// show it
					alert.show();

					return;
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} else {

				if (data != null) {
					if (data.equalsIgnoreCase("timeout")) {//
					// Log.e("data Received", "Response:"+data);
						Toast.makeText(AdvancSearchActivity.this,
								UNABLE_TO_REACH, 1).show();
						return;
					} else if (data.equalsIgnoreCase("nonetwork")) {
						// Log.e("data Received", "Response:"+data);
						Toast.makeText(AdvancSearchActivity.this, noNetwork, 1)
								.show();
						return;
					}

				}

			}

			// Toast.makeText(AdvancSearchActivity.this, "Response:"+data,
			// 1).show();

			Intent intent = new Intent(AdvancSearchActivity.this,
					ShowOffersActivity.class);
			// to clear all top activities on it
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			// to start the same Activity if its already running
			intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
			b.putString("latEnterCity", latEnterCity);
			b.putString("lonEnterCity", lonEnterCity);
			b.putString("response", data);
			intent.putExtras(b);
			AdvancSearchActivity.this.finish();
			startActivity(intent);

		}

		public String doInBackground(String... params) {
			String response = null;
			String x = params[0];
			String y = params[1];
			String category = params[2];

			// Log.d("address=>",""+x+"==="+y+"=="+selectedRadius);
			try {
				// filterOffersByThisCuisineList.toString().substring(1,
				// filterOffersByThisCuisineList.toString().length() -
				// 1).replace(", ", ",");
				// http://app.mapmyindia.com/hdfcapi/poi.do?x=28.5229&y=77.2094&radius=2&type=offers
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
				nameValuePairs.add(new BasicNameValuePair("x", x));
				nameValuePairs.add(new BasicNameValuePair("y", y));
				nameValuePairs.add(new BasicNameValuePair("rtype", "json/xml"));
				nameValuePairs.add(new BasicNameValuePair("type", "offers"));

				// nameValuePairs.add(new BasicNameValuePair("radius", "10"));
				nameValuePairs.add(new BasicNameValuePair("radius", ""
						+ selectedRadius / 1000));

				/*---NOTE---
				 * We are sending 'City' only in Adv.Search b`coz if we don`t then 'MapMyIndia'
				 * API will return me all India`s data of concern 'Category'...
				 */
				nameValuePairs.add(new BasicNameValuePair("City", b
						.getString("city")));
				// nameValuePairs.add(new BasicNameValuePair("City", ""));
				nameValuePairs.add(new BasicNameValuePair("Area", b
						.getString("area")));
				// Log.e("Test ---",
				// "Category:"+advSearcDatahBundle.getString("category"));
				nameValuePairs.add(new BasicNameValuePair("EMI", b
						.getString("emi")));

				System.out.println("GetOffersData.doInBackground() URL:"
						+ OffersPostUrl + ", Lat:" + x + ", Long:" + y);

				// response =
				// pU.getPostXML(PostUrl,ShowOffersActivity.this,nameValuePairs);
				response = ProxyUrlUtil
						.getOffers(OffersPostUrl, nameValuePairs);
				// response =
				// ProxyUrlUtil.getOffers("http://app.mapmyindia.com/hdfcapi/poi.do",nameValuePairs);
				// Log.d("Response of offers=>",""+response);

			} catch (Exception e) {
				e.printStackTrace();
				if (ProxyUrlUtil.isNetworkAvailable(AdvancSearchActivity.this)) {
					return "timeout";

				} else {
					return "nonetwork";

				}

			}

			return response;
		}
	}

	// .......Start of 'GetSearchedData'
	/**
	 * Sole purpose of this Class is to get Location of City/Area entered(From
	 * Offers Home) os Selected(from Adv-Search). Once it get Location
	 * Successfully, it will pass it to 'GetOffersData' to get all 'Offers'
	 * nearBy or Around the concern City/Area.
	 * 
	 * @author Rupesh
	 * 
	 */
	private class GetCityOffersData extends UserTask<String, Void, String> {
		String type;// , lat, lon;
		// ProgressDialog mPdialog;

		public void onPreExecute() {
			if (mPdialog == null)
				mPdialog = new ProgressDialog(AdvancSearchActivity.this);

			mPdialog.setMessage("Please wait....");
			mPdialog.setIndeterminate(true);
			mPdialog.setCancelable(false);
			mPdialog.show();

		}

		public void onPostExecute(String data) {

			if (mPdialog != null) {
				if (mPdialog.isShowing())
					mPdialog.dismiss();
			}

			if (data == null || data.length() < 20) {
				return;
			}
			// try {
			/*
			 * This how the response 'data' looks like_ { "search": "map",
			 * "result": { "type": "exact/best", "address":
			 * "Vashi, Navi Mumbai, Maharashtra", "pos": { "lat": 19.0768,
			 * "lon": 73.001 }, "lev": 9 }, "alternates": [ { "address":
			 * "Kashi Vashi,Chembur,Mumbai,Maharashtra", "lev": 0
			 * },{},{},{},{},...
			 * 
			 * ] }
			 */
			JSONObject jObj = null;
			// JSONObject jObjj = null;
			JSONArray alternates = null;
			try {
				// Log.v("Entered-City Info", "All Info:"+data);
				jObj = new JSONObject(data);

				alternates = jObj.getJSONArray("alternates");

				JSONObject resultObj = jObj.getJSONObject("result");
				type = resultObj.getString("type");
				String address = resultObj.getString("address");
				String level = resultObj.getString("lev");

				JSONObject position = resultObj.getJSONObject("pos");
				latEnterCity = position.getString("lat");
				lonEnterCity = position.getString("lon");
				// latEnterCity=lat;
				// lonEnterCity=lon;

			} catch (JSONException e) {
				e.printStackTrace();
			}

			// Log.d("type=>",""+type);
			try {

				if (type.equalsIgnoreCase("exact")
						|| type.equalsIgnoreCase("best")) {
					// Double latitude=Double.parseDouble(latEnterCity);
					// Double longitude=Double.parseDouble(lonEnterCity);
					// //
					// Log.d("latitude & longitude=",latitude+"==	"+longitude);
					//
					// mapOffersOverlays = mMapView.getOverlays();// gc...
					// GeoPoint point = new GeoPoint((int) (latitude * 1E6),
					// (int) (longitude * 1E6));
					// MapController mapController = mMapView.getController();
					// mapController.animateTo(point);
					// mapController.setZoom(15);

					// In this case get all categories data...
					// getOffersData = (GetOffersData) new
					// GetOffersData().execute(latEnterCity,lonEnterCity,"");
					new GetOffersData().execute(latEnterCity, lonEnterCity, "");

				} else {

					Toast.makeText(AdvancSearchActivity.this,
							"Please, Try again later!", 1).show();
					/*
					 * Enter here when the location lat-long 'type' of 'entered'
					 * city is not exact! Not needed here...
					 */

				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		@Override
		public String doInBackground(String... params) {
			String response = null;
			String address = params[0];
			// Log.d("doInBackground=>","GetCityOffersData - Entered city:"+address);
			try {
				URL PostUrl = new URL(
						"http://apis.mapmyindia.com/v2.0/geocode/key=2c07ede70ef00dd05ab7db0366e64ecd&rtype=json");
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
						1);
				nameValuePairs.add(new BasicNameValuePair("addr", address));
				ProxyUrlUtil pU = new ProxyUrlUtil();

				response = pU.getPostXML(PostUrl, AdvancSearchActivity.this,
						nameValuePairs);
				// Log.d("Responmse=>",""+response);
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			}

			return response;
		}

	}

	// ...........END....

	/**
	 * Show Toast message.
	 * 
	 * @param context
	 *            current context.
	 * @param msg
	 *            message to show.
	 * @param color
	 *            background color of Toast if needed null otherwise.
	 */
	private Toast showTost(Context context, String msg) {
		// TODO Auto-generated method stub
		Toast toast = Toast.makeText(AdvancSearchActivity.this, msg, 1);
		toast.setGravity(Gravity.CENTER, 0, 0);

		View view = toast.getView();
		view.setPadding(2, 2, 2, 2);

		view.setBackgroundColor(res.getColor(R.color.red));
		// toast.show();
		return toast;
	}

	/**
	 * Show City list. That can be shorted as user start typing. gc...
	 */
	private void showCityList() {
		// TODO Auto-generated method stub
		// final Dialog callAlert = new
		// Dialog(AdvancSearchActivity.this,R.style.CutomDialog);
		final Dialog callAlert = new Dialog(AdvancSearchActivity.this);
		// callAlert.requestWindowFeature(Window.FEATURE_NO_TITLE);
		WindowManager.LayoutParams wmlp = callAlert.getWindow().getAttributes();
		wmlp.gravity = Gravity.LEFT;// CENTER;
		wmlp.width = LayoutParams.WRAP_CONTENT;// ru
		wmlp.height = LayoutParams.MATCH_PARENT;// ru
		// callAlert.getWindow().setAttributes(wmlp);
		callAlert.setContentView(R.layout.filters_layout);
		// ru
		callAlert.getWindow().setLayout(LayoutParams.WRAP_CONTENT,
				LayoutParams.FILL_PARENT);

		ImageView close = (ImageView) callAlert.findViewById(R.id.btn_close);
		ImageView btnGo = (ImageView) callAlert.findViewById(R.id.btn_go);
		// Get ListView of 'Cuisine & Area'
		ListView cityListView = (ListView) callAlert
				.findViewById(R.id.area_filter_list);

		TextView noFilterFound = (TextView) callAlert
				.findViewById(R.id.text_no_filter_found);

		// Cusine and Area layout that contain 'Title' & their concern 'List' of
		// filter items.
		final LinearLayout byCuisineLayout = (LinearLayout) callAlert
				.findViewById(R.id.byCusine_layout);
		LinearLayout byAreaLayout = (LinearLayout) callAlert
				.findViewById(R.id.byArea_layout);

		cityListView.setAdapter(new DataAdapter(AdvancSearchActivity.this,
				R.layout.my_spinner_text, cityArrayList));

		// if (cityListView.getAdapter().getCount()==0) {
		// Log.v("ShowOffersActivity",
		// "Filter- CuisineList Size:areaArrayList.size() == Empty!");
		// //Show No Filter Text...
		// if (noFilterFound.getVisibility()==View.GONE) {
		// Log.v("ShowOffersActivity", "Filter- in noFilterFound visibility!");
		// noFilterFound.setVisibility(View.VISIBLE);
		// }
		// } else {
		// Log.v("ShowOffersActivity", "Filter- Cuisine & Area List != Empty!");
		// //Hide No Filter if Visible...
		// if (noFilterFound.getVisibility()==View.VISIBLE) {
		// noFilterFound.setVisibility(View.GONE);
		// }
		//
		// }

		// For Area Filters Item...
		// if (cityListView.getAdapter().getCount()!=0) {
		//
		// if (byAreaLayout.getVisibility()==View.GONE) {
		// //Toast.makeText(ShowOffersActivity.this, "Enter in RU_"+ru,
		// 0).show();
		// byAreaLayout.setVisibility(View.VISIBLE);
		//
		// }
		//
		// cityListView.setAdapter(new DataAdapter(AdvancSearchActivity.this,
		// R.layout.my_spinner_text, cityArrayList));
		// } else {
		// //Toast.makeText(ShowOffersActivity.this, "Enter in RU_"+ru,
		// 0).show();
		// if (byAreaLayout.getVisibility()==View.VISIBLE) {
		// byAreaLayout.setVisibility(View.GONE);
		// //areaArrayList.add("TEST AREA");
		//
		// }
		//
		// }

		cityListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adp, View vw, int pos,
					long id) {
				// TODO Auto-generated method stub
				cCity.setText(adp.getItemAtPosition(pos).toString());

				// remove drawer
				callAlert.dismiss();
			}
		});
		// onclick of this 'btnGo' you have to get the 'Offers' according to
		// 'Filter' items checked!
		// btnGo.setOnClickListener(new OnClickListener() {});
		// close.setOnClickListener(new OnClickListener() {});

		callAlert.show();
	}
}
