package com.rblbank.mobank;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.google.android.gcm.GCMBaseIntentService;

@SuppressLint("NewApi")
public class generatePictureStyleNotification extends
		AsyncTask<String, Void, Bitmap> {

	private Context mContext;
	private String title, message, imageUrl;
	private int countTrials = 0;

	public generatePictureStyleNotification(Context context, String title,
			String message, String imageUrl) {
		super();
		this.mContext = context;
		this.title = title;
		this.message = message;
		this.imageUrl = imageUrl;
	}

	@Override
	protected Bitmap doInBackground(String... params) {
		Bitmap myBitmap = null;

		//System.out.println("start now");
		while (myBitmap == null) {
			myBitmap = downloadImage();
			if (countTrials > 2) {
				//System.out.println("go for normal now");
				break;
			} else {
				//System.out.println("try again - "+countTrials);
				countTrials++;
			}
		}
		return myBitmap;
	}

	private Bitmap downloadImage() {
		try {
			URL url = new URL(this.imageUrl);
			HttpURLConnection connection = (HttpURLConnection) url
					.openConnection();
			connection.setDoInput(true);
			connection.connect();
			InputStream in = connection.getInputStream();
			return BitmapFactory.decodeStream(in);
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	protected void onPostExecute(Bitmap result) {
		super.onPostExecute(result);

		if (result == null) {

			//System.out.println("Normal notification in itteration - "
			//		+ countTrials);
			long when = System.currentTimeMillis();
			int notification_id = (int) when;
			NotificationManager mNotificationManager = (NotificationManager) mContext
					.getSystemService(Context.NOTIFICATION_SERVICE);
			String appName = this.title;

			PackageManager pm = mContext.getPackageManager();
			Intent notificationIntent = pm.getLaunchIntentForPackage(mContext
					.getApplicationContext().getPackageName());

			// notificationIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP |
			// Intent.FLAG_ACTIVITY_CLEAR_TOP);
			notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
					| Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
			// notificationIntent.putExtra("pushBundle", extras);

			PendingIntent contentIntent = PendingIntent.getActivity(mContext,
					100, notificationIntent, PendingIntent.FLAG_ONE_SHOT);

			// Use NotificationCompat.Builder to set up our notification.
			NotificationCompat.Builder builder = new NotificationCompat.Builder(
					mContext);
			builder.setDefaults(Notification.DEFAULT_SOUND);

			// icon appears in device notification bar and right hand corner
			// of
			// notification
			builder.setSmallIcon(mContext.getApplicationInfo().icon);

			// Content title, which appears in large type at the top of the
			// notification
			builder.setContentTitle(this.title);

			// Content text, which appears in smaller text below the title

			builder.setContentText(this.message);

			builder.setStyle(new NotificationCompat.BigTextStyle()
					.bigText(message));

			builder.setDefaults(Notification.DEFAULT_VIBRATE);

			// Set the intent that will fire when the user taps the
			// notification.
			builder.setContentIntent(contentIntent);

			// set auto cancel
			builder.setAutoCancel(true);

			// Large icon appears on the left of the notification
			builder.setLargeIcon(BitmapFactory.decodeResource(
					mContext.getResources(), R.drawable.icon));
			int notId = 0;

			// Log.e(TAG, "createnotification called:" +
			// "showing notification");
			mNotificationManager.notify((String) appName, notification_id,
					builder.build());

		} else {
			System.out.println("Big picture in itteration - " + countTrials);
			/*
			 * long when = System.currentTimeMillis(); int notification_id =
			 * (int) when;
			 * 
			 * PackageManager pm = mContext.getPackageManager(); Intent intent =
			 * pm.getLaunchIntentForPackage(mContext.getPackageName());
			 * intent.putExtra("key", "value"); //Intent notificationIntent =
			 * new Intent(this, PushHandlerActivity.class);
			 * //intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP |
			 * Intent.FLAG_ACTIVITY_CLEAR_TOP);
			 * 
			 * PendingIntent pendingIntent = PendingIntent.getActivity(mContext,
			 * 100, intent, PendingIntent.FLAG_ONE_SHOT);
			 * 
			 * NotificationManager notificationManager = (NotificationManager)
			 * mContext.getSystemService(Context.NOTIFICATION_SERVICE);
			 * Notification notif = new Notification.Builder(mContext)
			 * .setContentIntent(pendingIntent) .setContentTitle(title)
			 * .setContentText(message)
			 * .setSmallIcon(mContext.getApplicationInfo().icon)
			 * .setLargeIcon(BitmapFactory
			 * .decodeResource(mContext.getResources(), R.drawable.icon))
			 * .setStyle(new Notification.BigPictureStyle().bigPicture(result))
			 * .build(); notif.flags |= Notification.FLAG_AUTO_CANCEL;
			 * notificationManager.notify(notification_id, notif);
			 */

			long when = System.currentTimeMillis();
			int notification_id = (int) when;
			NotificationManager mNotificationManager = (NotificationManager) mContext
					.getSystemService(Context.NOTIFICATION_SERVICE);
			String appName = this.title;

			PackageManager pm = mContext.getPackageManager();
			Intent notificationIntent = pm.getLaunchIntentForPackage(mContext
					.getApplicationContext().getPackageName());

			// notificationIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP |
			// Intent.FLAG_ACTIVITY_CLEAR_TOP);
			notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
					| Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
			// notificationIntent.putExtra("pushBundle", extras);

			PendingIntent contentIntent = PendingIntent.getActivity(mContext,
					100, notificationIntent, PendingIntent.FLAG_ONE_SHOT);

			// Use NotificationCompat.Builder to set up our notification.
			NotificationCompat.Builder builder = new NotificationCompat.Builder(
					mContext);
			builder.setDefaults(Notification.DEFAULT_SOUND);

			// icon appears in device notification bar and right hand corner
			// of
			// notification
			builder.setSmallIcon(mContext.getApplicationInfo().icon);

			// Content title, which appears in large type at the top of the
			// notification
			builder.setContentTitle(this.title);

			// Content text, which appears in smaller text below the title

			builder.setContentText(this.message);

			// TODO ok till here

			NotificationCompat.BigPictureStyle s = new NotificationCompat.BigPictureStyle()
					.bigPicture(result);
			s.setSummaryText(message);
			builder.setStyle(s);

			// builder.setStyle(new
			// NotificationCompat.BigTextStyle().bigText(message));

			builder.setDefaults(Notification.DEFAULT_VIBRATE);

			// Set the intent that will fire when the user taps the
			// notification.
			builder.setContentIntent(contentIntent);

			// set auto cancel
			builder.setAutoCancel(true);

			// Large icon appears on the left of the notification
			builder.setLargeIcon(BitmapFactory.decodeResource(
					mContext.getResources(), R.drawable.icon));

			Notification notification = builder.build();
			// Will display the notification in the notification bar
			mNotificationManager.notify(notification_id, notification);
			// mNotificationManager.notify((String) appName,
			// notification_id,
			// builder.build());
		}
	}
}

/*
 * public class GCMIntentService extends
 * com.worklight.androidgap.push.GCMIntentService{ //Nothing to do here... }
 */
